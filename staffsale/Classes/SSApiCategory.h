//
//  SSApiNewsCategory.h
//  staffsale
//
//  Created by Daniel Wetzel on 26.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiObject.h"

@interface SSApiCategory : SSApiObject

@property (nonatomic, strong, readonly) NSString *identifier;
@property (nonatomic, strong, readonly) SSApiCategory *parentCategory;

@property (nonatomic, strong, readonly) NSString *title;

@property (nonatomic, strong, readonly) NSArray *imageURLs;
@property (nonatomic, strong, readonly) NSURL *titleImageURL;

@end

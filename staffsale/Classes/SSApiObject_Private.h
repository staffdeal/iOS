//
//  SSApiObject_Private.h
//  staffsale
//
//  Created by Daniel Wetzel on 18.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiObject.h"

@interface SSApiObject ()

@property (nonatomic, weak, readwrite) SSApiObject *parent;
@property (nonatomic, weak, readwrite) SSApiModule *module;

@end

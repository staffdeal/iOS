//
//  SSHTMLTemplate.h
//  staffsale
//
//  Created by Daniel Wetzel on 26.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SSHTMLTemplate : NSObject

- (id)initWithName:(NSString *)name;	// the loaded file is named "name.template.html"

- (void)assignString:(NSString *)value forField:(NSString *)field;
- (void)appendString:(NSString *)value toField:(NSString *)field;

- (void)assignDate:(NSDate *)date forField:(NSString *)field style:(NSDateFormatterStyle)style;
- (void)assignDate:(NSDate *)date forField:(NSString *)field dateStyle:(NSDateFormatterStyle)dateStyle timeStyle:(NSDateFormatterStyle)timeStyle;

- (NSString *)content;

@end

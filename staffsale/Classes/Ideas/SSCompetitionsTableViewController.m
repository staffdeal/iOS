//
//  SSIdeasTableViewController.m
//  staffsale
//
//  Created by Daniel Wetzel on 02.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSCompetitionsTableViewController.h"

#import "SSApiIdeasModule.h"
#import "SSApiIdeasCompetition.h"

#import "DWActivityManager.h"
#import "DWAlertView.h"
#import "DWCountdownLabel.h"

#import "SSTableViewCell.h"

#import "SSIdeasCompetitionDetailsViewController.h"

#define kSSIdeasTableViewCellBackgroundColorKey @"ideas.list.item.backgroundColor"
#define kSSIdeasTableViewCellSelectedBackgroundColorKey @"ideas.list.item.backgroundColor.selected"

@interface SSCompetitionsTableViewController ()

@property (nonatomic, readonly) SSApiIdeasModule *ideasModule;

@end

@implementation SSCompetitionsTableViewController

- (SSApiIdeasModule *)ideasModule {
	return (SSApiIdeasModule *)self.apiModule;
}

- (void)viewDidLoad
{
	self.isDataController = YES;
	self.needsFilterButton = YES;
	self.needsRefreshControl = YES;
    [super viewDidLoad];
	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (section == 0) {
		return self.ideasModule.competitions.count;
	}
	return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	SSTableViewCell *cell = (SSTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kSSTableViewCellRequseIdentifier forIndexPath:indexPath];
	cell.style = SSTableViewCellStyleIdeas;
	cell.read = YES;
		
	NSString *competitionIdentifier = [self.ideasModule.competitions.allKeys objectAtIndex:indexPath.row];
	if (competitionIdentifier) {
		
		SSApiIdeasCompetition *competition = [self.ideasModule.competitions objectForKey:competitionIdentifier];
		if (competition) {
			cell.titleLabel.text = competition.title;
			cell.info1Label.date = competition.endDate;
			cell.titleImageURL = competition.category.titleImageURL;
			cell.info3Label.text = competition.companyName;
			
			cell.badge = (competition.ideas.count > 0) ? [NSString stringWithFormat:@"%i",competition.ideas.count] : nil;
		}
	}
    
    return cell;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSString *competitionIdentifier = [self.ideasModule.competitions.allKeys objectAtIndex:indexPath.row];
	if (competitionIdentifier) {
		SSApiIdeasCompetition *competition = [self.ideasModule.competitions objectForKey:competitionIdentifier];
		
		SSLog(@"Showing competition details (%@)",competition.title);
		
		SSIdeasCompetitionDetailsViewController *competitionController = [[SSIdeasCompetitionDetailsViewController alloc] initWithCompetition:competition];
		[self.navigationController pushViewController:competitionController animated:YES];
	}
}

@end
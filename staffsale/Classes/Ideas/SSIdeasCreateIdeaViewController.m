//
//  SSIdeasCreateIdeaViewController.m
//  staffsale
//
//  Created by Daniel Wetzel on 12.11.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSIdeasCreateIdeaViewController.h"

#import <MobileCoreServices/MobileCoreServices.h>

#import "SSTableViewCell.h"

#import "DWConfiguration.h"

#import "SSWebViewController.h"
#import "SSHTMLTemplate.h"

#import "SSApiIdeasCompetition.h"
#import "SSApiSubmit.h"

#import "DWInputTableViewController.h"
#import "DWURLConnection.h"
#import "DWActivityManager.h"
#import "DWAlertView.h"
#import "DWActionSheet.h"
#import "UIImage+DWToolbox.h"


#define kSSIdeasCompetitionsDetailsCellSelectedBackgroundColorKey @"ideas.list.item.backgroundColor.selected"

@interface SSIdeasCreateIdeaViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong, readwrite) SSIdeaDraft *draft;

@end

@implementation SSIdeasCreateIdeaViewController

- (id)initWithCompetition:(SSApiIdeasCompetition *)competition {
	SSIdeaDraft *newDraft = [[SSIdeaDraft alloc] init];
	newDraft.competition = competition;
	return [self initWithDraft:newDraft];
}

- (id)initWithDraft:(SSIdeaDraft *)draft {
	if ((self = [self initWithStyle:UITableViewStylePlain])) {
		self.draft = draft;
		self.title = NSLocalizedString(@"Neue Idee", @"New Idea");
	}
	return self;
}

- (SSApiIdeasCompetition *)competition {
	return self.draft.competition;
}

- (void)viewDidLoad
{
	self.isDataController = YES;
    [super viewDidLoad];
	
	UIBarButtonItem *previewButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Vorschau", @"New Idea")
																	  style:UIBarButtonItemStyleDone
																	 target:self
																	 action:@selector(previewDraft)];
	previewButton.enabled = NO;
	self.navigationItem.rightBarButtonItem = previewButton;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (section == 0) {
		return 3;
	} else if (section == 1) {
		return 0;
	}
	return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SSTableViewCell *cell = (SSTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kSSTableViewCellRequseIdentifier forIndexPath:indexPath];
	
	cell.style = SSTableViewCellStyleInputList;
	
	UIColor *selectedBackgroundColor = [UIColor colorFromCustomizationKey:kSSIdeasCompetitionsDetailsCellSelectedBackgroundColorKey];
	if (selectedBackgroundColor != nil) {
		cell.selectedBackgroundColor = selectedBackgroundColor;
	}
	
	if (indexPath.section == 0 && indexPath.row == 0) {
		
		if (self.draft.text.length == 0) {
			cell.info3Label.text = nil;
			cell.titleLabel.text = NSLocalizedString(@"Beschreibung hinzufügen", @"New Idea");
			cell.checkbox.checked = NO;
		} else {
			cell.info3Label.text = NSLocalizedString(@"Beschreibung", @"New Idea");
			cell.titleLabel.text = self.draft.text;
			cell.checkbox.checked = YES;
		}
		
	} else if (indexPath.section == 0 && indexPath.row == 1) {
		cell.info3Label.text = nil; //NSLocalizedString(@"Foto", @"New Idea");
		if (self.draft.image == nil) {
			cell.titleLabel.text = NSLocalizedString(@"Foto hinzufügen", @"New Idea");
			cell.checkbox.checked = NO;
		} else {
			cell.titleLabel.text = NSLocalizedString(@"Foto entfernen", @"New Idea");
			cell.checkbox.checked = YES;
		}
	} else if (indexPath.section == 0 && indexPath.row == 2) {
		if (self.draft.URL == nil) {
			cell.info3Label.text = nil;
			cell.titleLabel.text = NSLocalizedString(@"Link hinzufügen", @"New Idea");
			cell.checkbox.checked = NO;
		} else {
			cell.info3Label.text = NSLocalizedString(@"Link", @"New Idea");
			cell.titleLabel.text = self.draft.URL.absoluteString;
			cell.checkbox.checked = YES;
		}
	}
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
	
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.row == 0) {
		
		DWInputTableViewController *input = [[DWInputTableViewController alloc] initWithType:DWInputTableViewControllerTypeTextField returnHandler:^(NSString *inputText) {
			[self.navigationController popViewControllerAnimated:YES];
			self.draft.text = inputText;
			[self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForItem:0 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
			[self checkIfSendable];
		}];
		
		input.title = NSLocalizedString(@"Idee einreichen", @"New Idea");
		input.footerText = NSLocalizedString(@"Bitte beschreiben Sie Ihre Idee so ausführlich wie möglich.", @"New Idea");
		input.inputText = self.draft.text;
		[self.navigationController pushViewController:input animated:YES];
		
	} else if (indexPath.row == 1) {
		
		if (self.draft.image == nil) {
			
			NSUInteger isCameraAvailable = ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) ? 1 : 0;
			NSUInteger isPhotoLibraryAvailable = ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) ? 1 : 0;
			NSUInteger isSavedImagesAvailable = ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]) ? 1 : 0;
			
			NSUInteger numberOfAvailableSources = isCameraAvailable + isPhotoLibraryAvailable + isSavedImagesAvailable;
			
			if (numberOfAvailableSources > 0) {
				
				UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
				imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
				imagePicker.delegate = self;
				imagePicker.editing = YES;
				
				if (numberOfAvailableSources == 1) {
					
					if (isCameraAvailable) {
						imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
					} else if (isPhotoLibraryAvailable) {
						imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
					} else if (isSavedImagesAvailable) {
						imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
					}
					[self presentViewController:imagePicker animated:YES completion:^{}];
					
				} else {
					
					DWActionSheet *sheet = [[DWActionSheet alloc] initWithTitle:NSLocalizedString(@"Bild auswählen", @"New Idea")];
					[sheet addCancelItemWithTitle:@"Abbrechen" handler:^{
						[self.tableView deselectRowAtIndexPath:indexPath animated:YES];
					}];
					if (isCameraAvailable) {
						[sheet addItemWithTitle:NSLocalizedString(@"Kamera", @"New Idea") handler:^{
							imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
							[self presentViewController:imagePicker animated:YES completion:^{}];
						}];
					}
					if (isPhotoLibraryAvailable) {
					[sheet addItemWithTitle:NSLocalizedString(@"Bibliothek", @"New Idea") handler:^{
						imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
						[self presentViewController:imagePicker animated:YES completion:^{}];
					}];
					}
					if (isSavedImagesAvailable) {
						[sheet addItemWithTitle:NSLocalizedString(@"Aufnahmen", @"New Idea") handler:^{
							imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
							[self presentViewController:imagePicker animated:YES completion:^{}];
						}];
					}
					
					[sheet showFromTabbar:self.tabBarController.tabBar];
					
				}
			}
		} else {
			
			self.draft.image = nil;
			[self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForItem:1 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
			
		}
		
	} else if (indexPath.row == 2) {
		DWInputTableViewController *input = [[DWInputTableViewController alloc] initWithType:DWInputTableViewControllerTypeURL returnHandler:^(NSString *inputText) {
			[self.navigationController popViewControllerAnimated:YES];
			
			if (inputText.length == 0) {
				self.draft.URL = nil;
			} else {
				
				if (![inputText hasPrefix:@"http"] && ![inputText hasPrefix:@"ftp"]) {
					inputText = [NSString stringWithFormat:@"http://%@",inputText];
				}
				
				NSURL *url = [NSURL URLWithString:inputText];
				if (url) {
					self.draft.URL = url;
				}
			}
			[self checkIfSendable];
			[self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForItem:2 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
		}];
		
		input.title = NSLocalizedString(@"Idee einreichen", @"New Idea");
		input.footerText = NSLocalizedString(@"Geben Sie eine URL an. Das kann ein Youtube-Video oder eine andere Webseite sein.", @"New Idea");
		input.inputText = self.draft.URL.absoluteString;
		[self.navigationController pushViewController:input animated:YES];
		
	}
}

#pragma mark - Image Picker Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
	
	
	DWActivityItem *activity = [[DWActivityManager sharedManager] addActivityWithTitle:NSLocalizedString(@"Bild wird verarbeitet", @"New Idea")];
	
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
		if (image == nil) {
			image = [info objectForKey:UIImagePickerControllerOriginalImage];
		}
		
		if (image) {
			if (image.size.width > 800 | image.size.height > 800) {
				image = [image imageByMatchingWidthOrHeightOfSize:CGSizeMake(800.0f, 800.0f)];
			}
			
			sleep(2); // this is only to ensure that the activity view is about to be seen
			dispatch_async(dispatch_get_main_queue(), ^{
				self.draft.image = image;
				[self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForItem:1 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
				[activity hide];
			});
		}
	});
	[self dismissViewControllerAnimated:YES completion:^{}];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
	[self dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark - Sending

- (void)checkIfSendable {
	self.navigationItem.rightBarButtonItem.enabled = [self isSendable];
}

- (BOOL)isSendable {
	if (self.draft.text.length > 10) {
		return YES;
	} else {
		return NO;
	}
}

- (void)previewDraft {
	if ([self isSendable]) {
		
		SSLog(@"Showing idea preview (%@)",self.competition.title);
		
		SSWebViewController *previewController = [[SSWebViewController alloc] initWithHTML:self.draft.draftTemplate.content];
		previewController.title = NSLocalizedString(@"Vorschau", @"New Idea");
		UIBarButtonItem *sendButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Senden", @"New Idea")
																	   style:UIBarButtonItemStyleDone
																	  target:self
																	  action:@selector(sendDraft)];
		previewController.navigationItem.rightBarButtonItem = sendButton;
		[self.navigationController pushViewController:previewController animated:YES];
		
	}
}

- (void)sendDraft {
	
	SSLog(@"Sending idea (%@)",self.competition.title);
	
	DWActivityItem *activity = [[DWActivityManager sharedManager] addActivityWithTitle:NSLocalizedString(@"Idee wird gesendet", @"New Idea")];
	
	SSBackendRequest *upload = [SSBackendRequest requestWithPath:@"ideas/submit"];
	[upload addParameter:self.draft.competition.identifier forKey:@"competition"];
	[upload addUploadValue:self.draft.text forKey:@"text"];
	
	if (self.draft.URL) {
		[upload addUploadValue:self.draft.URL.absoluteString forKey:@"url"];
	}
	if (self.draft.image) {
		[upload addUploadImage:self.draft.image forKey:@"image" filename:@"image.png"];
	}
	
	[upload startOnSharedConnectionWithCompletion:^(SSBackendResponse *response, NSError *error) {
		[activity hide];
		
		if (response.submit.isSuccessful) {
			
			SSLog(@"Submittion of idea (%@) successfull",self.competition.title);
			
			DWAlertView *successView = [[DWAlertView alloc] initWithTitle:NSLocalizedString(@"Idee senden", @"New Idea")
															   andMessage:NSLocalizedString(@"Die Idee wurde gesendet. Vielen Dank.\nDie Gewinner werden per E-Mail benachrichtigt.", @"New Idea")];
			
			[successView addActionWithTitle:NSLocalizedString(@"OK", @"New Idea") block:^{
				[self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
			}];
			[successView show];
			
		} else {
			
			SSLog(@"Submittion of idea (%@) failed",self.competition.title);
			
			DWAlertView *errorView = [[DWAlertView alloc] initWithTitle:NSLocalizedString(@"Idee senden", @"New Idea")
															 andMessage:NSLocalizedString(@"Das Sender der Idee war nicht erfolgreich.", @"New Idea")];
			[errorView addActionWithTitle:NSLocalizedString(@"Erneut versuchen", @"New Idea") block:^{
				[self sendDraft];
			}];
			[errorView addActionWithTitle:NSLocalizedString(@"Abbrechen", @"General") block:^{
				[self.navigationController popViewControllerAnimated:YES];
			}];
			[errorView show];
		}
	}];
}

@end

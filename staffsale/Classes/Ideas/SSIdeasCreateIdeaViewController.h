//
//  SSIdeasCreateIdeaViewController.h
//  staffsale
//
//  Created by Daniel Wetzel on 12.11.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SSModuleTableViewController.h"
#import "SSIdeaDraft.h"

@class SSApiIdeasCompetition;

@interface SSIdeasCreateIdeaViewController : SSModuleTableViewController

@property (nonatomic, strong, readonly) SSIdeaDraft *draft;
@property (nonatomic, strong, readonly) SSApiIdeasCompetition *competition;

- (id)initWithDraft:(SSIdeaDraft *)draft;
- (id)initWithCompetition:(SSApiIdeasCompetition *)competition;

@end

//
//  SSApiIdeasCompetition.h
//  staffsale
//
//  Created by Daniel Wetzel on 05.11.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiObject.h"

#import "SSApiModule.h"

@class SSApiCategory;

@class SSHTMLTemplate;

@interface SSApiIdeasCompetition : SSApiObject

@property (nonatomic, strong, readonly) NSString *identifier;
@property (nonatomic, weak, readonly) SSApiCategory *category;

@property (nonatomic, strong, readonly) NSString *title;
@property (nonatomic, strong, readonly) NSString *companyName;

@property (nonatomic, strong, readonly) NSString *teaserText;
@property (nonatomic, strong, readonly) NSString *descriptionHTML;
@property (nonatomic, strong, readonly) NSString *rulesHTML;

@property (nonatomic, strong, readonly) NSDate *startDate;
@property (nonatomic, strong, readonly) NSDate *endDate;

@property (nonatomic, strong, readonly) NSURL *titleImageURL;
@property (nonatomic, strong, readonly) NSArray *imageURLs;

@property (nonatomic, readonly) BOOL isRefreshingIdeas;
@property (nonatomic, strong, readonly) NSDictionary *ideas;

- (SSHTMLTemplate *)descriptionTemplate;
- (SSHTMLTemplate *)rulesTemplate;

- (void)performInitialIdeasLoadingWithCompletion:(SSApiModuleRefreshHandler)handler;
- (void)refreshIdeasWithCompletion:(SSApiModuleRefreshHandler)handler;

@end

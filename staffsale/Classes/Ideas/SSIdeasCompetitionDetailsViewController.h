//
//  SSIdeasCompetitionDetailsViewController.h
//  staffsale
//
//  Created by Daniel Wetzel on 08.11.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SSModuleTableViewController.h"

@class SSApiIdeasCompetition;

@interface SSIdeasCompetitionDetailsViewController : SSModuleTableViewController

@property (nonatomic, strong, readonly) SSApiIdeasCompetition *competition;

- (id)initWithCompetition:(SSApiIdeasCompetition *)competition;

@end

//
//  SSApiIdeasCompetition.m
//  staffsale
//
//  Created by Daniel Wetzel on 05.11.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiIdeasCompetition.h"
#import "SSApiIdeasCompetition_Private.h"

#import "SSApiObject_Private.h"

#import "SSBackendConnection.h"

#import "SSHTMLTemplate.h"
#import "SSApiCategory.h"
#import "SSApiIdea.h"

#import "SSBackendRequest.h"

@interface SSApiIdeasCompetition()

@property (nonatomic, strong, readwrite) NSString *identifier;

@property (nonatomic, assign) BOOL isRefreshingIdeas;

@end

@implementation SSApiIdeasCompetition

- (id)initWithXMLElement:(RXMLElement *)element {
	if ((self = [super initWithXMLElement:element])) {
		
		if ([element.tag isEqualToString:@"competition"]) {
			self.identifier = [element attribute:@"id"];
			self.categoryIdentifier = [[element child:@"category"] text];
			
			self.title = [[element child:@"title"] text];
			self.companyName = [[element child:@"company"] text];
			self.teaserText = [[[element child:@"teaser"] text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
			self.descriptionHTML = [[[element child:@"description"] text]  stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];;
			self.rulesHTML = [[[element child:@"rules"] text]  stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
			
			self.startDate = [[SSBackendConnection dateFormatter] dateFromString:[[element child:@"start"] text]];
			self.endDate = [[SSBackendConnection dateFormatter] dateFromString:[[element child:@"end"] text]];
			
			NSMutableArray *imageURLs = [NSMutableArray array];
			[element iterate:@"images.image" usingBlock:^(RXMLElement *imageElement) {
				NSString *imageIdentifier = [imageElement attribute:@"id"];
				if (imageIdentifier) {
					NSURL *imageURL = [[NSURL alloc] initWithString:[imageElement text]];
					if ([imageIdentifier isEqualToString:@"titleImage"]) {
						self.titleImageURL = imageURL;
					} else {
						[imageURLs addObject:imageURL];
					}
				}
			}];
			if (imageURLs.count > 0) {
				self.imageURLs = [[NSArray alloc] initWithArray:imageURLs];
			}
		}
		
		
		if (self.identifier == nil) {
			self = nil;
		}
		
	}
	return self;
}

- (SSHTMLTemplate *)descriptionTemplate {
	SSHTMLTemplate *template = [[SSHTMLTemplate alloc] initWithName:@"competitionDescription"];
	[template assignString:self.title forField:@"title"];
	[template assignString:self.descriptionHTML forField:@"content"];
	
	NSString *categoryString = @"";
	if (self.category) {
		categoryString = [self.category.title copy];
	}
	if (self.companyName) {
		if (categoryString.length > 0) {
			categoryString = [categoryString stringByAppendingString:@", "];
		}
		categoryString = [categoryString stringByAppendingString:self.companyName];
	}
	[template assignString:categoryString forField:@"category"];
	
	[template assignDate:self.endDate forField:@"date" dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle];
	
	if (self.titleImageURL) {
		[template assignString:@"block" forField:@"titleImageDivDisplay"];
		[template assignString:self.titleImageURL.absoluteString forField:@"titleImageURL"];
	}
	
	return template;
}

- (SSHTMLTemplate *)rulesTemplate {
	SSHTMLTemplate *template = [[SSHTMLTemplate alloc] initWithName:@"competitionDescription"];
	[template assignString:self.title forField:@"title"];
	[template assignString:self.rulesHTML forField:@"content"];
	
	NSString *categoryString = @"";
	if (self.category) {
		categoryString = [self.category.title copy];
	}
	if (self.companyName) {
		if (categoryString.length > 0) {
			categoryString = [categoryString stringByAppendingString:@", "];
		}
		categoryString = [categoryString stringByAppendingString:self.companyName];
	}
	[template assignString:categoryString forField:@"category"];
	[template assignDate:self.endDate forField:@"date" dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle];
	[template assignString:@"none" forField:@"titleImageDivDisplay"];
	
	return template;
}


#pragma mark - Ideas

- (NSString *)ideasCacheID {
	return [NSString stringWithFormat:@"ideas_%@",self.identifier];
}

- (void)performInitialIdeasLoadingWithCompletion:(SSApiModuleRefreshHandler)handler {
	NSError *error = nil;
	SSBackendResponse *response = [self.module cachedResponseForKey:[self ideasCacheID]];
	if (response) {
		
		RXMLElement *element = [response.element child:@"ideas"];
		if (element == nil || [self importIdeasFromElement:element] == NO) {
			error = [NSError errorWithDomain:@"SSApiIdeasXMLMalformedError" code:100 userInfo:nil];
		}
		
	};
	
	handler(self.ideas, error);
}

- (void)refreshIdeasWithCompletion:(SSApiModuleRefreshHandler)handler {
	
	if (self.isRefreshingIdeas == NO) {
		self.isRefreshingIdeas = YES;
		
		SSBackendRequest *request = [[SSBackendRequest alloc] initWithPath:@"ideas/list"];
		[request addParameter:self.identifier forKey:@"competition"];
		[self.module.backendConnection sendRequest:request completion:^(SSBackendResponse *response, NSError *error) {
			
			dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
				
				NSError *blockError = error;
				
				if (blockError == nil) {
					[self.module cacheResponse:response forKey:[self ideasCacheID]];
					
					
					RXMLElement *element = [response.element child:@"ideas"];
					if (element == nil || [self importIdeasFromElement:element] == NO) {
						blockError = [NSError errorWithDomain:@"SSApiIdeasXMLMalformedError" code:100 userInfo:nil];
					}
				}
				
				dispatch_async(dispatch_get_main_queue(), ^{
					self.isRefreshingIdeas = NO;
					handler(self.ideas, blockError);
				});	
			});
		}];
	} else {
		int64_t delayInSeconds = 2.0;
		dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
		dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
			[self refreshIdeasWithCompletion:handler];
		});
	}
}

- (BOOL)importIdeasFromElement:(RXMLElement *)element {
	
	if ([element.tag isEqualToString:@"ideas"]) {
		
		NSMutableDictionary *ideas = [NSMutableDictionary dictionary];
		[element iterate:@"idea" usingBlock:^(RXMLElement *ideaElement) {
			
			SSApiIdea *idea = [[SSApiIdea alloc] initWithXMLElement:ideaElement];
			if (idea) {
				idea.parent = self;
				idea.module = self.module;
				[ideas setObject:idea forKey:idea.identifier];
			}
		}];
		
		self.ideas = [[NSDictionary alloc] initWithDictionary:ideas];
		
		return YES;
	}
	
	return NO;
}

@end

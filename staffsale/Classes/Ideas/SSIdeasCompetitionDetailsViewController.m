//
//  SSIdeasCompetitionDetailsViewController.m
//  staffsale
//
//  Created by Daniel Wetzel on 08.11.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSIdeasCompetitionDetailsViewController.h"

#import <QuartzCore/QuartzCore.h>

#import "SSApiIdeasCompetition.h"

#import "DWConfiguration.h"
#import "DWTextStyle.h"

#import "DWImageView.h"
#import "DWLabel.h"
#import "DWAlertView.h"

#import "SSTableViewCell.h"

#import "SSWebViewController.h"
#import "SSHTMLTemplate.h"

#import "SSIdeasCreateIdeaViewController.h"
#import "SSIdeasTableViewController.h"

#define kSSIdeasCompetitionsDetailsTeaserStyleKey @"competitions.details.teaser"
#define kSSIdeasCompetitionsDetailsTeaserBackgroundColorKey @"competitions.details.teaser.backgroundColor"

@interface SSIdeasCompetitionDetailsViewController () <DWImageViewDelegate>

@property (nonatomic, strong, readwrite) SSApiIdeasCompetition *competition;

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) DWImageView *teaserImageView;
@property (nonatomic, strong) DWLabel *teaserLabel;
@property (nonatomic, strong) UIView *headerView;

@end

@implementation SSIdeasCompetitionDetailsViewController

- (id)initWithCompetition:(SSApiIdeasCompetition *)competition {
	if ((self = [super init])) {
		self.competition = competition;
		self.title = competition.title;
	}
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.tableView.rowHeight = 80.0f;
	
	UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background_black"]];
	backgroundView.frame = self.view.bounds;
	backgroundView.contentMode = UIViewContentModeCenter;
	backgroundView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
	self.tableView.backgroundView = backgroundView;

	self.headerView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.bounds.size.width, 0.0f)];
	UIColor *teaserBackgroundColor = [UIColor colorFromCustomizationKey:kSSIdeasCompetitionsDetailsTeaserBackgroundColorKey];
	if (teaserBackgroundColor == nil) {
		teaserBackgroundColor = [UIColor whiteColor];
	}
	self.headerView.backgroundColor = teaserBackgroundColor;

	if (self.competition.titleImageURL) {
		self.teaserImageView = [[DWImageView alloc] initWithURL:self.competition.titleImageURL];
		self.teaserImageView.frame = CGRectMake(0.0f, 0.0f, self.headerView.bounds.size.width, 0.0f);
		self.teaserImageView.delegate = self;
		//self.teaserImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
		self.teaserImageView.layer.shadowColor = [UIColor blackColor].CGColor;
		self.teaserImageView.layer.shadowOffset = CGSizeMake(0.0f, -2.0f);
		self.teaserImageView.layer.shadowRadius = 7.0f;
		self.teaserImageView.layer.shadowOpacity = 0.5;
		
		[self.headerView addSubview:self.teaserImageView];
	}
	if (self.competition.teaserText.length > 0) {
		self.teaserLabel = [[DWLabel alloc] initWithFrame:CGRectMake(10.0f, 0.0f, self.headerView.bounds.size.width - 20.0f, 0.0f)];
		self.teaserLabel.textStyle = [DWTextStyle textStyleWithKey:kSSIdeasCompetitionsDetailsTeaserStyleKey];
		self.teaserLabel.text = self.competition.teaserText;
		self.teaserLabel.backgroundColor = [UIColor clearColor];
		self.teaserLabel.numberOfLines = 0;
		self.teaserLabel.lineBreakMode = NSLineBreakByWordWrapping;
		
		[self.headerView addSubview:self.teaserLabel];
	}

	[self resizeHeaderView];
	self.tableView.tableHeaderView = self.headerView;
	
	[self.competition performInitialIdeasLoadingWithCompletion:^(id data, NSError *error) {}];
	[self.competition refreshIdeasWithCompletion:^(id data, NSError *error) {
		[self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:2 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
	}];

}

#pragma mark - Layout

- (void)resizeHeaderView {
		
	if (self.teaserImageView.image) {
		CGFloat imageRatio = self.teaserImageView.image.size.width / self.teaserImageView.image.size.height;
		CGFloat imageWidth = self.teaserImageView.frame.size.width;
		CGFloat imageHeight = imageWidth / imageRatio;
		self.teaserImageView.frame = CGRectMake(0.0f, 0.0f, imageWidth, imageHeight);
		self.teaserImageView.hidden = NO;
	} else {
		self.teaserImageView.hidden = YES;
		self.teaserImageView.frame = CGRectMake(0.0f, 0.0f, self.teaserImageView.frame.size.width, 0.0f);
	}
	
	if (self.teaserLabel.text.length > 0) {
		[self.teaserLabel sizeToFit];
		self.teaserLabel.frame = CGRectMake(self.teaserLabel.frame.origin.x, CGRectGetMaxY(self.teaserImageView.frame) + 10.0f, self.teaserLabel.frame.size.width, self.teaserLabel.frame.size.height);
	} else {
		self.teaserLabel.frame = CGRectMake(self.teaserLabel.frame.origin.x, CGRectGetMaxY(self.teaserImageView.frame) + 10.0f, self.teaserLabel.frame.size.width, 0.0f);
	}
	
	
	CGRect headerFrame = self.headerView.frame;
	
	headerFrame.size.height = CGRectGetMaxY(self.teaserLabel.frame) + 10.0f;
	
	self.headerView.frame = headerFrame;

}

- (void)imageViewDidLoadRemoteImage:(DWImageView *)imageView {
	if (self.isViewLoaded) {
		[self resizeHeaderView];
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	SSTableViewCell *cell = (SSTableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:kSSTableViewCellRequseIdentifier];
	cell.style = SSTableViewCellStyleCompetitionDetails;
	

	NSString *cellTitle = nil;
	NSString *cellBadge = nil;
	switch (indexPath.row) {
		case 0:
			cellTitle = NSLocalizedString(@"Beschreibung", @"Competitions");
			break;
		case 1:
			cellTitle = NSLocalizedString(@"Regeln & Preise", @"Competitions");
			break;
		case 2:
			cellTitle = NSLocalizedString(@"Ideen ansehen", @"Competitions");
			cellBadge = (self.competition.ideas.count > 0) ? [NSString stringWithFormat:@"%i",self.competition.ideas.count] : nil;
			break;
		case 3:
			cellTitle = NSLocalizedString(@"Mitmachen", @"Competitions");
			break;
		default:
			break;
	}

	cell.titleLabel.text = cellTitle;
	cell.badge = cellBadge;
		
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.row == 0) {
		
		SSLog(@"Showing competition description (%@)",self.competition.title);
		
		SSWebViewController *browser = [[SSWebViewController alloc] initWithHTML:self.competition.descriptionTemplate.content];
		browser.title = NSLocalizedString(@"Beschreibung", @"General");
		[self.navigationController pushViewController:browser animated:YES];
	
		
	} else if (indexPath.row == 1) {
		
		SSLog(@"Showing competition rules (%@)",self.competition.title);
		
		SSWebViewController *browser = [[SSWebViewController alloc] initWithHTML:self.competition.rulesTemplate.content];
		browser.title = NSLocalizedString(@"Beschreibung", @"General");
		[self.navigationController pushViewController:browser animated:YES];
		
		
	} else if (indexPath.row == 2) {

		SSLog(@"Showing competition ideas (%@)",self.competition.title);
		
		SSIdeasTableViewController *ideasViewController = [[SSIdeasTableViewController alloc] initWithCompetition:self.competition];
		[self.navigationController pushViewController:ideasViewController animated:YES];
		
	} else if (indexPath.row == 3) {
		
		SSLog(@"Starting competition idea gathering (%@)",self.competition.title);
		
		SSIdeasCreateIdeaViewController *creationController = [[SSIdeasCreateIdeaViewController alloc] initWithCompetition:self.competition];
		[self.navigationController pushViewController:creationController animated:YES];
		
	}
}

@end

//
//  SSIdeasTableViewController.h
//  staffsale
//
//  Created by Daniel Wetzel on 16.11.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSModuleTableViewController.h"

#import "SSApiIdea.h"
#import "SSApiIdeasCompetition.h"

@interface SSIdeasTableViewController : SSModuleTableViewController

@property (nonatomic, strong, readonly) SSApiIdeasCompetition *competition;

- (id)initWithCompetition:(SSApiIdeasCompetition *)competition;

@end

//
//  SSIdeasTableViewController.m
//  staffsale
//
//  Created by Daniel Wetzel on 16.11.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSIdeasTableViewController.h"

#import "SSTableViewCell.h"
#import "SSApiUser.h"

#import "SSWebViewController.h"
#import "SSHTMLTemplate.h"

#import "NSDateFormatter+DWToolbox.h"

@interface SSIdeasTableViewController ()

@property (nonatomic, strong, readwrite) SSApiIdeasCompetition *competition;

@end

@implementation SSIdeasTableViewController

- (id)initWithCompetition:(SSApiIdeasCompetition *)competition {
	if ((self = [super init])) {
		self.competition = competition;
		self.title = NSLocalizedString(@"Ideen ansehen", @"Ideas");
	}
	return self;
}


- (void)viewDidLoad
{
	self.isDataController = YES;
	self.needsRefreshControl = YES;
    [super viewDidLoad];
	
	if (self.competition.isRefreshingIdeas) {
		[self.refreshControl beginRefreshing];
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (section == 0) {
		return self.competition.ideas.count;
	}
	return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	SSTableViewCell *cell = (SSTableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:kSSTableViewCellRequseIdentifier];
	cell.style = SSTableViewCellStyleIdeas;
	
	SSApiIdea *idea = [self.competition.ideas.allValues objectAtIndex:indexPath.row];
	cell.titleLabel.text = idea.text;

	if (idea.images.count > 0) {
		cell.titleImageURL = idea.images[0];
	} else {
		cell.titleImageURL = nil;
	}

	cell.info1Label.text = [[NSDateFormatter dateFormatterWithDateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle] stringFromDate:idea.submissionDate];
	cell.info3Label.text = [NSString stringWithFormat:@"%@ %@",idea.user.firstname, idea.user.lastname];
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	SSApiIdea *idea = [self.competition.ideas.allValues objectAtIndex:indexPath.row];
	
	if (idea) {
		SSWebViewController *browser = [[SSWebViewController alloc] initWithHTML:[idea ideaTemplate].content];
		browser.title = idea.user.fullname;
		[self.navigationController pushViewController:browser animated:YES];
	}
}

- (void)refresh {
	[self.competition refreshIdeasWithCompletion:^(id data, NSError *error) {
		[self showCurrentData];
	}];
}

@end
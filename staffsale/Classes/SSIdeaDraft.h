//
//  SSIdeaDraft.h
//  staffsale
//
//  Created by Daniel Wetzel on 12.11.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SSHTMLTemplate.h"

@class SSApiIdeasCompetition;

@interface SSIdeaDraft : NSObject

@property (nonatomic, weak) SSApiIdeasCompetition *competition;

@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSURL *URL;

- (SSHTMLTemplate *)draftTemplate;

@end

//
//  SSApiSubmit.m
//  staffsale
//
//  Created by Daniel Wetzel on 14.11.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiSubmit.h"

@interface SSApiSubmit ()

@property (nonatomic, readwrite) SSApiSubmitStatus status;
@property (nonatomic, strong, readwrite) NSString *message;

@end

@implementation SSApiSubmit

- (id)initWithXMLElement:(RXMLElement *)element {
	if ((self = [super initWithXMLElement:element])) {
		
		NSString *statusString = [[element child:@"status"] text];
		self.status = ([statusString.uppercaseString isEqualToString:@"OK"]) ? SSApiSubmitStatusSuccess : SSApiSubmitStatusError;
		self.message = [[element child:@"message"] text];
	}
	return self;
}

- (BOOL)isSuccessful {
	return (self.status == SSApiSubmitStatusSuccess);
}

@end

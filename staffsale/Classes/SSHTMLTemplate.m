//
//  SSHTMLTemplate.m
//  staffsale
//
//  Created by Daniel Wetzel on 26.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSHTMLTemplate.h"

#import "SSBackendConnection.h"
#import "DWConfiguration.h"

#import "DWTextStyle.h"

#import "NSDateFormatter+DWToolbox.h"

@interface SSHTMLTemplate ()

@property (nonatomic, strong) NSString *template;

@property (nonatomic, strong) NSMutableDictionary *fields;

@end

@implementation SSHTMLTemplate

- (id)initWithName:(NSString *)name {
	if ((self = [super init])) {
		
		NSString *fullName = [NSString stringWithFormat:@"%@.template",name];
		NSURL *templateURL = [[NSBundle mainBundle] URLForResource:fullName withExtension:@"html"];
		if (templateURL) {
			self.fields = [[NSMutableDictionary alloc] init];
			
			self.template = [[NSMutableString alloc] initWithContentsOfURL:templateURL
																			  encoding:NSUTF8StringEncoding
																				 error:nil];
			[self prepareGeneralAdjustments];
		} else {
			self = nil;
		}
	}
	return self;
}

- (void)prepareGeneralAdjustments {

	[self assignString:@"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0\">" forField:@"meta"];

	DWTextStyle *style = [DWTextStyle textStyleWithKey:@"html"];
	if (style == nil) {
		style = [[DWTextStyle alloc] init];
		style.font = [UIFont systemFontOfSize:14.0f];
		style.color = [UIColor colorWithHexString:@"#606060"];
	}
	
	[self assignString:style.font.fontName forField:@"font"];
	[self assignString:[NSString stringWithFormat:@"#%@",style.color.hexStringFromColor] forField:@"color"];
}

- (void)assignDate:(NSDate *)date forField:(NSString *)field style:(NSDateFormatterStyle)style {
	assert(field);
	[self assignDate:date forField:field dateStyle:style timeStyle:style];
}

- (void)assignDate:(NSDate *)date forField:(NSString *)field dateStyle:(NSDateFormatterStyle)dateStyle timeStyle:(NSDateFormatterStyle)timeStyle {
	assert(field);
	NSString *stringValue = [[NSDateFormatter dateFormatterWithDateStyle:dateStyle timeStyle:timeStyle] stringFromDate:date];
	[self assignString:stringValue forField:field];
}

- (void)assignString:(NSString *)value forField:(NSString *)field {
	assert(field);
	if (value == nil) {
		value = @"";
	}
	[self.fields setObject:value forKey:field];
}

- (void)appendString:(NSString *)value toField:(NSString *)field {
	assert(field);
	NSString *string = [self.fields objectForKey:field];
	if (string == nil) {
		string = @"";
	}
	string = [string stringByAppendingString:value];
	[self assignString:string forField:field];
}

- (NSString *)content {
	
	NSMutableString *content = [NSMutableString stringWithString:self.template];
	for (NSString *field in self.fields) {
		NSString *escapedField = [NSString stringWithFormat:@"##%@##",field.uppercaseString];
		[content replaceOccurrencesOfString:escapedField
								 withString:[self.fields objectForKey:field]
									options:NSCaseInsensitiveSearch
									  range:NSMakeRange(0, content.length)];
	}
	return content;
}

@end

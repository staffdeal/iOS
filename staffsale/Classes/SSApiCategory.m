//
//  SSApiNewsCategory.m
//  staffsale
//
//  Created by Daniel Wetzel on 26.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiCategory.h"
#import "SSApiCategory_Private.h"

#import "SSApiNewsModule.h"
#import "UIColor+DWToolbox.h"

@interface SSApiCategory ()

@property (nonatomic, strong, readwrite) NSString *identifier;
@property (nonatomic, strong, readwrite) NSString *parentIdentifier;

@property (nonatomic, strong, readwrite) NSString *title;
@property (nonatomic, strong, readwrite) NSString *slug;
@property (nonatomic, strong, readwrite) UIColor *color;

@property (nonatomic, strong, readwrite) NSArray *imageURLs;
@property (nonatomic, strong, readwrite) NSURL *titleImageURL;

@end

@implementation SSApiCategory

- (id)initWithXMLElement:(RXMLElement *)element {
	if ((self = [super initWithXMLElement:element])) {
		
		if ([element.tag isEqualToString:@"category"]) {
			
			self.identifier = [element attribute:@"id"];
			self.parentIdentifier = [[element child:@"parent"] text];
			
			self.title = [[element child:@"title"] text];
			self.slug = [[element child:@"slug"] text];

			NSMutableArray *imageURLs = [NSMutableArray array];
			[element iterate:@"images.image" usingBlock:^(RXMLElement *imageElement) {
				NSString *imageIdentifier = [imageElement attribute:@"id"];
				if (imageIdentifier) {
					NSURL *imageURL = [[NSURL alloc] initWithString:[imageElement text]];
					if ([imageIdentifier isEqualToString:@"titleImage"]) {
						self.titleImageURL = imageURL;
					} else {
						[imageURLs addObject:imageURL];
					}
				}
			}];
			if (imageURLs.count > 0) {
				self.imageURLs = [[NSArray alloc] initWithArray:imageURLs];
			}
			
		} else {
			self = nil;
		}
		
	}
	return self;
}

- (SSApiCategory *)parentCategory {
	if (self.parentIdentifier) {
		return [self.module categoryWithIdentifier:self.parentIdentifier];
	} else {
		return nil;
	}
}

@end

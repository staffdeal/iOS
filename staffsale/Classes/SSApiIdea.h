//
//  SSApiCompetitionIdea.h
//  staffsale
//
//  Created by Daniel Wetzel on 16.11.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiObject.h"

@class SSApiUser;
@class SSHTMLTemplate;

@interface SSApiIdea : SSApiObject

@property (nonatomic, strong, readonly) NSString *identifier;

@property (nonatomic, strong, readonly) NSString *text;
@property (nonatomic, strong, readonly) NSURL *link;
@property (nonatomic, strong, readonly) NSDate *submissionDate;
@property (nonatomic, strong, readonly) SSApiUser *user;

@property (nonatomic, strong, readonly) NSArray *images;

- (SSHTMLTemplate *)ideaTemplate;

@end

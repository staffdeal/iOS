//
//  SSApiUser_Private.h
//  staffsale
//
//  Created by Daniel Wetzel on 16.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiUser.h"

@interface SSApiUser()

@property (nonatomic, strong, readwrite) NSString *identifier;
@property (nonatomic, strong, readwrite) NSString *firstname;
@property (nonatomic, strong, readwrite) NSString *lastname;
@property (nonatomic, strong, readwrite) NSString *email;

@end

//
//  SSApiUser.m
//  staffsale
//
//  Created by Daniel Wetzel on 10.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiUser.h"
#import "SSApiUser_Private.h"

@implementation SSApiUser

- (id)initWithXMLElement:(RXMLElement *)element {
	if ((self = [super initWithXMLElement:element])) {
		
		if ([element.tag isEqualToString:@"user"]) {
			
			self.identifier = [element attribute:@"id"];
			
			self.firstname = [[element child:@"firstname"] text];
			self.lastname = [[element child:@"lastname"] text];
			self.email = [[element child:@"email"] text];
		} else {
			self = nil;
		}
	}
	return self;
}

- (NSString *)fullname {
	return [NSString stringWithFormat:@"%@ %@",self.firstname, self.lastname];
}

@end

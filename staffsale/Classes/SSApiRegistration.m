//
//  SSApiRegistration.m
//  staffsale
//
//  Created by Daniel Wetzel on 03.12.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiRegistration.h"

@interface SSApiRegistration ()

@property (nonatomic, strong, readwrite) NSString *path;
@property (nonatomic, strong, readwrite) NSString *hint;
@property (nonatomic, strong, readwrite) NSString *status;
@end

@implementation SSApiRegistration

- (id)initWithXMLElement:(RXMLElement *)element {
	if ((self = [super initWithXMLElement:element])) {
		self.path = [[element child:@"path"] text];
		self.hint = [[element child:@"hint"] text];
		
		self.status = [[element child:@"status"] text];
	}
	return self;
}

@end

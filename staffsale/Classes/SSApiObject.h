//
//  SSAPIObject.h
//  staffsale
//
//  Created by Daniel Wetzel on 09.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "RXMLElement.h"

@class SSApiModule;

@interface SSApiObject : NSObject

@property (nonatomic, strong, readonly) RXMLElement *element;

@property (nonatomic, weak, readonly) SSApiObject *parent;
@property (nonatomic, weak, readonly) SSApiModule *module;

+ (id)objectWithXMLElement:(RXMLElement *)element;

- (id)initWithXMLElement:(RXMLElement *)element;

@end

//
//  SSApiNews.h
//  staffsale
//
//  Created by Daniel Wetzel on 17.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiObject.h"

@class SSApiNewsCategory;
@class SSApiNewsModule;

@interface SSApiNews : SSApiObject

@property (nonatomic, strong, readonly) NSString *identifier;
@property (nonatomic, weak, readonly) SSApiNewsCategory *category;

@property (nonatomic, strong, readonly) NSString *title;
@property (nonatomic, strong, readonly) NSString *contentHTML;
@property (nonatomic, strong, readonly) NSDate *publishedDate;

@property (nonatomic, strong, readonly) NSURL *publicURL;

@property (nonatomic, strong, readonly) NSURL *titleImageURL;
@property (nonatomic, strong, readonly) NSDictionary *contentImageURLs;

@property (nonatomic, readonly) BOOL read;

- (NSString *)detailsHTML;
- (void)markAsRead;

@end

//
//  SSAPIObject.m
//  staffsale
//
//  Created by Daniel Wetzel on 09.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiObject.h"
#import "SSApiObject_Private.h"

#import "SSApiModule.h"

@interface SSApiObject()

@property (nonatomic, strong, readwrite) RXMLElement *element;

@end

@implementation SSApiObject

+ (id)objectWithXMLElement:(RXMLElement *)element {
	SSApiObject *object = nil;
	
	if (element) {
		NSString *tagName = element.tag;
		if (tagName.length > 2) {
			NSString *className = [NSString stringWithFormat:@"SSApi%@",[tagName capitalizedString]];
			Class elementClass = NSClassFromString(className);
			if (elementClass) {
				object = [[elementClass alloc] initWithXMLElement:element];
			}
		}
	}
	
	return object;
}

- (id)initWithXMLElement:(RXMLElement *)element {
	if ((self = [super init])) {
		self.element = element;
	}
	return self;
}

- (SSApiModule *)module {
	if (self->_module) {
		return self->_module;
	} else if ([self isKindOfClass:[SSApiModule class]]) {
		return self;
	} else {
		return self.parent.module;
	}
}

@end

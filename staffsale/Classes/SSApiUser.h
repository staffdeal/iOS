//
//  SSApiUser.h
//  staffsale
//
//  Created by Daniel Wetzel on 10.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiObject.h"

@interface SSApiUser : SSApiObject

@property (nonatomic, strong, readonly) NSString *identifier;

@property (nonatomic, strong, readonly) NSString *firstname;
@property (nonatomic, strong, readonly) NSString *lastname;
@property (nonatomic, strong, readonly) NSString *email;

- (NSString *)fullname;

@end

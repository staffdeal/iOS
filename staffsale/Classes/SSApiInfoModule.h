//
//  SSApiInfoModule.h
//  staffsale
//
//  Created by Daniel Wetzel on 23.11.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiModule.h"

@class SSBackendConnection;

@interface SSApiInfoModule : SSApiModule

@property (nonatomic, strong, readonly) NSArray *modules;

- (id)initWithConnection:(SSBackendConnection *)connection;

@end

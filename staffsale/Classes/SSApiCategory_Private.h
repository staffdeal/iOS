//
//  SSApiNewsCategory_Private.h
//  staffsale
//
//  Created by Daniel Wetzel on 26.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiNewsCategory.h"

@class SSApiModule;

@interface SSApiCategory ()

@property (nonatomic, weak, readwrite) SSApiModule *module;

@end

//
//  SSApiAuth.h
//  staffsale
//
//  Created by Daniel Wetzel on 15.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiObject.h"

@class SSApiUser;

@interface SSApiAuth : SSApiObject

@property (nonatomic, strong, readonly) NSString *status;
@property (nonatomic, strong, readonly) NSString *token;

@property (nonatomic, strong, readonly) SSApiUser *user;

@end

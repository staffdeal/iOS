//
//  SSApiDealsModule.h
//  staffsale
//
//  Created by Daniel Wetzel on 23.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiModule.h"

typedef void (^SSApiDealSavingHandler)(BOOL success);

@class SSApiDeal;

@interface SSApiDealsModule : SSApiModule

@property (nonatomic, strong, readonly) NSDictionary *deals;
@property (nonatomic, strong, readonly) NSDictionary *savedDeals;

- (void)refreshWithCompletion:(void (^)(NSDictionary *deals, NSError *error))completion;

- (void)saveDealForUsage:(SSApiDeal *)deal completion:(SSApiDealSavingHandler)handler;

@end

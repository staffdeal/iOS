//
//  SSApiInfo.m
//  staffsale
//
//  Created by Daniel Wetzel on 09.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiInfo.h"

#import "SSApiUser.h"

#import "SSApiModule_Private.h"

static SSApiInfo *latestInstance;

@interface SSApiInfo()

@property (nonatomic, strong, readwrite) NSString *clientName;
@property (nonatomic, strong, readwrite) NSString *clientMail;
@property (nonatomic, strong, readwrite) NSDictionary *modules;
@property (nonatomic, strong, readwrite) SSApiUser *user;
@property (nonatomic, strong, readwrite) SSApiRegistration *registration;

@end

@implementation SSApiInfo

- (id)initWithXMLElement:(RXMLElement *)element {
	if (self = [super initWithXMLElement:element]) {
		
		if ([element.tag isEqualToString:@"info"]) {
			
			self.clientName = [[element child:@"client.name"] text];
			self.clientMail = [[element child:@"client.email"] text];
			
			NSMutableDictionary *modules = [NSMutableDictionary dictionary];
			[element iterate:@"modules.module" usingBlock:^(RXMLElement *moduleElement) {
				
				NSString *moduleIdentifier = [moduleElement attribute:@"id"];
				if (moduleIdentifier) {
					
					NSString *moduleClassName = [NSString stringWithFormat:@"SSApi%@Module",[moduleIdentifier capitalizedString]];
					Class moduleClass = NSClassFromString(moduleClassName);
					if (moduleClass == nil) {
						moduleClass = [SSApiModule class];
					}
					
					if ([moduleClass isSubclassOfClass:[SSApiModule class]]) {
						SSApiModule *module = [[moduleClass alloc] initWithXMLElement:moduleElement];
						if (module) {
							[modules setObject:module forKey:module.identifier];
						}
					}
					
				}
			}];
			
			if (modules.count > 0) {
				self.modules = [NSDictionary dictionaryWithDictionary:modules];
			}
			
			RXMLElement *userElement = [element child:@"user"];
			if (userElement) {
				self.user = [[SSApiUser alloc] initWithXMLElement:userElement];
			}
			
			RXMLElement *registrationElement = [element child:@"registration"];
			if (registrationElement) {
				self.registration = [[SSApiRegistration alloc] initWithXMLElement:registrationElement];
			}
			
		latestInstance = self;
			
		}
		
	}
	return self;
	
}

+ (SSApiInfo *)latestInfoInstance {
	return latestInstance;
}

@end
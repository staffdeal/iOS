//
//  SSModuleTableViewController.m
//  staffsale
//
//  Created by Daniel Wetzel on 10.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSModuleTableViewController.h"

#import "SSTableViewCell.h"
#import "DWAlertView.h"

#import "DWActivityManager.h"

@interface SSModuleTableViewController ()

@end

@implementation SSModuleTableViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	
	if (self.needsRefreshControl) {
		self.refreshControl = [[UIRefreshControl alloc] init];
	} else {
		self.refreshControl = nil;
	}
	
	self.tableView.rowHeight = 80.0f;
	
	UINib *cellNib = [UINib nibWithNibName:@"SSTableViewCell" bundle:nil];
	[self.tableView registerNib:cellNib forCellReuseIdentifier:kSSTableViewCellRequseIdentifier];
	
	if (self.isDataController) {
		[self.refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
		
		if (self.needsFilterButton) {
			UIBarButtonItem *filterButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"funnel"]
																			 style:UIBarButtonItemStylePlain
																			target:self
																			action:@selector(filterButtonPressed)];
			self.navigationItem.leftBarButtonItem = filterButton;
		}
		
		UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background_black"]];
		backgroundView.frame = self.tableView.bounds;
		backgroundView.contentMode = UIViewContentModeCenter;
		self.tableView.backgroundView = backgroundView;
	}
	
	UIView *footer =[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.tableView.bounds.size.width, 0.0f)];
	footer.backgroundColor = [UIColor clearColor];
	self.tableView.tableFooterView = footer;
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(showCurrentData)
												 name:SSApiModuleFinishedRefreshNotification
											   object:self.apiModule];
}

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:SSApiModuleFinishedRefreshNotification
												  object:self.apiModule];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	if (self.apiModule.isRefreshing) {
		[self.refreshControl beginRefreshing];
	}
}

- (void)setApiModule:(SSApiModule *)apiModule {
	if (apiModule != self->_apiModule) {
		self->_apiModule = apiModule;
		self.title = self.apiModule.title;
	}
}

- (void)refresh {
	[self.apiModule refreshWithCompletion:^(id data, NSError *error) {
		if (error != nil) {
			[[DWActivityManager sharedManager] addActivityWithTitle:error.localizedDescription timeout:5.0f];
		}
	}];
}

- (void)showCurrentData {
	if (self.isViewLoaded) {
		[self.tableView reloadData];
		[self.refreshControl endRefreshing];
	}
}

- (void)filterButtonPressed {
	
	DWAlertView *alertView = [DWAlertView alertViewWithTitle:@"Filter" andMessage:@"Comming soon"];
	[alertView addActionWithTitle:@"OK"];
	[alertView show];
}

@end

//
//  SSApiInfoModule.m
//  staffsale
//
//  Created by Daniel Wetzel on 23.11.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiInfoModule.h"
#import "SSApiModule_Private.h"

#import "SSBackendConnection.h"
#import "SSApiInfo.h"

@interface SSApiInfoModule ()

@property (nonatomic, strong, readwrite) NSArray *modules;

@end

@implementation SSApiInfoModule

- (id)initWithConnection:(SSBackendConnection *)connection {
	if ((self = [super init])) {
		self.backendConnection = connection;
		self.path = @"info";
		self.identifier = @"info";
		self.title = @"info";
	}
	return self;
}

- (void)performInitalLoading:(void (^)(SSApiModule *, NSError *))completion {
	
	SSBackendResponse *cachedResponse = [self cachedResponseForKey:@"index"];
	if (cachedResponse) {
		self.modules = [cachedResponse.info.modules.allValues copy];
		completion(self,nil);
	} else {
		completion(nil, [NSError errorWithDomain:@"SSApiInfoModuleNoCachedDataErrorDomain" code:100 userInfo:nil]);
	}
	[self signalRefreshFinished];
}

- (void)refreshWithCompletion:(SSApiModuleRefreshHandler)completion {
	
	SSBackendRequest *request = [SSBackendRequest requestWithPath:self.path];
	[self.backendConnection sendRequest:request completion:^(SSBackendResponse *response, NSError *error) {
		
		if (error == nil) {
			
			[self cacheResponse:response forKey:@"index"];
			self.modules = [response.info.modules.allValues copy];
		}
		completion(self.modules, error);
		
	}];
	
}

@end

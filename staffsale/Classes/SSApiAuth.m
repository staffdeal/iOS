//
//  SSApiAuth.m
//  staffsale
//
//  Created by Daniel Wetzel on 15.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiAuth.h"

#import "SSApiUser.h"

@interface SSApiAuth()

@property (nonatomic, strong, readwrite) NSString *status;
@property (nonatomic, strong, readwrite) NSString *token;
@property (nonatomic, strong, readwrite) SSApiUser *user;

@end

@implementation SSApiAuth

- (id)initWithXMLElement:(RXMLElement *)element {
	if ((self = [super initWithXMLElement:element])) {
		
		RXMLElement *userElement = [element child:@"user"];
		if (userElement) {
			SSApiUser *user = [[SSApiUser alloc] initWithXMLElement:[element child:@"user"]];
			if (user) {
				self.user = user;
				self.status = [[element child:@"status"] text];
				self.token = [[element child:@"token"] text];
			}
		}
		if (self.status == nil) {
			self.status = @"Authentication error";
			self.token = nil;
		}
	}
	return self;
}

@end

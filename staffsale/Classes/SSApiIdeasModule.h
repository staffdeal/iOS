//
//  SSApiIdeasModule.h
//  staffsale
//
//  Created by Daniel Wetzel on 23.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiModule.h"

@class SSApiIdeasCompetition;

@interface SSApiIdeasModule : SSApiModule

@property (nonatomic, strong, readonly) NSDictionary *competitions;

@end

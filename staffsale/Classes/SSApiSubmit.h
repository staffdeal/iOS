//
//  SSApiSubmit.h
//  staffsale
//
//  Created by Daniel Wetzel on 14.11.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiObject.h"

typedef enum {
	SSApiSubmitStatusError,
	SSApiSubmitStatusSuccess
} SSApiSubmitStatus;


@interface SSApiSubmit : SSApiObject

@property (nonatomic, readonly) SSApiSubmitStatus status;
@property (nonatomic, strong, readonly) NSString *message;

@property (nonatomic, readonly) BOOL isSuccessful;

@end

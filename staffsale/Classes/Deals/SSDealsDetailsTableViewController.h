//
//  SSDealsDetailsTableViewController.h
//  staffsale
//
//  Created by Daniel Wetzel on 20.11.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSModuleTableViewController.h"

@class SSApiDeal;

@interface SSDealsDetailsTableViewController : SSModuleTableViewController

@property (nonatomic, strong, readonly) SSApiDeal *deal;

- (id)initWithDeal:(SSApiDeal *)deal;

@end

//
//  SSDealsCodeViewController.m
//  staffsale
//
//  Created by Daniel Wetzel on 21.11.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSDealsCodeViewController.h"

@interface SSDealsCodeViewController ()

@property (nonatomic, copy) NSString *textCode;
@property (nonatomic, strong) UIImage *barCode;

@end

@implementation SSDealsCodeViewController

- (id)initWithCode:(id)code {
	if ((self = [super initWithStyle:UITableViewStyleGrouped])) {
		if ([code isKindOfClass:[NSString class]]) {
			self.textCode = code;
			self.title = NSLocalizedString(@"Online-Code", @"Deals");
		} else if ([code isKindOfClass:[UIImage class]]) {
			self.barCode = code;
			self.title = NSLocalizedString(@"Coupon-Code", @"Deals");
		} else {
			self = nil;
		}
	}
	return self;
}

- (id)initWithTextCode:(NSString *)code {
	return [self initWithCode:code];
}

- (id)initWithBarcode:(UIImage *)code {
	return [self initWithCode:code];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.tableView.bounces = NO;
	self.tableView.scrollEnabled = NO;
	
	if (self.textCode) {
		self.tableView.rowHeight = 60.0f;
	} else if (self.barCode) {
		self.tableView.rowHeight = [self barCodeSize].height;
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
	
	if (self.textCode) {
		return NSLocalizedString(@"Benutzen Sie diesen Coupon-Code bei Ihrer Online-Bestellung.", @"Deals");
	} else if (self.barCode) {
		return NSLocalizedString(@"Zeigen Sie diesen Code beim Kauf in der Filiale an der Kasse.", @"Deals");
	}
	return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGSize)barCodeSize {
	CGFloat ratio = self.barCode.size.width / self.barCode.size.height;
	CGFloat width = 300.0f;
	CGFloat height = width * ratio;
	return CGSizeMake(width, height);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
	if (self.textCode) {
		cell.textLabel.text = self.textCode;
		cell.textLabel.font = [UIFont fontWithName:@"Courier-Bold" size:22.0f];
		cell.textLabel.textAlignment = NSTextAlignmentCenter;
	} else if (self.barCode) {
		
		UIImageView *barcodeView = [[UIImageView alloc] initWithImage:self.barCode];
		CGSize barcodeSize = [self barCodeSize];
		barcodeView.frame = CGRectMake(0.0f, 0.0f, barcodeSize.width, barcodeSize.height);
		barcodeView.backgroundColor = [UIColor clearColor];
		[cell.contentView addSubview:barcodeView];
	}
    
    return cell;
}



@end

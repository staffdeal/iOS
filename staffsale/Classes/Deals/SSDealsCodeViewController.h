//
//  SSDealsCodeViewController.h
//  staffsale
//
//  Created by Daniel Wetzel on 21.11.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSDealsCodeViewController : UITableViewController

- (id)initWithCode:(id)code;
- (id)initWithTextCode:(NSString *)code;
- (id)initWithBarcode:(UIImage *)code;

@end

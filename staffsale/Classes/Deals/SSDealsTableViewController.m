//
//  SSDealsTableViewController.m
//  staffsale
//
//  Created by Daniel Wetzel on 02.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSDealsTableViewController.h"

#import "SSApiDealsModule.h"
#import "SSApiDeal.h"

#import "SSTableViewCell.h"

#import "SSDealsDetailsTableViewController.h"

#define kSSDealsShowSavedKey @"showSavedDeals"

@interface SSDealsTableViewController ()

@property (nonatomic, assign) BOOL showSavedDeals;
@property (nonatomic, readonly) SSApiDealsModule *dealsModule;

@property (nonatomic, readonly) NSDictionary *usedDeals;

@end

@implementation SSDealsTableViewController

- (SSApiDealsModule *)dealsModule {
	return (SSApiDealsModule *)self.apiModule;
}

- (NSDictionary *)usedDeals {
	if (self.showSavedDeals) {
		return self.dealsModule.savedDeals;
	} else {
		return self.dealsModule.deals;
	}
}

- (void)setShowSavedDeals:(BOOL)showSavedDeals {
	if (self.showSavedDeals != showSavedDeals) {
		self->_showSavedDeals = showSavedDeals;
		
		[[NSUserDefaults standardUserDefaults] setBool:self.showSavedDeals forKey:kSSDealsShowSavedKey];
		if (self.isViewLoaded) {
			[self applyFilterTitle];
			[self.tableView reloadData];
		}
	}
}

- (void)viewDidLoad
{
	self.isDataController = YES;
    self.needsFilterButton = NO;
	self.needsRefreshControl = YES;
	[super viewDidLoad];
	
	UIBarButtonItem *filterButton = [[UIBarButtonItem alloc] initWithTitle:@""
																	 style:UIBarButtonItemStylePlain
																	target:self
																	action:@selector(toggleFilter)];
	self.navigationItem.leftBarButtonItem = filterButton;
	
	self.showSavedDeals = [[NSUserDefaults standardUserDefaults] boolForKey:kSSDealsShowSavedKey];
	[self applyFilterTitle];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)applyFilterTitle {
	NSString *filterTitle = (self.showSavedDeals) ? NSLocalizedString(@"Neue Deals", @"Deals") : NSLocalizedString(@"Gesicherte Deals", @"Deals");
	self.navigationItem.leftBarButtonItem.title = filterTitle;
}

- (void)toggleFilter {
	self.showSavedDeals = !self.showSavedDeals;
	SSLog(@"Switching deals filter to %@ deals", (self.showSavedDeals) ? @"saved" : @"new");
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self.usedDeals.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SSTableViewCell *cell = (SSTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kSSTableViewCellRequseIdentifier forIndexPath:indexPath];
	cell.style = SSTableViewCellStyleIdeas;
	
	SSApiDeal *deal = [self.usedDeals.allValues objectAtIndex:indexPath.row];
	
	cell.titleLabel.text = deal.title;
	cell.info1Label.date = deal.endDate;
    cell.info3Label.text = deal.provider;
	
	if (deal.images.count > 0) {
		cell.titleImageURL = deal.images[0];
	} else {
		cell.titleImageURL = nil;
	}
	
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	SSApiDeal *deal = [self.usedDeals.allValues objectAtIndex:indexPath.row];
	
	SSDealsDetailsTableViewController *detailsController = [[SSDealsDetailsTableViewController alloc] initWithDeal:deal];
	[self.navigationController pushViewController:detailsController animated:YES];
}

@end

//
//  SSDealsDetailsTableViewController.m
//  staffsale
//
//  Created by Daniel Wetzel on 20.11.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSDealsDetailsTableViewController.h"

#import <QuartzCore/QuartzCore.h>

#import "SSApiDeal.h"
#import "SSApiDealPass.h"

#import "DWImageView.h"
#import "DWLabel.h"
#import "NSDateFormatter+DWToolbox.h"
#import "DWTextStyle.h"
#import "UIColor+DWToolbox.h"
#import "DWAlertView.h"

#import "SSTableViewCell.h"
#import "SSWebViewController.h"
#import "SSHTMLTemplate.h"

#import "DWActivityManager.h"

#import "SSDealsCodeViewController.h"

#define _functionOnline @"online"
#define _functionOnsite @"onsite"
#define _functionBranches @"branches"

@interface SSDealsDetailsTableViewController ()

@property (nonatomic, strong, readwrite) SSApiDeal *deal;

@property (nonatomic, readonly) UIView *headerView;

@property (nonatomic, strong) NSArray *functions;

@end

@implementation SSDealsDetailsTableViewController

- (id)initWithDeal:(SSApiDeal *)deal {
	if ((self = [super init])) {
		self.deal = deal;
		self.title = deal.title;
		
		NSMutableArray *functions = [NSMutableArray array];
		if (self.deal.onlinePass) {
			[functions addObject:_functionOnline];
		}
		if (self.deal.onsitePass) {
			[functions addObject:_functionOnsite];
		}
		
		// TODO: Branches still missing
		[functions addObject:_functionBranches];
		
		self.functions = (functions.count > 0) ? [[NSArray alloc] initWithArray:functions] : nil;
	}
	return self;
}

- (UIView *)headerView {
	UIView *headerView = self.tableView.tableHeaderView;
	if (headerView == nil) {
		headerView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 20.0f)];
		self.tableView.tableHeaderView = headerView;
	}
	return headerView;
}

- (void)viewDidLoad
{
	self.isDataController = YES;
	self.needsFilterButton = NO;
	self.needsRefreshControl = NO;
    [super viewDidLoad];
	
	
	CGFloat headerHeight = 110.0f;
	CGFloat margin = 5.0f;
	
	UIColor *backgroundColor = [UIColor colorFromCustomizationKey:@"deals.backgroundColor"];
	if (backgroundColor == nil) {
		backgroundColor = [UIColor whiteColor];
	}
	
	UIColor *teaserBackgroundColor = [UIColor colorFromCustomizationKey:@"deals.teaser.backgroundColor"];
	if (teaserBackgroundColor == nil) {
		teaserBackgroundColor = [backgroundColor colorByDarkeningTo:0.6f];
	}
	
	self.headerView.backgroundColor = teaserBackgroundColor;
	self.headerView.frame = (CGRect){self.headerView.frame.origin,{self.headerView.frame.size.width, headerHeight}};	// 100
	
	UIView *topShadowView = [[UIView alloc] init];
	topShadowView.backgroundColor = backgroundColor;
	topShadowView.layer.shadowColor = [UIColor blackColor].CGColor;
	topShadowView.layer.shadowOffset = CGSizeMake(0.0f, -2.0f);
	topShadowView.layer.shadowRadius = 7.0f;
	topShadowView.layer.shadowOpacity = 0.5;
	[self.headerView addSubview:topShadowView];
	
	CGFloat X = margin;
	CGFloat Y = margin;
	CGFloat W = self.view.bounds.size.width - Y - Y;
	CGFloat teaserY = 0.0f;
	
	DWTextStyle *validityStyle = [[DWTextStyle alloc] initWithCustomizationKey:@"deals.validity"];
	if (validityStyle.font == nil) {
		validityStyle.font = [UIFont systemFontOfSize:14.0f];
	}
	CGFloat validityLabelHeight = validityStyle.font.lineHeight;
	
	DWTextStyle *featuredStyle = [[DWTextStyle alloc] initWithCustomizationKey:@"deals.featured"];
	if (featuredStyle.font == nil) {
		featuredStyle.font = [UIFont systemFontOfSize:14.0f];
	}
	CGFloat featuredLabelHeight = featuredStyle.font.lineHeight;
	
	if (self.deal.images.count > 0) {
		DWImageView *imageView = [[DWImageView alloc] initWithFrame:CGRectMake(X, Y, 100.0f, self.headerView.frame.size.height - X * 2.0f)];
		imageView.backgroundColor = [UIColor clearColor];
		imageView.autoresizingMask = UIViewAutoresizingNone;
		imageView.contentMode = UIViewContentModeScaleAspectFill;
		imageView.layer.cornerRadius = 5.0f;
		imageView.layer.masksToBounds = YES;
		UIColor *borderColor = [UIColor colorFromCustomizationKey:@"deals.image.border.color"];
		if (borderColor) {
			imageView.layer.borderColor = borderColor.CGColor;
			imageView.layer.borderWidth = 1.0f;
		}
		imageView.URL = [self.deal.images objectAtIndex:0];
		[self.headerView addSubview:imageView];
		X = CGRectGetMaxX(imageView.frame) + margin;
		W = W - X + margin;
	}
	
	CGFloat titleHeight = self.headerView.frame.size.height - (2 * margin);
	
	DWLabel *validityLabel = [[DWLabel alloc] initWithFrame:CGRectMake(X, Y, W, validityLabelHeight)];
	validityLabel.backgroundColor = [UIColor clearColor];
	validityLabel.autoresizingMask = UIViewAutoresizingNone;
	validityLabel.verticalAlignment = DWLabelVerticalAlignmentTop;
	validityLabel.textAlignment = NSTextAlignmentRight;
	validityLabel.textStyle = validityStyle;
	NSString *validityDateString = [[NSDateFormatter dateFormatterWithDateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle] stringFromDate:self.deal.endDate];
	validityLabel.text = [NSString stringWithFormat:NSLocalizedString(@"gültig bis %@", @"Deals"),validityDateString];
	[self.headerView addSubview:validityLabel];
	Y = CGRectGetMaxY(validityLabel.frame) + margin;
	titleHeight = titleHeight - CGRectGetHeight(validityLabel.frame) - margin;
	
	if (self.deal.isFeatured) {
		DWLabel *featuredLabel = [[DWLabel alloc] initWithFrame:CGRectMake(X, self.headerView.frame.size.height - featuredLabelHeight - margin, W, featuredLabelHeight)];
		featuredLabel.backgroundColor = [UIColor clearColor];
		featuredLabel.textAlignment = NSTextAlignmentRight;
		featuredLabel.verticalAlignment = DWLabelVerticalAlignmentBottom;
		featuredLabel.textStyle = featuredStyle;
		featuredLabel.text = NSLocalizedString(@"Deal des Monats", @"Deals");
		[self.headerView addSubview:featuredLabel];
		titleHeight = titleHeight - CGRectGetHeight(featuredLabel.frame) - margin;
		teaserY = CGRectGetMaxY(featuredLabel.frame) + margin;
	}
	
	DWLabel *titleLabel = [[DWLabel alloc] initWithFrame:CGRectMake(X, Y, W, titleHeight)];
	titleLabel.backgroundColor = [UIColor clearColor];
	titleLabel.textAlignment = NSTextAlignmentRight;
	titleLabel.textStyleKey = @"deals.title";
	titleLabel.numberOfLines = 3;
	titleLabel.text = self.deal.title;
	[self.headerView addSubview:titleLabel];
	if (teaserY == 0) {
		teaserY = CGRectGetMaxY(titleLabel.frame) + margin;
	}
	
	topShadowView.frame = CGRectMake(0.0f, 0.0f, self.headerView.bounds.size.width, teaserY);
	
	if (self.deal.teaser.length > 0) {
		DWLabel *teaserLabel = [[DWLabel alloc] initWithFrame:CGRectMake(margin, teaserY + margin, self.headerView.bounds.size.width - (2 * margin), 20.0f)];
		teaserLabel.backgroundColor = [UIColor clearColor];
		teaserLabel.textStyleKey = @"deals.teaser";
		teaserLabel.text = self.deal.teaser;
		teaserLabel.numberOfLines = 0;
		teaserLabel.lineBreakMode = NSLineBreakByWordWrapping;
		[self.headerView addSubview:teaserLabel];
		
		CGSize teaserSize = [teaserLabel.text sizeWithFont:teaserLabel.font
										 constrainedToSize:CGSizeMake(teaserLabel.frame.size.width, CGFLOAT_MAX)
											 lineBreakMode:NSLineBreakByWordWrapping];
		teaserLabel.frame = (CGRect){teaserLabel.frame.origin,teaserSize};
		
		self.headerView.frame = (CGRect){self.headerView.frame.origin,{self.headerView.frame.size.width, CGRectGetMaxY(teaserLabel.frame) + margin}};
	}
	
	self.tableView.tableHeaderView = self.headerView;
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (section == 0) {
		if (self.deal.isSaved) {
			return self.functions.count + 1;
		} else {
			return 2;
		}
	}
	return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	SSTableViewCell *cell = (SSTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kSSTableViewCellRequseIdentifier forIndexPath:indexPath];
	cell.style = SSTableViewCellStyleDealDetails;
	
	if (indexPath.row == 0) {
		cell.titleLabel.text = NSLocalizedString(@"Beschreibung", @"Deals");
	} else {
		if (self.deal.isSaved) {
			NSInteger functionIndex = indexPath.row - 1;
			if (self.functions.count > functionIndex) {
				NSString *function = [self.functions objectAtIndex:functionIndex];
				
				if ([function isEqualToString:_functionOnline]) {
					
					cell.titleLabel.text = NSLocalizedString(@"Online einlösen", @"Deals");
					
				} else if ([function isEqualToString:_functionOnsite]) {
					
					cell.titleLabel.text = NSLocalizedString(@"Vor Ort einlösen", @"Deals");
					
				} else if ([function isEqualToString:_functionBranches]) {
					
					cell.titleLabel.text = NSLocalizedString(@"Filiale suchen", @"Deals");
					
				}
			}
		} else {
			cell.titleLabel.text = NSLocalizedString(@"Jetzt sichern", @"Deals");
		}
	}
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if (indexPath.row == 0) {
		SSWebViewController *browser = [[SSWebViewController alloc] initWithHTML:self.deal.descriptionTemplate.content];
		browser.title = self.deal.title;
		[self.navigationController pushViewController:browser animated:YES];
	} else {
		if (self.deal.isSaved) {
			NSInteger functionIndex = indexPath.row - 1;
			if (self.functions.count > functionIndex) {
				NSString *function = [self.functions objectAtIndex:functionIndex];
				
				if ([function isEqualToString:_functionOnline]) {

					[self.deal.onlinePass requestCodeWithCompletion:^(id code, SSApiDealPassMethod method, NSError *error) {
						SSDealsCodeViewController *codeViewController = [[SSDealsCodeViewController alloc] initWithCode:code];
						[self.navigationController pushViewController:codeViewController animated:YES];
					}];
					
				} else if ([function isEqualToString:_functionOnsite]) {
					
					[self.deal.onsitePass requestCodeWithCompletion:^(id code, SSApiDealPassMethod method, NSError *error) {
						SSDealsCodeViewController *codeViewController = [[SSDealsCodeViewController alloc] initWithCode:code];
						[self.navigationController pushViewController:codeViewController animated:YES];
					}];
					
				} else if ([function isEqualToString:_functionBranches]) {
					
					DWAlertView *hintView = [[DWAlertView alloc] initWithTitle:@"Branches" andMessage:@"Comming soon"];
					[hintView addActionWithTitle:@"OK" block:^{
						[self.tableView deselectRowAtIndexPath:indexPath animated:YES];
					}];
					[hintView show];
				}
			}
		} else {
			
			if (indexPath.row == 1) {
				DWActivityItem *activity = [[DWActivityManager sharedManager] addActivityWithTitle:NSLocalizedString(@"wird gesichert …", @"Deals")];
				[self.deal saveWithCompletion:^(BOOL success) {
					[activity hide];
					if (success) {
						[self showCurrentData];
					} else {
						DWAlertView *errorAlert = [[DWAlertView alloc] initWithTitle:self.deal.module.title
																		  andMessage:NSLocalizedString(@"Der Deal konnte nicht gesichert werden", @"Deals")];
						[errorAlert show];
					}
				}];
			}
			
		}
	}
	
}

@end

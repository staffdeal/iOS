//
//  SSAppDelegate.h
//  staffsale
//
//  Created by Daniel Wetzel on 19.09.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SSNewsTableViewController;
@class SSCompetitionsTableViewController;
@class SSDealsTableViewController;
@class SSSettingsTableViewController;

@class SSApiInfoModule;

@interface SSAppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) SSApiInfoModule *infoModule;

@property (nonatomic, weak, readonly) SSNewsTableViewController *newsViewController;
@property (nonatomic, weak, readonly) SSCompetitionsTableViewController *ideasViewController;
@property (nonatomic, weak, readonly) SSDealsTableViewController *dealsViewController;
@property (nonatomic, weak, readonly) SSSettingsTableViewController *settingsViewController;

- (void)startLogin;

@end

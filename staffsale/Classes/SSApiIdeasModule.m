//
//  SSApiIdeasModule.m
//  staffsale
//
//  Created by Daniel Wetzel on 23.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiIdeasModule.h"
#import "SSApiModule_Private.h"

#import "SSApiCategory_Private.h"

#import "SSApiIdeasCompetition.h"
#import "SSApiIdeasCompetition_Private.h"

#import "SSApiObject_Private.h"

#import "SSBackendRequest.h"

#import "RXMLElement.h"

@interface SSApiIdeasModule()

@property (nonatomic, strong, readwrite) NSDictionary *competitions;

@end

@implementation SSApiIdeasModule

- (id)initWithXMLElement:(RXMLElement *)element {
	if ((self = [super initWithXMLElement:element])) {
		self.competitions = [[NSDictionary alloc] init];
	}
	return self;
}

- (NSString *)competitionsPath {
	return [NSString stringWithFormat:@"%@/competitions",self.path];
}

- (NSString *)ideasPath {
	return [NSString stringWithFormat:@"%@/ideas",self.path];
}

- (void)performInitalLoading:(void (^)(SSApiModule *, NSError *))completion {
	
	SSBackendResponse *response = [self cachedResponseForKey:@"competitions"];
	if (response) {
		if (response.element && response.error == nil) {
			[self importIdeasFromElement:response.element];
		}
		completion(self,nil);
	} else {
		completion(self,[NSError errorWithDomain:@"SSIdeasModuleInitialLoadingFailedErrorDomain" code:600 userInfo:nil]);
	}
}

- (void)refreshWithCompletion:(void (^)(id data, NSError *error))completion {
	if (self.isRefreshing == NO) {
		
		[super refreshWithCompletion:completion];
		
		SSBackendRequest *request = [SSBackendRequest requestWithPath:[self competitionsPath]];
		[self.backendConnection sendRequest:request completion:^(SSBackendResponse *response, NSError *error) {
			if (error == nil) {
				
				dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
					
					[self cacheResponse:response forKey:@"competitions"];
					
					[self importIdeasFromElement:response.element];
					dispatch_async(dispatch_get_main_queue(), ^{
						completion(self.competitions,error);
						[self signalRefreshFinished];
					});
				});
				
			} else {
				completion(nil,error);
				[self signalRefreshFinished];
			}
		}];
		
	}
}

- (void)importIdeasFromElement:(RXMLElement *)element {
	
	RXMLElement *categoriesElement = [element child:@"categories"];
	if (categoriesElement) {
		NSArray *categoryElements = [categoriesElement children:@"category"];
		for (RXMLElement *categoryElement in categoryElements) {
			SSApiCategory *category = [[SSApiCategory alloc] initWithXMLElement:categoryElement];
			if (category) {
				category.module = self;
				[self addCategory:category];
			}
		}
	}
	
	RXMLElement *competitionsElement = [element child:@"competitions"];
	NSMutableDictionary *receivedCompetitions = [NSMutableDictionary dictionary];
	[competitionsElement iterate:@"competition" usingBlock:^(RXMLElement *competitionElement) {
		
		SSApiIdeasCompetition *competition = [[SSApiIdeasCompetition alloc] initWithXMLElement:competitionElement];
		if (competition) {
			competition.category = [self categoryWithIdentifier:competition.categoryIdentifier];
			competition.module = self;
			[receivedCompetitions setObject:competition forKey:competition.identifier];
		}
	}];
	self.competitions = [[NSDictionary alloc] initWithDictionary:receivedCompetitions];
}

@end
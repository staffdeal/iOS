//
//  SSApiRegistration.h
//  staffsale
//
//  Created by Daniel Wetzel on 03.12.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiObject.h"

@interface SSApiRegistration : SSApiObject

@property (nonatomic, strong, readonly) NSString *path;
@property (nonatomic, strong, readonly) NSString *hint;

@property (nonatomic, strong, readonly) NSString *status;

@end

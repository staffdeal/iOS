//
//  SSApiDealPass.m
//  staffsale
//
//  Created by Daniel Wetzel on 16.11.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiDealPass.h"

#import "QRCodeGenerator.h"

@interface SSApiDealPass ()

@property (nonatomic, readwrite) SSApiDealPassType type;
@property (nonatomic, readwrite) SSApiDealPassMethod method;
@property (nonatomic, strong, readwrite) NSURL *URL;

@property (nonatomic, strong) NSString *receivedCode;
@property (nonatomic, strong) UIImage *receivedBarcode;
@property (nonatomic, assign) SSApiDealPassMethod receivedMethod;

@end

@implementation SSApiDealPass

- (id)initWithXMLElement:(RXMLElement *)element {
	if ((self = [super initWithXMLElement:element])) {
		
		if ([element.tag isEqualToString:@"online"] ||
			[element.tag isEqualToString:@"onsite"]) {
			
			self.method = SSApiDealPassMethodText;
			
			NSString *URLString = [[element child:@"url"] text];
			if (URLString) {
				self.URL = [[NSURL alloc] initWithString:URLString];
			}
			
			RXMLElement *detailsElement = nil;
			
			RXMLElement *staticElement = [element child:@"static"];
			if (staticElement) {
				detailsElement = staticElement;
				self.type = SSApiDealPassTypeStatic;
			}
			
			NSString *method = [detailsElement attribute:@"method"].lowercaseString;
			if ([method isEqualToString:@"qr"]) {
				self.method = SSApiDealPassMethodQR;
			}
			
			if (self.type == SSApiDealPassTypeStatic) {
				self.receivedMethod = self.method;
				self.receivedCode = [[detailsElement child:@"code"] text];
			}
			
		}
	}
	return self;
}

- (void)requestCodeWithCompletion:(SSApiDealCodeRequestHandler)completion {
	
	dispatch_async(dispatch_get_main_queue(), ^{
		
		id code = nil;
		
		if (self.method == SSApiDealPassMethodText) {
			code = self.receivedCode;
		} else if (self.method == SSApiDealPassMethodQR) {
			
			if (self.receivedBarcode == nil) {
				self.receivedBarcode = [QRCodeGenerator qrImageForString:self.receivedCode imageSize:300.0f];
			}
			code = self.receivedBarcode;
		}
		
		completion(code, self.receivedMethod, nil);
	});
}

@end

//
//  SSApiNews.m
//  staffsale
//
//  Created by Daniel Wetzel on 17.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiNews.h"
#import "SSApiNews_Private.h"

#import "SSBackendConnection.h"
#import "SSApiNewsCategory.h"

#import "SSHTMLTemplate.h"

#import "DWURLDownload.h"

#import "SSApiNewsModule.h"
#import "SSApiModule.h"

@interface SSApiNews ()

@property (nonatomic, strong, readwrite) NSString *identifier;

@property (nonatomic, strong, readwrite) NSString *title;
@property (nonatomic, strong, readwrite) NSString *contentHTML;
@property (nonatomic, strong, readwrite) NSDate *publishedDate;

@property (nonatomic, strong, readwrite) NSURL *publicURL;

@property (nonatomic, strong, readwrite) NSURL *titleImageURL;
@property (nonatomic, strong, readwrite) NSDictionary *contentImageURLs;

@property (nonatomic, readonly) SSApiNewsModule *newsModule;

@end

@implementation SSApiNews

- (id)initWithXMLElement:(RXMLElement *)element {
	if ((self = [super initWithXMLElement:element])) {
		
		if ([element.tag isEqualToString:@"newsItem"]) {
			
			self.identifier = [element attribute:@"id"];
			self.categoryIdentifier = [[element child:@"category"] text];
			
			
			self.title = [[element child:@"title"] text];
			self.publishedDate = [[SSBackendConnection dateFormatter] dateFromString:[[element child:@"published"] text]];
			self.contentHTML = [[element child:@"text"] text];
			
			NSMutableDictionary *contentImageURLs = [NSMutableDictionary dictionary];
			self.contentImageURLs = nil;
			
			[element iterate:@"images.image" usingBlock:^(RXMLElement *imageElement) {
				if ([imageElement.tag isEqualToString:@"image"]) {
					
					NSString *imageIdentifier = [imageElement attribute:@"id"];
					NSString *imageURLString = [imageElement text];
					NSURL *imageURL = nil;
					if (imageURLString) {
						imageURL = [NSURL URLWithString:imageURLString];
					}
					
					if (imageURL) {
						if ([imageIdentifier isEqualToString:@"title"]) {
							self.titleImageURL = imageURL;
						} else {
							[contentImageURLs setObject:imageURL forKey:imageIdentifier];
						}
					}
				}
			}];
			
			if (contentImageURLs.count > 0) {
				self.contentImageURLs = [[NSDictionary alloc] initWithDictionary:contentImageURLs];
			}
			
			NSString *titleImageURLString = [[element child:@"titleImage"] text];
			if (titleImageURLString) {
				self.titleImageURL = [[NSURL alloc] initWithString:titleImageURLString];
			}
			
			NSString *publicURLString = [[element child:@"url"] text];
			if (publicURLString.length > 6) {
				self.publicURL = [[NSURL alloc] initWithString:publicURLString];
			}
			
		} else {
			self = nil;
		}
		
	}
	return self;
}

- (SSApiNewsModule *)newsModule {
	return (SSApiNewsModule *)self.module;
}

- (NSString *)detailsHTML {
	
	SSHTMLTemplate *template = [[SSHTMLTemplate alloc] initWithName:@"news"];
	if (template) {
		[template assignString:self.title forField:@"title"];
		[template assignString:[[SSBackendConnection dateFormatter] stringFromDate:self.publishedDate] forField:@"published"];
		[template assignString:self.contentHTML forField:@"content"];
		
		if (self.titleImageURL) {
			[template assignString:@"block" forField:@"titleImageDivDisplay"];
			[template assignString:[self.titleImageURL absoluteString] forField:@"titleImageURL"];
		} else {
			[template assignString:@"none" forField:@"titleImageDivDisplay"];
		}
		
		if (self.contentImageURLs.count > 0) {
			[template assignString:@"block" forField:@"contentImagesDivDisplay"];
			NSMutableString *imagesString = [NSMutableString string];
			for (NSString *imageIdentifier in self.contentImageURLs) {
				NSURL *imageURL = [self.contentImageURLs objectForKey:imageIdentifier];
				[imagesString appendFormat:@"<img class=\"contentImage\" src=\"%@\" />",imageURL.absoluteString];
			}
			[template assignString:imagesString forField:@"contentImages"];
		} else {
			[template assignString:@"none" forField:@"contentImagesDivDisplay"];
		}
		
		
		NSString *categoryString = nil;
		if (self.category) {
			SSApiCategory *parent = self.category;
			NSMutableArray *parents = [NSMutableArray arrayWithObject:parent.title];
			do {
				parent = parent.parentCategory;
				if (parent != nil) {
					[parents addObject:parent.title];
				}
			} while (parent != nil);
			categoryString = [parents.reverseObjectEnumerator.allObjects componentsJoinedByString:@" > "];
		} else {
			categoryString = NSLocalizedString(@"Nicht zugeordnet", @"News");
		}
		[template assignString:categoryString forField:@"category"];

		
		return template.content;
		
	}
	return nil;
}

- (BOOL)read {
	return [self.newsModule hasItemWithIdentifierBeenRead:self.identifier];
}

- (void)markAsRead {
	if (self.read == NO) {
		[self willChangeValueForKey:@"read"];
		[self.newsModule markItemWithIdentifierAsRead:self.identifier];
		[self didChangeValueForKey:@"read"];
	}
}

@end

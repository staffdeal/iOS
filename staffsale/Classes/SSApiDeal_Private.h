//
//  SSApiDeal_Private.h
//  staffsale
//
//  Created by Daniel Wetzel on 21.11.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiDeal.h"

@interface SSApiDeal ()

@property (nonatomic, assign) BOOL isSaved;

@end

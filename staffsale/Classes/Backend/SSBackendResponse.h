//
//  SSBackendResponse.h
//  staffsale
//
//  Created by Daniel Wetzel on 02.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SSBackendConnection;

@class RXMLElement;

@class SSApiObject;
@class SSApiInfo;
@class SSApiAuth;
@class SSApiError;
@class SSApiUser;
@class SSApiSubmit;
@class SSApiRegistration;

@interface SSBackendResponse : NSObject

@property (nonatomic, weak, readonly) SSBackendConnection *backendConnection;
@property (nonatomic, copy, readonly) NSURL *requestedURL;

@property (nonatomic, readonly) NSUInteger responseCode;
@property (nonatomic, strong, readonly) NSDictionary *responseHeaders;
@property (nonatomic, strong, readonly) NSString *responseString;
@property (nonatomic, copy, readonly) NSError *requestError;
@property (nonatomic, strong, readonly) NSData *responseData;

@property (nonatomic, readonly) BOOL loadedFromCache;

@property (nonatomic, readonly) NSUInteger documentVersion;

@property (nonatomic, strong, readonly) RXMLElement *element;

@property (nonatomic, strong, readonly) SSApiInfo *info;
@property (nonatomic, strong, readonly) SSApiUser *user;
@property (nonatomic, strong, readonly) SSApiAuth *authentication;
@property (nonatomic, strong, readonly) SSApiError *error;
@property (nonatomic, strong, readonly) SSApiSubmit *submit;
@property (nonatomic, strong, readonly) SSApiRegistration *registrationStatus;

- (id)initWithData:(NSData *)data connection:(__weak SSBackendConnection *)connection;

@end

//
//  SSBackendRequest.m
//  staffsale
//
//  Created by Daniel Wetzel on 02.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSBackendRequest.h"
#import "SSBackendRequest_Private.h"
#import "SSBackendResponse_Private.h"
#import "SSBackendConnection_Private.h"

#import "DWURLConnection.h"
#import "DWUploadField.h"


@interface SSBackendRequest() <NSURLConnectionDataDelegate>

@property (nonatomic, strong, readwrite) NSString *path;
@property (nonatomic, strong, readwrite) NSURL *requestingURL;

@property (nonatomic, strong) NSMutableDictionary *parameters;
@property (nonatomic, strong) NSMutableArray *uploadFields;

@property (nonatomic, strong) DWURLConnection *connection;

@end

@implementation SSBackendRequest

+ (SSBackendRequest *)requestWithPath:(NSString *)path {
	return [[SSBackendRequest alloc] initWithPath:path];
}

+ (SSBackendRequest *)startRequestWithPath:(NSString *)path completion:(SSBackendConnectionRequestHandler)completion {
	SSBackendRequest *request = [self requestWithPath:path];
	[request startOnSharedConnectionWithCompletion:completion];
	return request;
}

- (id)initWithPath:(NSString *)path {
	if ((self = [super init])) {
		
		NSArray *components = [path componentsSeparatedByString:@"?"];
		if (components.count == 1) {
			self.path = path;
		} else if (components.count == 2) {
			self.path = [components objectAtIndex:0];
			NSArray *parameters = [((NSString *)[components objectAtIndex:1]) componentsSeparatedByString:@"&"];
			for (NSString *parameter in parameters) {
				NSArray *parameterComponents = [parameter componentsSeparatedByString:@"="];
				if (parameterComponents.count == 2) {
					[self addParameter:[parameterComponents objectAtIndex:1] forKey:[parameterComponents objectAtIndex:0]];
				}
			}
		}
		self.state = SSBackendRequestStateUnhandled;
		
		[self adjustRequestingURL];
	}
	return self;
}

- (void)dealloc {
	[self.connection cancel];
	self.connection = nil;
	NSLog(@"Request dies");
}

#pragma mark - Completion handlers

- (void)addCompletionHandler:(SSBackendConnectionRequestHandler)completion {
	assert(completion);
	@synchronized(self) {
		if (self.completionHandlers == nil) {
			self.completionHandlers = [[NSMutableArray alloc] init];
		}
		
		if (![self.completionHandlers containsObject:completion]) {
			[self.completionHandlers addObject:completion];
		}
	}
}

- (void)removeCompletionHandler:(SSBackendConnectionRequestHandler)completion {
	assert(completion);
	@synchronized(self) {
		[self.completionHandlers removeObject:completion];
		
		if (self.completionHandlers.count == 0) {
			self.completionHandlers = nil;
		}
	}
}

- (void)removeAllCompletionHandlers {
	self.completionHandlers = nil;
}

#pragma mark - Parameters

- (void)addParameter:(NSString *)value forKey:(NSString *)key {
	if (self.parameters == nil) {
		self.parameters = [[NSMutableDictionary alloc] init];
	}
	
	[self.parameters setObject:value forKey:key];
	[self adjustRequestingURL];
}

- (void)removeParameterForKey:(NSString *)key {
	[self.parameters removeObjectForKey:key];
	[self adjustRequestingURL];
}

- (NSString *)parameterForKey:(NSString *)key {
	return [self.parameters objectForKey:key];
}

- (void)addUploadValue:(NSString *)value forKey:(NSString *)key {
	assert(key);
	assert(value);
	DWUploadField *field = [[DWUploadField alloc] initWithIdentifier:key stringValue:value];
	[self addUploadField:field];
}

- (void)addUploadImage:(UIImage *)image forKey:(NSString *)key filename:(NSString *)filename {
	assert(key);
	assert(image);
	
	if (filename == nil) {
		filename = [NSString stringWithFormat:@"%@.png",key];
	}
	DWUploadField *field = [[DWUploadField alloc] initWithIdentifier:key image:image filename:filename];
	[self addUploadField:field];
}

- (void)addUploadField:(DWUploadField *)field {
	if (field) {
		if (self.uploadFields == nil) {
			self.uploadFields = [[NSMutableArray alloc] init];
		}
		[self.uploadFields addObject:field];
	}
}

- (void)adjustRequestingURL {
	
	NSURL *targetURL = [self.backendConnection.baseURL URLByAppendingPathComponent:self.path isDirectory:YES];
	if (targetURL) {
		
		NSMutableArray *usedParamterStrings = [NSMutableArray array];
		for (NSString *parameterName in self.parameters) {
			NSString *parameterValue = self.parameters[parameterName];
			[usedParamterStrings addObject:[NSString stringWithFormat:@"%@=%@",parameterName,parameterValue]];
		}
		
		if (self.backendConnection.token) {
			[usedParamterStrings addObject:[NSString stringWithFormat:@"token=%@",self.backendConnection.token]];
		}
		
		if (usedParamterStrings.count > 0) {
			NSString *paramterString = [usedParamterStrings componentsJoinedByString:@"&"];
			NSString *targetURLString = [[targetURL absoluteString] stringByAppendingFormat:@"?%@",paramterString];
			targetURL = [NSURL URLWithString:targetURLString];
		}
		
		self.requestingURL = targetURL;
	}
}

#pragma mark - Action

- (void)start {
	if (self.state != SSBackendRequestStateRunning &&
		self.state != SSBackendRequestStateFinished &&
		self.state != SSBackendRequestStateFailed &&
		self.state != SSBackendRequestStateCanceled) {
		
		dispatch_async(dispatch_get_main_queue(), ^{
			
			[self adjustRequestingURL];
			NSURL *targetURL = [self.requestingURL copy];
			if (targetURL) {
							
				self.connection = [[DWURLConnection alloc] initWithURL:targetURL];
				[self.connection addHeaderValue:@"application/xml" forKey:@"Content-Type"];
				for (DWUploadField *field in self.uploadFields) {
					[self.connection addUploadField:field];
				}
				
				__weak SSBackendRequest *blockself = self;
				
				[self.connection startWithCompletionHandler:^(NSData *receivedData, NSDictionary *responseHeaders, NSUInteger statusCode, NSError *error) {
					
					dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
						
#ifdef ENABLE_API_MONITOR
						NSString *responseString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
						SSMON(@"%@",responseString);
#endif
						
						blockself.response = [[SSBackendResponse alloc] initWithReceivedData:receivedData
																					 headers:responseHeaders
																				  statusCode:statusCode
																				  backendConnection:self.backendConnection];
						blockself.response.requestedURL = targetURL;
						
						blockself.response.requestError = error;
						dispatch_async(dispatch_get_main_queue(), ^{
							
							if (error == nil) {
								blockself.state = SSBackendRequestStateFinished;
								[blockself.backendConnection handleRequestFinished:blockself];
							} else {
								blockself.state = SSBackendRequestStateFailed;
								[blockself.backendConnection handleRequestFailed:blockself];
							}
						});
					});
				}];
			}
		});
	}
}

- (void)cancel {
	[self.connection cancel];
	self.connection = nil;
	self.state = SSBackendRequestStateCanceled;
	[self.backendConnection handleRequestCanceled:self];
}

- (void)startOnSharedConnectionWithCompletion:(SSBackendConnectionRequestHandler)completion {
	[[SSBackendConnection sharedConnection] sendRequest:self completion:completion];
}

@end
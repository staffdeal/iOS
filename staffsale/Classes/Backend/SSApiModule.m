//
//  SSApiModule.m
//  staffsale
//
//  Created by Daniel Wetzel on 10.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiModule.h"
#import "SSApiModule_Private.h"

#import "SSBackendResponse.h"

NSString * const SSApiModuleStartsRefreshNotification = @"moduleStartRefresh";
NSString * const SSApiModuleFinishedRefreshNotification = @"moduleFinishedRefresh";


@interface SSApiModule()

@property (nonatomic, readwrite) SSApiModuleUserLevel userlevel;

@property (nonatomic, strong) NSMutableDictionary *readData;

@property (nonatomic, strong) NSMutableDictionary *categories;

@end

@implementation SSApiModule

- (NSString *)description {
	return [NSString stringWithFormat:@"%@, %@, %@, %@, %@",
			[super description],
			self.identifier,
			self.title,
			self.path,
			[[self class] userLevelStringForLevel:self.userlevel]];
}

+ (SSApiModuleUserLevel)userLevelForString:(NSString *)levelString {
	if ([levelString isEqualToString:@"admin"]) {
		return SSApiModuleUserLevelAdmin;
	} else if ([levelString isEqualToString:@"user"]) {
		return SSApiModuleUserLevelUser;
	} else if ([levelString isEqualToString:@"guest"]) {
		return SSApiModuleUserLevelGuest;
	} else {
		return SSApiModuleUserLevelNone;
	}
}

+ (NSString *)userLevelStringForLevel:(SSApiModuleUserLevel)level {
	switch (level) {
		case SSApiModuleUserLevelGuest:
			return @"guest";
			break;
		case SSApiModuleUserLevelUser:
			return @"user";
			break;
		case SSApiModuleUserLevelAdmin:
			return @"admin";
			break;
		default:
			return nil;
	}
}

- (id)initWithXMLElement:(RXMLElement *)element {
	if ((self = [super initWithXMLElement:element])) {
		
		self.categories = [[NSMutableDictionary		alloc] init];
		self.identifier = [element attribute:@"id"];
		
		if (self.identifier ) {
			self.title = [[element child:@"title"] text];
			
			self.path = [[element child:@"path"] text];
			BOOL neededPathCleaning = NO;
			while ([self.path hasPrefix:@"/"]) {
				self.path = [self.path stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@""];
				neededPathCleaning = YES;
			}
			while ([self.path hasSuffix:@"/"]) {
				self.path = [self.path stringByReplacingCharactersInRange:NSMakeRange(self.path.length - 1, 1) withString:@""];
				neededPathCleaning = YES;
			}
			if (neededPathCleaning) {
				SSLog(@"Module path %@ needed cleaning, please correct in backend settings.",self.path);
			}
			
			self.userlevel = [[self class] userLevelForString:[[element child:@"authLevel"] text]];
			
			[self loadReadData];
		} else {
			// a module without an id must not be used
			self = nil;
		}
		
	}
	return self;
}

- (void)performInitalLoading:(void (^)(SSApiModule *module, NSError *error))completion {
	SSLog(@"Module %@ need to override performInitalLoading!!!", self.identifier);
	dispatch_async(dispatch_get_main_queue(), ^{
		completion(self,nil);
	});
}

- (void)refreshWithCompletion:(void (^)(id, NSError *))completion {
	self.isRefreshing = YES;
	SSLog(@"Refreshing data of module %@",self.identifier);
	[[NSNotificationCenter defaultCenter] postNotificationName:SSApiModuleStartsRefreshNotification object:self];
}

- (void)signalRefreshFinished {
	self.isRefreshing = NO;
	SSLog(@"Refreshing refreshing finshed of module %@",self.identifier);
	[[NSNotificationCenter defaultCenter] postNotificationName:SSApiModuleFinishedRefreshNotification object:self];
}

#pragma mark - Caching

- (NSURL *)cacheURL {
	
	NSArray *caches = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
	if (caches.count > 0) {
		NSString *cachePath = [caches objectAtIndex:0];
		NSURL *cacheURL = [[[NSURL fileURLWithPath:cachePath] URLByAppendingPathComponent:@"/Modules" isDirectory:YES] URLByAppendingPathComponent:self.identifier isDirectory:YES];
		
		//			NSString *tokenSegment = (self.isLoggedIn) ? self.token : @"anonymous";
		//			cacheURL = [cacheURL URLByAppendingPathComponent:tokenSegment isDirectory:YES];
		
		cachePath = [cacheURL path];
		
		BOOL isDirectory = NO;
		if (!([[NSFileManager defaultManager] fileExistsAtPath:cachePath isDirectory:&isDirectory] && isDirectory)) {
			[[NSFileManager defaultManager] createDirectoryAtPath:cachePath withIntermediateDirectories:YES attributes:nil error:nil];
		}
		
		return cacheURL;
	}
	
	return nil;
}

- (NSURL *)cacheURLForKey:(NSString *)key {
	key = [key stringByReplacingOccurrencesOfString:@"/" withString:@"_"];
	return [self.cacheURL URLByAppendingPathComponent:[key stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding] isDirectory:NO];
}

- (void)saveData:(NSData *)data forKey:(NSString *)key {
	assert(key);
	if (data == nil) {
		[self removeDataForKey:key];
	} else {
		NSURL *cacheURL = [self cacheURLForKey:key];
		[data writeToURL:cacheURL atomically:YES];
	}
}

- (NSData *)dataForKey:(NSString *)key {
	assert(key);
	return [[NSData alloc] initWithContentsOfURL:[self cacheURLForKey:key]];
}

- (void)removeDataForKey:(NSString *)key {
	assert(key);
	[[NSFileManager defaultManager] removeItemAtURL:[self cacheURLForKey:key] error:NULL];
}

- (SSBackendResponse *)cachedResponseForKey:(NSString *)key {
	assert(key);
	SSBackendResponse *response = nil;
	NSData *data = [self dataForKey:[key stringByAppendingString:@".xml"]];
	if (data == nil) {
		data = [self dataForKey:key];
	}
	
	if (data) {
		response = [[SSBackendResponse alloc] initWithData:data connection:self.backendConnection];
	}
	return response;
}

- (void)cacheResponse:(SSBackendResponse *)response forKey:(NSString *)key {
	assert(response);
	assert(key);
	
	[self removeDataForKey:key];
	[self removeDataForKey:[key stringByAppendingString:@".xml"]];
	
	NSString *contentTypeString = [response.responseHeaders objectForKey:@"Content-Type"];
	if ([contentTypeString isEqualToString:@"application/xml"]) {
		key = [key stringByAppendingString:@".xml"];
	}
	[self saveData:response.responseData forKey:key];
}

#pragma mark - Read/Unread

- (void)markItemWithIdentifierAsRead:(NSString *)identifier {
	assert(identifier);
	@synchronized(self) {
		if (self.readData == nil) {
			self.readData = [[NSMutableDictionary alloc] init];
		}
		[self.readData setObject:[NSDate date] forKey:identifier];
		dispatch_async(dispatch_get_main_queue(), ^{
			[self synchronizeReadData];
		});
	}
}

- (BOOL)hasItemWithIdentifierBeenRead:(NSString *)identifier {
	assert(identifier);
	@synchronized(self) {
		return ([self.readData objectForKey:identifier] != nil);
	}
}

- (NSURL *)readDataURL {
	NSArray *libraryDirs = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
	if (libraryDirs.count > 0) {
		NSURL *dbURL = [[NSURL alloc] initFileURLWithPath:[libraryDirs objectAtIndex:0]];
		dbURL = [dbURL URLByAppendingPathComponent:@"Static" isDirectory:YES];
		if (![[NSFileManager defaultManager] fileExistsAtPath:dbURL.path]) {
			[[NSFileManager defaultManager] createDirectoryAtURL:dbURL
									 withIntermediateDirectories:YES
													  attributes:nil
														   error:NULL];
		}
		
		if (dbURL) {
			dbURL = [dbURL URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist",self.identifier] isDirectory:NO];
			if (dbURL) {
				return dbURL;
			}
		}
	}
	return nil;
}

- (void)synchronizeReadData {
	@synchronized(self) {
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
			if (self.readData.count > 0) {
				NSURL *url = [self readDataURL];
				if (url) {
					[self.readData writeToURL:url atomically:YES];
				}
			}
		});
	}
}

- (void)loadReadData {
	NSURL *url = [self readDataURL];
	if (url) {
		self.readData = [[NSMutableDictionary alloc] initWithContentsOfURL:url];
	}
	if (self.readData == nil) {
		self.readData = [[NSMutableDictionary alloc] init];
	}
}

#pragma mark - Categories

- (void)clearCategories {
	[self.categories removeAllObjects];
}

- (void)addCategory:(SSApiCategory *)category {
	assert((category));
	[self.categories setObject:category forKey:category.identifier];
}

- (void)removeCategory:(SSApiCategory *)category {
	assert(category);
	[self.categories removeObjectForKey:category.identifier];
}

- (SSApiCategory *)categoryWithIdentifier:(NSString *)identifier {
	return [self.categories objectForKey:identifier];
}

@end

//
//  SSBackendConnection.h
//  staffsale
//
//  Created by Daniel Wetzel on 02.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DWConfiguration.h"
#import "SSBackendRequest.h"
#import "SSBackendResponse.h"

@class SSApiUser;

@protocol SSBackendConnectionDelegate;

typedef void (^SSBackendConnectionLoginHandler)(SSApiUser *user, NSError *error);

@interface SSBackendConnection : NSObject

#pragma mark - Connection Management

@property (nonatomic, weak) id <SSBackendConnectionDelegate>delegate;

@property (nonatomic, strong, readonly) DWConfiguration *configuration;
@property (nonatomic, strong, readonly) NSURL *baseURL;

@property (nonatomic, readonly) BOOL isLoggedIn;
@property (nonatomic, readonly) SSApiUser *user;

+ (SSBackendConnection *)sharedConnection;

+ (NSDateFormatter *)dateFormatter;

- (id)initWithConfiguration:(DWConfiguration *)configuration;

- (void)sendRequest:(SSBackendRequest *)request completion:(SSBackendConnectionRequestHandler)handler;

- (void)loginWithEmail:(NSString *)email andPassword:(NSString *)password completion:(SSBackendConnectionLoginHandler)completion;
- (void)logout;

@end

@protocol SSBackendConnectionDelegate <NSObject>

- (void)connectionDidReceiveTokenError:(SSBackendConnection *)connection;

@end
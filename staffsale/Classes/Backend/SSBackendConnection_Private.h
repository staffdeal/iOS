//
//  SSBackendConnection_Private.h
//  staffsale
//
//  Created by Daniel Wetzel on 05.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSBackendConnection.h"

@interface SSBackendConnection ()

@property (nonatomic, strong, readwrite) NSString *token;

- (void)handleRequestFinished:(SSBackendRequest *)request;
- (void)handleRequestFailed:(SSBackendRequest *)request;
- (void)handleRequestCanceled:(SSBackendRequest *)request;

@end

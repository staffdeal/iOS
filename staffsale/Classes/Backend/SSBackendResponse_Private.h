//
//  SSBackendResponse_Privat.h
//  staffsale
//
//  Created by Daniel Wetzel on 02.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSBackendResponse.h"

@class SSBackendConnection;

@interface SSBackendResponse ()

@property (nonatomic, copy, readwrite) NSURL *requestedURL;
@property (nonatomic, copy, readwrite) NSError *requestError;

- (id)initWithReceivedData:(NSData *)data headers:(NSDictionary *)headers statusCode:(NSUInteger)statusCode backendConnection:(__weak SSBackendConnection *)connection;

@end

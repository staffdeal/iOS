//
//  SSBackendResponse.m
//  staffsale
//
//  Created by Daniel Wetzel on 02.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSBackendResponse.h"
#import "SSBackendResponse_Private.h"

#import "RXMLElement.h"

#import "SSApiObject.h"
#import "SSApiInfo.h"
#import "SSApiAuth.h"
#import "SSApiError.h"
#import "SSApiUser.h"
#import "SSApiSubmit.h"
#import "SSApiRegistration.h"

#import "SSApiModule_Private.h"

@interface SSBackendResponse() {
	
	NSString *_responseString;
	
}

@property (nonatomic, weak, readwrite) SSBackendConnection *backendConnection;

@property (nonatomic, assign) NSUInteger responseCode;
@property (nonatomic, strong, readwrite) NSDictionary *responseHeaders;
@property (nonatomic, strong, readwrite) NSData *responseData;

@property (nonatomic, readwrite) BOOL loadedFromCache;

@property (nonatomic, strong, readwrite) RXMLElement *element;

@property (nonatomic, readwrite) NSUInteger documentVersion;
@property (nonatomic, strong, readwrite) SSApiInfo *info;
@property (nonatomic, strong, readwrite) SSApiUser *user;
@property (nonatomic, strong, readwrite) SSApiAuth *authentication;
@property (nonatomic, strong, readwrite) SSApiError *error;
@property (nonatomic, strong, readwrite) SSApiSubmit *submit;
@property (nonatomic, strong, readwrite) SSApiRegistration *registrationStatus;

@end

@implementation SSBackendResponse

- (id)initWithData:(NSData *)data connection:(__weak SSBackendConnection *)connection {
	if ((self = [self initWithReceivedData:data headers:nil statusCode:600 backendConnection:connection])) {
		
	}
	return self;
}

- (id)initWithReceivedData:(NSData *)data headers:(NSDictionary *)headers statusCode:(NSUInteger)statusCode backendConnection:(__weak SSBackendConnection *)connection {
	if ((self = [super init])) {
		
		self.backendConnection = connection;
		
		self.responseCode = statusCode;
		self.responseHeaders = headers;
		self.responseData = data;
		
		self.loadedFromCache = (self.responseCode >= 600);
		
		if (self.responseCode == 600) {
			if (data.length == 0) {
				self.responseCode = 601;
				self.requestError = [[NSError alloc] initWithDomain:@"SSBackendCacheLoadingErrorDomain" code:601 userInfo:[NSDictionary dictionaryWithObject:@"Could not load cached response" forKey:@"description"]];
			}
		}
		
		if (self.responseCode == 200 ||
			self.responseCode == 600 ||
			[SSApiError isResponseCodeHandled:self.responseCode]) {
			
			
			self.element = [RXMLElement elementFromXMLData:self.responseData];
			if (self.element.isValid) {
				
				if ([self.element.tag isEqualToString:@"staffsale"]) {
					self.documentVersion = [self.element attributeAsInt:@"version"];
					if (self.documentVersion == 0) {
						NSException *e = [[NSException alloc] initWithName:@"BackendDocumentVersionMismatchException"
																	reason:@"The backend's document version was not readable"
																  userInfo:nil];
						[e raise];
					}
					
					[self.element iterate:@"*" usingBlock:^(RXMLElement *element) {
						SSApiObject *object = [SSApiObject objectWithXMLElement:element];
						if (object) {
							
							if ([object isKindOfClass:[SSApiInfo class]] && self.info == nil) {
								self.info = (SSApiInfo *)object;
								if (self.info.user) {
									self.user = self.info.user;
								}
								
								for (NSString *moduleIdentifier in self.info.modules) {
									SSApiModule *module = [self.info.modules objectForKey:moduleIdentifier];
									module.backendConnection = connection;
								}
								
							} else if ([object isKindOfClass:[SSApiAuth class]]) {
								self.authentication = (SSApiAuth *)object;
								if (self.authentication.user) {
									self.user = self.authentication.user;
								}
							} else if ([object isKindOfClass:[SSApiError class]]) {
								self.error = (SSApiError *)object;
							} else if ([object isKindOfClass:[SSApiSubmit class]]) {
								self.submit = (SSApiSubmit *)object;
							} else if ([object isKindOfClass:[SSApiRegistration class]]) {
								self.registrationStatus = (SSApiRegistration *)object;
							}
						}
					}];
				}
			}
			
		} else {
			
		}
	}
	return self;
}

- (NSString *)responseString {
	if (self->_responseString == nil) {
		self->_responseString = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
	}
	return self->_responseString;
}

@end
//
//  SSBackendConnection.m
//  staffsale
//
//  Created by Daniel Wetzel on 02.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSBackendConnection.h"
#import "SSBackendConnection_Private.h"
#import "SSBackendRequest_Private.h"

#import "SSBackendResponse_Private.h"

#import "PDKeychainBindings.h"

#import "SSApiAuth.h"
#import "SSApiError.h"
#import "SSApiUser.h"
#import "SSApiUser_Private.h"
#import "RXMLElement.h"

#import "Reachability.h"

#import "NSDateFormatter+DWToolbox.h"

#define kSSBackendConnectionTokenKeychainKey @"token"
#define kSSBackendConnectionUserFirstnameKeychainKey @"user.firstname"
#define kSSBackendConnectionUserLastnameKeychainKey @"user.lastname"
#define kSSBackendConnectionUserEmailKeychainKey @"user.email"

@interface SSBackendConnection() {
	
	SSApiUser *_user;
	
}

@property (nonatomic, strong, readwrite) DWConfiguration *configuration;
@property (nonatomic, strong, readwrite) NSURL *baseURL;

@property (nonatomic, strong, readwrite) Reachability *reachability;
@property (nonatomic, readwrite) NetworkStatus reachabilityStatus;
@property (nonatomic, readonly) BOOL isOnline;

@property (nonatomic, strong) NSMutableArray *requests;

@property (nonatomic,assign) BOOL isLoggingIn;

@property (nonatomic, readonly) NSURL *cacheURL;

@end

@implementation SSBackendConnection

+ (SSBackendConnection *)sharedConnection {
	
	static SSBackendConnection *sharedConnection = nil;
	
	if (sharedConnection == nil) {
		sharedConnection = [[SSBackendConnection alloc] initWithConfiguration:[DWConfiguration defaultConfiguration]];
	}
	
	return sharedConnection;
}

+ (NSDateFormatter *)dateFormatter {
		
	static NSDateFormatter *dateFormatter = nil;
	if (dateFormatter == nil) {
		dateFormatter = [[NSDateFormatter alloc] init];
		[dateFormatter setDateFormat:@"yyyy-MM-dd"];
		[dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
	}
	return dateFormatter;
	
}

- (id)initWithConfiguration:(DWConfiguration *)configuration {
	if ((self = [super init])) {
		self.configuration = configuration;
		[self cacheURL];
		
		
		
		id baseURLString = [self.configuration customValueForKey:@"url"];
		NSAssert([baseURLString isKindOfClass:[NSString class]], @"url missing in configuration or not a string");
		self.baseURL = [[NSURL alloc] initWithString:(NSString *)baseURLString];
		
		
		self.reachability = [Reachability reachabilityWithHostname:self.baseURL.host];
		self.reachability.reachableBlock = ^(Reachability*reach) {
			SSLog(@"Reachibility: ONLINE");
			self.reachabilityStatus = reach.currentReachabilityStatus;
		};
		self.reachability.unreachableBlock = ^(Reachability*reach)
		{
			SSLog(@"Reachibility: OFFLINE");
			self.reachabilityStatus = reach.currentReachabilityStatus;
		};
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(applicationPauses)
													 name:UIApplicationDidEnterBackgroundNotification
												   object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(applicationResumes)
													 name:UIApplicationWillEnterForegroundNotification
												   object:nil];
		[self applicationResumes];
	}
	return self;
}

#pragma mark - Application Events

- (void)applicationPauses {
	SSLog(@"Backend Connection pauses all activities due to application background mode");
	[self.reachability stopNotifier];
}

- (void)applicationResumes {
	SSLog(@"Backend Connection resumes full operational mode");
	self.reachabilityStatus = self.reachability.currentReachabilityStatus;
	[self.reachability startNotifier];
}

#pragma mark - Requests

- (void)addRequest:(SSBackendRequest *)request {
	assert(request);
	if (self.requests == nil) {
		self.requests = [[NSMutableArray alloc] init];
	}
	
	SSLog(@"Queueing request");
	request.backendConnection = self;
	request.state = SSBackendRequestStateQueued;
	
	[self.requests addObject:request];
	[self processRequestQueue];
}

- (void)removeRequest:(SSBackendRequest *)request {
	assert(request);
	
	request.backendConnection = nil;
	[request removeAllCompletionHandlers];
	if (request.state == SSBackendRequestStateQueued) {
		request.state = SSBackendRequestStateUnhandled;
	}
	
	[self.requests removeObject:request];
	
	if (self.requests.count == 0) {
		self.requests = nil;
	} else {
		[self processRequestQueue];
	}
}

- (void)processRequestQueue {
	dispatch_async(dispatch_get_main_queue(), ^{
		for (SSBackendRequest *request in self.requests) {
			if (request.state == SSBackendRequestStateQueued) {
				[request start];
			}
			break;
		}
	});
}

- (void)sendRequest:(SSBackendRequest *)request completion:(SSBackendConnectionRequestHandler)handler {
	assert(request);
	if (handler != nil) {
		[request addCompletionHandler:handler];
	}
	
	[self addRequest:request];
	
}

- (void)handleRequestFinished:(SSBackendRequest *)request {
	if (request.state == SSBackendRequestStateFinished) {
		
		NSError *error = request.response.requestError;
		if (error == nil) {
			error = request.response.error.errorObject;
		}
		
		BOOL tokenIsStillValid = YES;
		// we check if the response has an invalid token response
		if (request.response.error.errorCode == SSApiErrorTokenInvalid) {
			// intercept the response here
			SSLog(@"Backend kicked my token");
			self.token = nil;
			tokenIsStillValid = NO;
			error = request.response.error.errorObject;
		} else if (request.response.user) {
			[self setUser:request.response.user];
		}
				
		for (SSBackendConnectionRequestHandler handler in request.completionHandlers) {
			handler(request.response, error);
		}
		
		[self removeRequest:request];
		
		if (tokenIsStillValid == NO) {
			if ([self.delegate respondsToSelector:@selector(connectionDidReceiveTokenError:)]) {
				[self.delegate connectionDidReceiveTokenError:self];
			}
		}
	}
}

- (void)handleRequestFailed:(SSBackendRequest *)request {
	if (request.state == SSBackendRequestStateFailed) {
		
		for (SSBackendConnectionRequestHandler handler in request.completionHandlers) {
			handler(request.response, request.response.requestError);
		}
		
		[self removeRequest:request];
	}
	
}

- (void)handleRequestCanceled:(SSBackendRequest *)request {
	if (request.state == SSBackendRequestStateCanceled) {
		[self removeRequest:request];
	}
	
}

#pragma mark - Login

- (void)loginWithEmail:(NSString *)email andPassword:(NSString *)password completion:(SSBackendConnectionLoginHandler)completion {
	if (self.isLoggedIn == NO) {
		if (self.isLoggingIn) {
			dispatch_async(dispatch_get_main_queue(), ^{
				NSError *error = [NSError errorWithDomain:@"SSBackendLoginConcurrencyForbidden" code:0 userInfo:nil];
				completion(nil,error);
			});
		} else {
			self.isLoggingIn = YES;
			SSBackendRequest *loginRequest = [[SSBackendRequest alloc] initWithPath:@"auth/token"];
			[loginRequest addParameter:[email stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding] forKey:@"email"];
			[loginRequest addParameter:[password stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding] forKey:@"password"];
			[self sendRequest:loginRequest completion:^(SSBackendResponse *response, NSError *error) {
				
				if (error == nil) {
					
					SSApiAuth *authentication = response.authentication;
					if (authentication) {
						
						if ([authentication.status hasSuffix:@"OK"]) {
							if ([authentication.token length] > 10) {
								self.token = authentication.token;
								if (response.authentication.user) {
									// there are user information, we fetch them
									[self setUser:response.authentication.user];
								}
								completion(authentication.user,nil);
							} else {
								error = [[NSError alloc] initWithDomain:@"AuthenticationErrorDomain" code:0 userInfo:[NSDictionary dictionaryWithObject:@"Token malformated" forKey:@"status"]];
							}
						} else {
							error = [[NSError alloc] initWithDomain:@"AuthenticationErrorDomain" code:0 userInfo:[NSDictionary dictionaryWithObject:authentication.status forKey:@"status"]];
						}
					}
				}
				
				completion(nil, error);
				self.isLoggingIn = NO;
			}];
		}
		
		
	} else {
		dispatch_async(dispatch_get_main_queue(), ^{
			completion(nil,nil);
		});
	}
}

- (void)loginWithEmail:(NSString *)email andPassword:(NSString *)password {
	
}

- (void)logout {
	SSBackendRequest *logoutRequest = [SSBackendRequest requestWithPath:@"auth/reset"];
	[self sendRequest:logoutRequest completion:^(SSBackendResponse *response, NSError *error) {}];
	self.token = nil;
}

#pragma mark - Token Management

- (BOOL)isLoggedIn {
	return (self.token != nil);
}

- (void)setToken:(NSString *)token {
	if (![token isEqualToString:self.token]) {
		
		if (token) {
			SSLog(@"Saving token: %@", token);
			[[PDKeychainBindings sharedKeychainBindings] setObject:token forKey:kSSBackendConnectionTokenKeychainKey];
		} else {
			SSLog(@"Deleting token");
			[[PDKeychainBindings sharedKeychainBindings] removeObjectForKey:kSSBackendConnectionTokenKeychainKey];
			[self setUser:nil];
		}
		//SSLog(@"Remove token-specific cache dir");
		//[[NSFileManager defaultManager] removeItemAtURL:self.cacheURL error:NULL];
	}
}

- (NSString *)token {
	return [[PDKeychainBindings sharedKeychainBindings] objectForKey:kSSBackendConnectionTokenKeychainKey];
}

- (void)setUser:(SSApiUser *)user {
	self->_user = user;
	if (user) {
		SSLog(@"Saving user information: %@ %@ (%@)",user.firstname, user.lastname, user.email);
		[[PDKeychainBindings sharedKeychainBindings] setObject:user.firstname forKey:kSSBackendConnectionUserFirstnameKeychainKey];
		[[PDKeychainBindings sharedKeychainBindings] setObject:user.lastname forKey:kSSBackendConnectionUserLastnameKeychainKey];
		[[PDKeychainBindings sharedKeychainBindings] setObject:user.email forKey:kSSBackendConnectionUserEmailKeychainKey];
	} else {
		SSLog(@"Deleting user information");
		[[PDKeychainBindings sharedKeychainBindings] removeObjectForKey:kSSBackendConnectionUserFirstnameKeychainKey];
		[[PDKeychainBindings sharedKeychainBindings] removeObjectForKey:kSSBackendConnectionUserLastnameKeychainKey];
		[[PDKeychainBindings sharedKeychainBindings] removeObjectForKey:kSSBackendConnectionUserEmailKeychainKey];
	}
}

- (SSApiUser *)user {
	if (self.isLoggedIn) {
		if (self->_user == nil) {
			self->_user = [[SSApiUser alloc] initWithXMLElement:nil];
			self->_user.firstname = [[PDKeychainBindings sharedKeychainBindings] objectForKey:kSSBackendConnectionUserFirstnameKeychainKey];
			self->_user.lastname = [[PDKeychainBindings sharedKeychainBindings] objectForKey:kSSBackendConnectionUserLastnameKeychainKey];
			self->_user.email = [[PDKeychainBindings sharedKeychainBindings] objectForKey:kSSBackendConnectionUserEmailKeychainKey];
		}
		return self->_user;
	}
	return nil;
}

#pragma mark - Caching

- (BOOL)isOnline {
	return (self.reachabilityStatus != NotReachable);
}

@end
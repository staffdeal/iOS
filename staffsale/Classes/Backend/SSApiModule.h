//
//  SSApiModule.h
//  staffsale
//
//  Created by Daniel Wetzel on 10.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiObject.h"

#import "SSBackendConnection.h"

#import "SSApiCategory.h"

typedef void (^SSApiModuleRefreshHandler)(id data, NSError *error);

@class SSBackendRequest;
@class SSBackendResponse;

extern NSString * const SSApiModuleStartsRefreshNotification;
extern NSString * const SSApiModuleFinishedRefreshNotification;

typedef enum {
	SSApiModuleUserLevelNone,
	SSApiModuleUserLevelAdmin,
	SSApiModuleUserLevelUser,
	SSApiModuleUserLevelGuest
} SSApiModuleUserLevel;

@interface SSApiModule : SSApiObject

@property (nonatomic, weak, readonly) SSBackendConnection *backendConnection;

@property (nonatomic, strong, readonly) NSString *identifier;
@property (nonatomic, strong, readonly) NSString *title;
@property (nonatomic, strong, readonly) NSString *path;
@property (nonatomic, readonly) SSApiModuleUserLevel userlevel;

@property (nonatomic, readonly) BOOL isRefreshing;

+ (SSApiModuleUserLevel)userLevelForString:(NSString *)levelString;
+ (NSString *)userLevelStringForLevel:(SSApiModuleUserLevel)level;

- (void)performInitalLoading:(void (^)(SSApiModule *module, NSError *error))completion;

- (void)refreshWithCompletion:(SSApiModuleRefreshHandler)completion;
- (void)signalRefreshFinished;

- (void)markItemWithIdentifierAsRead:(NSString *)identifier;
- (BOOL)hasItemWithIdentifierBeenRead:(NSString *)identifier;

- (void)saveData:(NSData *)data forKey:(NSString *)key;
- (NSData *)dataForKey:(NSString *)key;

- (SSBackendResponse *)cachedResponseForKey:(NSString *)key;
- (void)cacheResponse:(SSBackendResponse *)response forKey:(NSString *)key;


- (SSApiCategory *)categoryWithIdentifier:(NSString *)identifier;

@end
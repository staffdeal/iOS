//
//  SSBackendRequest_Private.h
//  staffsale
//
//  Created by Daniel Wetzel on 02.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSBackendRequest.h"

#import "SSBackendConnection.h"

@interface SSBackendRequest ()

@property (nonatomic, weak) SSBackendConnection *backendConnection;

@property (nonatomic, strong) NSMutableArray *completionHandlers;
@property (nonatomic, assign) SSBackendRequestState state;

@property (nonatomic, strong) SSBackendResponse *response;

- (void)adjustRequestingURL;

- (void)start;
- (void)cancel;

- (void)addCompletionHandler:(SSBackendConnectionRequestHandler)completion;
- (void)removeCompletionHandler:(SSBackendConnectionRequestHandler)completion;
- (void)removeAllCompletionHandlers;

@end

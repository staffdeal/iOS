//
//  SSBackendRequest.h
//  staffsale
//
//  Created by Daniel Wetzel on 02.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SSBackendResponse.h"

typedef void (^SSBackendConnectionRequestHandler)(SSBackendResponse *response, NSError *error);
typedef enum {
	SSBackendRequestStateUnhandled,
	SSBackendRequestStateQueued,
	SSBackendRequestStateRunning,
	SSBackendRequestStateFinished,
	SSBackendRequestStateFailed,
	SSBackendRequestStateCanceled
} 	SSBackendRequestState;

@interface SSBackendRequest : NSObject

@property (nonatomic, strong, readonly) NSString *path;
@property (nonatomic, strong, readonly) NSURL *requestingURL;

@property (nonatomic, readonly) SSBackendRequestState state;

+ (SSBackendRequest *)requestWithPath:(NSString *)path;
+ (SSBackendRequest *)startRequestWithPath:(NSString *)path completion:(SSBackendConnectionRequestHandler)completion;

- (id)initWithPath:(NSString *)path;

- (void)addParameter:(NSString *)value forKey:(NSString *)key;
- (void)removeParameterForKey:(NSString *)key;
- (NSString *)parameterForKey:(NSString *)key;

- (void)addUploadValue:(NSString *)value forKey:(NSString *)key;
- (void)addUploadImage:(UIImage *)image forKey:(NSString *)key filename:(NSString *)filename;

- (void)startOnSharedConnectionWithCompletion:(SSBackendConnectionRequestHandler)completion;

@end

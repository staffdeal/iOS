//
//  SSApiCompetitionIdea.m
//  staffsale
//
//  Created by Daniel Wetzel on 16.11.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiIdea.h"

#import "SSApiObject_Private.h"

#import "SSApiUser.h"

#import "SSBackendConnection.h"
#import "SSHTMLTemplate.h"

@interface SSApiIdea ()

@property (nonatomic, strong, readwrite) NSString *identifier;

@property (nonatomic, strong, readwrite) NSString *text;
@property (nonatomic, strong, readwrite) NSURL *link;
@property (nonatomic, strong, readwrite) NSDate *submissionDate;
@property (nonatomic, strong, readwrite) SSApiUser *user;

@property (nonatomic, strong, readwrite) NSArray *images;

@end

@implementation SSApiIdea

- (id)initWithXMLElement:(RXMLElement *)element {
	if ((self = [super initWithXMLElement:element])) {
		
		if ([element.tag isEqualToString:@"idea"]) {
			
			self.identifier = [element attribute:@"id"];
			
			self.text = [[element child:@"text"] text];
			self.submissionDate = [[SSBackendConnection dateFormatter] dateFromString:[[element child:@"submissionDate"] text]];
			self.user = [[SSApiUser alloc] initWithXMLElement:[element child:@"user"]];
			self.user.parent = self;
			
			NSString *urlString = [[element child:@"url"] text];
			if (urlString.length > 6) {
				self.link = [[NSURL alloc] initWithString:urlString];
			}
			RXMLElement *imagesElement = [element child:@"images"];
			if (imagesElement) {
				NSMutableArray *imageURLs = [NSMutableArray array];
				[imagesElement iterate:@"image" usingBlock:^(RXMLElement *imageElement) {
					NSString *imageIdentifier = [imageElement attribute:@"id"];
					if (imageIdentifier) {
						NSString *imageURLString = [imageElement text];
						if (imageURLString) {
							NSURL *imageURL = [NSURL URLWithString:imageURLString];
							if (imageURL) {
								[imageURLs addObject:imageURL];
							}
						}
					}
				}];
				
				self.images = (imageURLs.count > 0) ? [[NSArray alloc] initWithArray:imageURLs] : nil;
			}
			
		} else {
			self = nil;
		}
		
		if (self.identifier == nil) {
			self = nil;
		}
		
	}
	return self;
}

- (SSHTMLTemplate *)ideaTemplate {
	
	SSHTMLTemplate *template = [[SSHTMLTemplate alloc] initWithName:@"idea"];
	
	[template assignString:self.user.fullname forField:@"author"];
	[template assignString:NSLocalizedString(@"Ideen-Einreichung", @"Idea") forField:@"title"];
	[template assignDate:self.submissionDate forField:@"submitted" dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterNoStyle];
	[template assignString:self.text forField:@"content"];
	
	if (self.link) {
		NSString *linkTitle = NSLocalizedString(@"Zusätzlicher Link", @"Idea");
		[template appendString:[NSString stringWithFormat:@"<h2>%@</h2><a href=\"%@\">%@</a>",linkTitle, self.link.absoluteString, self.link.absoluteString] toField:@"content"];
	}
	
	if (self.images.count == 0) {
		[template assignString:@"none" forField:@"contentImagesDIVDisplay"];
	} else {
		[template assignString:@"block" forField:@"contentImagesDIVDisplay"];
		[template assignString:NSLocalizedString(@"Bilder", @"Idea") forField:@"IMAGESTEXT"];
		
		for (NSURL *imageURL in self.images) {
			[template appendString:[NSString stringWithFormat:@"<img src=\"%@\" />",imageURL.absoluteString] toField:@"contentImages"];
		}
	}
	
	return template;
	
}

@end

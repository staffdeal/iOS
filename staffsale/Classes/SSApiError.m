//
//  SSApiError.m
//  staffsale
//
//  Created by Daniel Wetzel on 16.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiError.h"

@interface SSApiError ()

@property (nonatomic, readwrite) SSApiErrorCode errorCode;
@property (nonatomic, strong, readwrite) NSString *errorMessage;

@end

@implementation SSApiError

+ (BOOL)isResponseCodeHandled:(NSUInteger)code {
	
	switch (code) {
		case SSApiErrorTokenInvalid:
		case SSApiErrorTokenRequired:
		case SSApiErrorRegistrationExists:
		case SSApiErrorRegistrationInsufficient:
		case SSApiErrorRegistrationInternal:
			return YES;
			break;
			
		default:
			break;
	}
	return NO;
}

- (id)initWithXMLElement:(RXMLElement *)element {
	if ((self = [super initWithXMLElement:element])) {
		
		self.errorCode = [[element child:@"code"] textAsInt];
		self.errorMessage = [[element child:@"message"] text];
		
	}
	return self;
}

- (NSError *)errorObject {
	NSError *error = [[NSError alloc] initWithDomain:@"BackendApiErrorDomain"
												code:self.errorCode
											userInfo:[NSDictionary dictionaryWithObject:self.errorMessage forKey:@"message"]];
	return error;
}

- (NSString *)description {
	return [NSString stringWithFormat:@"%@ Error %i: %@", [super description], self.errorCode, self.errorMessage];
}

@end

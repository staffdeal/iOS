//
//  SSApiDeal.h
//  staffsale
//
//  Created by Daniel Wetzel on 16.11.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiObject.h"

@class SSApiDealPass;
@class SSHTMLTemplate;

#import "SSApiDealsModule.h"

@interface SSApiDeal : SSApiObject

@property (nonatomic, strong, readonly) NSString *identifier;

@property (nonatomic, strong, readonly) NSString *provider;
@property (nonatomic, strong, readonly) NSString *title;
@property (nonatomic, strong, readonly) NSDate *startDate;
@property (nonatomic, strong, readonly) NSDate *endDate;
@property (nonatomic, strong, readonly) NSString *teaser;
@property (nonatomic, strong, readonly) NSString *descriptionHTML;

@property (nonatomic, readonly) BOOL isFeatured;
@property (nonatomic, readonly) BOOL isSaved;

@property (nonatomic, strong, readonly) SSApiDealPass *onlinePass;
@property (nonatomic, strong, readonly) SSApiDealPass *onsitePass;

@property (nonatomic, strong, readonly) NSArray *images;

- (SSHTMLTemplate *)descriptionTemplate;

- (void)saveWithCompletion:(SSApiDealSavingHandler)handler;

@end

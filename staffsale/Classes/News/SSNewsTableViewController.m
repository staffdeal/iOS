//
//  SSNewsTableViewController.m
//  staffsale
//
//  Created by Daniel Wetzel on 02.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSNewsTableViewController.h"

#import "SSBackendConnection.h"
#import "SSBackendRequest.h"
#import "SSBackendResponse.h"
#import "DWConfiguration.h"

#import "SSApiNews.h"
#import "SSApiNewsModule.h"

#import "DWActivityManager.h"

#import "SSTableViewCell.h"
#import "UIColor+DWToolbox.h"

#import "SSApiNewsCategory.h"

#import "SSNewsDetailsViewController.h"

#import "DWAlertView.h"
#import "DWImageView.h"

@interface SSNewsTableViewController ()

@property (nonatomic, readonly) SSApiNewsModule *newsModule;

@end

@implementation SSNewsTableViewController

- (SSApiNewsModule *)newsModule {
	return (SSApiNewsModule *)self.apiModule;
}

- (void)viewDidLoad
{
	self.isDataController = YES;
	self.needsFilterButton = YES;
	self.needsRefreshControl = YES;
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (section == 0) {
		return self.newsModule.news.count;
	}
	return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SSTableViewCell *cell = (SSTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kSSTableViewCellRequseIdentifier forIndexPath:indexPath];
	cell.titleImageView.contentMode = UIViewContentModeScaleAspectFill;
    cell.style = SSTableViewCellStyleIdeas;
	cell.titleReadTextStyleKey = @"news.title.read";
	cell.titleUnreadTextStyleKey = @"news.title.unread";
	
	SSApiNews *news = [self.newsModule.news objectAtIndex:indexPath.row];

	
	cell.titleLabel.text = news.title;
	cell.info3Label.text = news.category.title;
	cell.unreadBackgroundColor = news.category.color;
	cell.readBackgroundColor = news.category.color;
	cell.categoryColorView.backgroundColor = news.category.color;

	static NSDateFormatter *publishedDateFormatter = nil;
	if (publishedDateFormatter == nil) {
		publishedDateFormatter = [[NSDateFormatter alloc] init];
		publishedDateFormatter.dateStyle = NSDateFormatterMediumStyle;
		publishedDateFormatter.timeStyle = NSDateFormatterNoStyle;
	}
	cell.info1Label.text = [publishedDateFormatter stringFromDate:news.publishedDate];
	cell.titleImageURL = news.titleImageURL;
		
	cell.read = news.read;
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	SSApiNews *news = [self.newsModule.news objectAtIndex:indexPath.row];

	SSLog(@"Showing news details (%@)",news.title);
	
	if (news) {
		[news markAsRead];
		
		SSTableViewCell *cell = (SSTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
		if (cell) {
			cell.read = YES;
		}
		
		SSNewsDetailsViewController *detailsController = [[SSNewsDetailsViewController alloc] initWithNews:news];
		[self.navigationController pushViewController:detailsController animated:YES];
	}
	
}

#pragma mark - Filter

@end

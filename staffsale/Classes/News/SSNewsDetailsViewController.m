//
//  SSNewsDetailsViewController.m
//  staffsale
//
//  Created by Daniel Wetzel on 26.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSNewsDetailsViewController.h"

#import "SSApiNews.h"
#import "SSWebViewController.h"

@interface SSNewsDetailsViewController ()

@property (nonatomic, strong, readwrite) SSApiNews *news;

@property (nonatomic, strong) UIWebView *webView;

@end

@implementation SSNewsDetailsViewController

- (id)initWithNews:(SSApiNews *)news {
	if ((self = [super init])) {
		self.news = news;
		self.title = news.title;
	}
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	self.webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
	self.webView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
	[self.view addSubview:self.webView];
	
	[self.webView loadHTMLString:self.news.detailsHTML baseURL:nil];
	
	if (self.news.publicURL) {
		UIBarButtonItem *publicURLItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Mehr", @"News")
																		  style:UIBarButtonItemStylePlain
																		 target:self
																		 action:@selector(publicURLBarItemPressed)];
		self.navigationItem.rightBarButtonItem = publicURLItem;
	}
}

- (void)publicURLBarItemPressed {
	SSWebViewController *browser = [[SSWebViewController alloc] initWithURL:self.news.publicURL];
	browser.title = NSLocalizedString(@"Web", @"News");
	[self.navigationController pushViewController:browser animated:YES];
	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  SSNewsDetailsViewController.h
//  staffsale
//
//  Created by Daniel Wetzel on 26.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SSApiNews;

@interface SSNewsDetailsViewController : UIViewController

@property (nonatomic, strong, readonly) SSApiNews *news;

- (id)initWithNews:(SSApiNews *)news;

@end

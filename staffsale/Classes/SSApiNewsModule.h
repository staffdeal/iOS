//
//  SSApiNewsModule.h
//  staffsale
//
//  Created by Daniel Wetzel on 18.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiModule.h"

@class SSApiNewsCategory;

@interface SSApiNewsModule : SSApiModule

@property (nonatomic, strong, readonly) NSArray *news;

@end

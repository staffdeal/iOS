//
//  SSModuleTableViewController.h
//  staffsale
//
//  Created by Daniel Wetzel on 10.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SSApiModule.h"

@interface SSModuleTableViewController : UITableViewController

@property (nonatomic, strong) SSApiModule *apiModule;
@property (nonatomic, assign) BOOL isDataController;
@property (nonatomic, assign) BOOL needsFilterButton;
@property (nonatomic, assign) BOOL needsRefreshControl;

- (void)refresh;	// nothing implemented
- (void)showCurrentData;	// reloads the table view
- (void)filterButtonPressed;

@end

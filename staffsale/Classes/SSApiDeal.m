//
//  SSApiDeal.m
//  staffsale
//
//  Created by Daniel Wetzel on 16.11.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiDeal.h"
#import "SSApiDeal_Private.h"
#import "SSApiObject_Private.h"

#import "SSBackendConnection.h"

#import "SSApiDealPass.h"

#import "SSHTMLTemplate.h"
#import "NSDateFormatter+DWToolbox.h"

@interface SSApiDeal ()

@property (nonatomic, strong, readwrite) NSString *identifier;
@property (nonatomic, strong, readwrite) NSString *provider;
@property (nonatomic, strong, readwrite) NSString *title;
@property (nonatomic, strong, readwrite) NSDate *startDate;
@property (nonatomic, strong, readwrite) NSDate *endDate;
@property (nonatomic, strong, readwrite) NSString *teaser;
@property (nonatomic, strong, readwrite) NSString *descriptionHTML;
@property (nonatomic, strong, readwrite) NSArray *images;
@property (nonatomic, readwrite) BOOL isFeatured;
@property (nonatomic, strong, readwrite) SSApiDealPass *onlinePass;
@property (nonatomic, strong, readwrite) SSApiDealPass *onsitePass;


@end

@implementation SSApiDeal

- (id)initWithXMLElement:(RXMLElement *)element {
	if ((self = [super initWithXMLElement:element])) {
		
		if ([element.tag isEqualToString:@"deal"]) {
			self.identifier = [element attribute:@"id"];
			
			self.isFeatured = ([[element attribute:@"featured"].lowercaseString isEqualToString:@"yes"]);
			self.provider = [[element child:@"provider"] text];
			self.title = [[element child:@"title"] text];
			self.startDate = [[SSBackendConnection dateFormatter] dateFromString:[[element child:@"startDate"] text]];
			self.endDate = [[SSBackendConnection dateFormatter] dateFromString:[[element child:@"endDate"] text]];
			self.teaser = [[element child:@"teaser"] text];
			self.descriptionHTML = [[element child:@"text"] text];
			
			RXMLElement *passElements = [element child:@"pass"];
			if (passElements) {
				
				RXMLElement *onlineElement = [passElements child:@"online"];
				if (onlineElement) {
					self.onlinePass = [[SSApiDealPass alloc] initWithXMLElement:onlineElement];
					self.onsitePass.parent = self;
				}
				
				RXMLElement *onsiteElement = [passElements child:@"onsite"];
				if (onsiteElement) {
					self.onsitePass = [[SSApiDealPass alloc] initWithXMLElement:onsiteElement];
					self.onsitePass.parent = self;
				}
			}
			
			NSMutableArray *imageURLs = [NSMutableArray array];
			[element iterate:@"images.image" usingBlock:^(RXMLElement *imageElement) {
				
				NSString *imageIdentifier = [imageElement attribute:@"id"];
				if (imageIdentifier) {
					NSString *imageURLString = [imageElement text];
					if (imageURLString) {
						NSURL *imageURL = [NSURL URLWithString:imageURLString];
						if (imageURL) {
							[imageURLs addObject:imageURL];
						}
					}
				}
			}];
			
			self.images = (imageURLs.count > 0) ? [[NSArray alloc] initWithArray:imageURLs] : nil;
			
			
		}
		
		if (self.identifier == nil) {
			self = nil;
		}
		
		
	}
	return self;
}

- (SSHTMLTemplate *)descriptionTemplate {
	
	SSHTMLTemplate *template = [[SSHTMLTemplate alloc] initWithName:@"deal.description"];
	
	[template assignString:self.title forField:@"title"];
	[template assignString:self.provider forField:@"company"];
	[template assignString:self.descriptionHTML forField:@"content"];
	
	NSDateFormatter *dateFormatter = [NSDateFormatter dateFormatterWithDateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle];
	[template assignString:[NSString stringWithFormat:@"%@ - %@",[dateFormatter stringFromDate:self.startDate], [dateFormatter stringFromDate:self.endDate]] forField:@"date"];
	
	return template;
}

- (void)saveWithCompletion:(SSApiDealSavingHandler)handler {
	[(SSApiDealsModule *)self.module saveDealForUsage:self completion:handler];
}

@end

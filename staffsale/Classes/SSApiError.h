//
//  SSApiError.h
//  staffsale
//
//  Created by Daniel Wetzel on 16.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiObject.h"

typedef enum {
	SSApiErrorTokenInvalid				= 401,
	SSApiErrorTokenRequired				= 403,
	SSApiErrorRegistrationExists		= 404,
	SSApiErrorRegistrationInsufficient	= 405,
	SSApiErrorRegistrationInternal		= 406,
} SSApiErrorCode;

@interface SSApiError : SSApiObject

@property (nonatomic, readonly) SSApiErrorCode errorCode;
@property (nonatomic, strong, readonly) NSString *errorMessage;

@property (nonatomic, readonly) NSError *errorObject;

+ (BOOL)isResponseCodeHandled:(NSUInteger)code;

@end

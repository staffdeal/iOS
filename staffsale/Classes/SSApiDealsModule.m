//
//  SSApiDealsModule.m
//  staffsale
//
//  Created by Daniel Wetzel on 23.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiDealsModule.h"

#import "SSBackendConnection.h"

#import "SSApiObject_Private.h"

#import "SSApiDeal.h"
#import "SSApiDeal_Private.h"

@interface SSApiDealsModule ()

@property (nonatomic, strong, readwrite) NSDictionary *deals;
@property (nonatomic, strong, readwrite) NSDictionary *savedDeals;

@property (nonatomic, assign) NSInteger hideAfter;

@end

@implementation SSApiDealsModule

- (void)performInitalLoading:(void (^)(SSApiModule *, NSError *))completion {
		
	[self loadSavedDeals];
	
	NSError *error = nil;
	
	SSBackendResponse *cachedResponse = [self cachedResponseForKey:@"list"];
	if (cachedResponse) {
		
		RXMLElement *dealsElement = [cachedResponse.element child:@"deals"];
		if (dealsElement == nil || ![self loadDealsFromElement:dealsElement]) {
			error = [NSError errorWithDomain:@"SSApiDealsModuleInitialLoadingErrorDomain" code:100 userInfo:nil];
		}
	}
	
	completion(self, error);
	
}

- (void)refreshWithCompletion:(void (^)(NSDictionary *, NSError *))completion {

	SSBackendRequest *request = [[SSBackendRequest alloc] initWithPath:self.path];
	[self.backendConnection sendRequest:request completion:^(SSBackendResponse *response, NSError *error) {
		
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
			
			NSError *blockError = [error copy];
			
			if (blockError == nil) {
				[self cacheResponse:response forKey:@"list"];
				
				RXMLElement *element = [response.element child:@"deals"];
				if (element == nil || ![self loadDealsFromElement:element]) {
					blockError = [NSError errorWithDomain:@"SSApiDealsModuleXMLFormatErrorDomain" code:100 userInfo:nil];
				}
			}
			
			dispatch_async(dispatch_get_main_queue(), ^{
				completion(self.deals,blockError);
				[self signalRefreshFinished];
			});
		});		
	}];

}

- (BOOL)loadDealsFromElement:(RXMLElement *)element {
	
	if ([element.tag isEqualToString:@"deals"]) {
		
		self.hideAfter = [[element attribute:@"hideAfter"] integerValue];
		
		NSMutableDictionary *deals = [NSMutableDictionary dictionary];
		[element iterate:@"deal" usingBlock:^(RXMLElement *dealElement) {
			
			SSApiDeal *deal = [self dealFromElement:dealElement];
			[deals setObject:deal forKey:deal.identifier];
			
		}];
		
		NSMutableDictionary *obtainingDeals = [NSMutableDictionary dictionary];
		for (NSString *dealIdentifier in deals) {
			if (![self.savedDeals.allKeys containsObject:dealIdentifier]) {
				[obtainingDeals setObject:[deals objectForKey:dealIdentifier] forKey:dealIdentifier];
			}
		}
		
		self.deals = (obtainingDeals.count > 0) ? [[NSDictionary alloc] initWithDictionary:obtainingDeals] : nil;
		
		return YES;
	}
	
	return NO;
}

- (SSApiDeal *)dealFromElement:(RXMLElement *)element {
	SSApiDeal *deal = nil;
	if ([element.tag isEqualToString:@"deal"]) {
		deal = [[SSApiDeal alloc] initWithXMLElement:element];
		if (deal) {
			deal.parent = self;
			deal.module = self.module;
		}
	}
	return deal;
}

- (void)loadSavedDeals {
	
	NSMutableDictionary *savedDeals = [NSMutableDictionary dictionary];
	
	NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtURL:[self savedDealsDirectoryURL]
												   includingPropertiesForKeys:nil
																	  options:NSDirectoryEnumerationSkipsHiddenFiles
																		error:NULL];
	for (NSURL *fileURL in files) {
		RXMLElement *element = [RXMLElement elementFromURL:fileURL];
		if (element) {
			SSApiDeal *deal = [self dealFromElement:element];
			if (deal) {
				deal.isSaved = YES;
				[savedDeals setObject:deal forKey:deal.identifier];
			}
		}
	}
	
	self.savedDeals = (savedDeals.count > 0) ? [[NSDictionary alloc] initWithDictionary:savedDeals] : nil;
}

- (NSURL *)savedDealsDirectoryURL {
	NSString *documentsDir = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
	NSURL *dealsURL = [[NSURL fileURLWithPath:documentsDir] URLByAppendingPathComponent:@"savedDeals" isDirectory:YES];
	BOOL isDir = NO;
	if (![[NSFileManager defaultManager] fileExistsAtPath:dealsURL.path isDirectory:&isDir] || isDir == NO) {
		[[NSFileManager defaultManager] createDirectoryAtURL:dealsURL
								 withIntermediateDirectories:YES
												  attributes:nil
													   error:NULL];
	}
	return dealsURL;
}

- (void)saveDealForUsage:(SSApiDeal *)deal completion:(SSApiDealSavingHandler)handler {
	
	NSURL *dealsURL = [self savedDealsDirectoryURL];
	NSString *filename = [NSString stringWithFormat:@"%@.xml",deal.identifier];
	NSURL *dealURL = [dealsURL URLByAppendingPathComponent:filename isDirectory:NO];
	
	[deal.element writeToURL:dealURL options:RXMLWritingOptionIndent completion:^(BOOL success) {
		if (success) {
			deal.isSaved = YES;
			
			NSMutableDictionary *newSavedDeals = [NSMutableDictionary dictionaryWithDictionary:self.savedDeals];
			[newSavedDeals setObject:deal forKey:deal.identifier];
			self.savedDeals = [[NSDictionary alloc] initWithDictionary:newSavedDeals];
			
			NSMutableDictionary *newDeals = [NSMutableDictionary dictionaryWithDictionary:self.deals];
			[newDeals removeObjectForKey:deal.identifier];
			self.deals = (newDeals.count > 0) ? [[NSDictionary alloc] initWithDictionary:newDeals] : nil;
			
		}
		handler(success);
	}];
}

@end

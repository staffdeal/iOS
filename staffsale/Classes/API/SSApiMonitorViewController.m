//
//  SSApiMonitorViewController.m
//  staffsale
//
//  Created by Daniel Wetzel on 02.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiMonitorViewController.h"

static NSMutableString *logString;

NSString * const SSApiMonitorLogChangedNotification = @"LogChangedNotification";

@interface SSApiMonitorViewController ()

@property (nonatomic, strong) UITextView *textView;

@end


@implementation SSApiMonitorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.textView = [[UITextView alloc] initWithFrame:self.view.bounds];
	self.textView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
	[self.view addSubview:self.textView];
	
	self.textView.editable = NO;
	self.textView.font = [UIFont fontWithName:@"CourierNewPSMT" size:13.0f];
	
	[self updateTextView];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTextView) name:SSApiMonitorLogChangedNotification object:nil];
	
}

- (void)viewDidUnload {
	[[NSNotificationCenter defaultCenter] removeObserver:self name:SSApiMonitorLogChangedNotification object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

+ (void)log:(NSString *)log {
	@synchronized(self) {
		if (logString == nil) {
			logString = [[NSMutableString alloc] initWithFormat:@"Log started at %@\n",[NSDate date]];
		}
		
		[logString appendFormat:@"%@\n",log];
		[[NSNotificationCenter defaultCenter] postNotificationName:SSApiMonitorLogChangedNotification object:nil];
	}
}

- (void)updateTextView {
	@synchronized(self) {
		dispatch_async(dispatch_get_main_queue(), ^{
			self.textView.text = logString;
		});
	}
}

@end

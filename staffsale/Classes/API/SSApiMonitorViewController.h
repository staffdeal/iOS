//
//  SSApiMonitorViewController.h
//  staffsale
//
//  Created by Daniel Wetzel on 02.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSApiMonitorViewController : UIViewController

+ (void)log:(NSString *)log;

@end

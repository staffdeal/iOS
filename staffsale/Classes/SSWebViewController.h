//
//  SSWebViewController.h
//  staffsale
//
//  Created by Daniel Wetzel on 08.11.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DWWebView.h"

@interface SSWebViewController : UIViewController

@property (nonatomic, strong, readonly) DWWebView *webView;

- (id)initWithURL:(NSURL *)startURL;
- (id)initWithHTML:(NSString *)startHTML;

@end

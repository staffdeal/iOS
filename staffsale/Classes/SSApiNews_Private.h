//
//  SSApiNews_Private.h
//  staffsale
//
//  Created by Daniel Wetzel on 26.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiNews.h"

@interface SSApiNews ()

@property (nonatomic, strong) NSString *categoryIdentifier;
@property (nonatomic, weak, readwrite) SSApiNewsCategory *category;

@end

//
//  SSAppDelegate.m
//  staffsale
//
//  Created by Daniel Wetzel on 19.09.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSAppDelegate.h"

#import "SSApiInfoModule.h"

#import "SSSplashView.h"

#import "SSBackendConnection.h"
#import "DWConfiguration.h"

#import "SSBackendRequest.h"

#import "SSNewsTableViewController.h"
#import "SSCompetitionsTableViewController.h"
#import "SSDealsTableViewController.h"
#import "SSSettingsTableViewController.h"
#import "SSApiMonitorViewController.h"

#import "SSUserSettings.h"
#import "SSApiInfo.h"

#import "SSApiModule.h"
#import "SSLoginTableViewController.h"

#import "DWActivityManager.h"
#import "DWAlertView.h"

#import "DWURLConnection.h"
#import "DWURLDownload.h"

#import "SSApiError.h"

#import "DWConditionalInvocation.h"

#ifdef SCREEN_MIRROR_ENABLED
#import "UIApplication+ScreenMirroring.h"
#endif

@interface SSAppDelegate() <SSBackendConnectionDelegate> {
	
	BOOL _infoRequestFailed;
	NSUInteger _runningInitialLoadings;
	
	NSDate * _lastInfoRefreshDate;
	
}

@property (nonatomic, strong) SSSplashView *splashView;

@property (nonatomic, weak, readwrite) SSNewsTableViewController *newsViewController;
@property (nonatomic, weak, readwrite) SSCompetitionsTableViewController *ideasViewController;
@property (nonatomic, weak, readwrite) SSDealsTableViewController *dealsViewController;
@property (nonatomic, weak, readwrite) SSSettingsTableViewController *settingsViewController;

@end

@implementation SSAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
	
#ifdef TESTFLIGHT
	[TestFlight setDeviceIdentifier:[UIDevice currentDevice].uniqueIdentifier];
	[TestFlight takeOff:@"dd8e82d3-7d44-40ce-9cdb-14b774e5c5f2"];
#endif
	
	self.splashView = [[SSSplashView alloc] initWithFrame:self.window.screen.bounds];
	self.splashView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
	
	[SSBackendConnection sharedConnection].delegate = self;
	[[SSBackendConnection sharedConnection].configuration applyUICustomizationsWithOptions:DWConfigurationTintColorNavigationbarElement | DWConfigurationTintColorButtonElement];
	
	self.infoModule = [[SSApiInfoModule alloc] initWithConnection:[SSBackendConnection sharedConnection]];
	
	UIViewController *rootViewController = self.window.rootViewController;
	[rootViewController.view addSubview:self.splashView];
	if ([rootViewController isKindOfClass:[UITabBarController class]]) {
		UITabBarController *tabbarController = (UITabBarController *)rootViewController;
		
		NSMutableArray *tabs = [NSMutableArray arrayWithArray:tabbarController.viewControllers];
		
		for (UIViewController *navigationController in tabs) {
			if ([navigationController isKindOfClass:[UINavigationController class]]) {
				
				UINavigationController *parentController = (UINavigationController *)navigationController;
				if (parentController.viewControllers.count > 0) {
					
					UIViewController *viewController = [parentController.viewControllers objectAtIndex:0];
					
					if ([viewController isKindOfClass:[SSNewsTableViewController class]]) {
						self.newsViewController = (SSNewsTableViewController *)viewController;
					} else if ([viewController isKindOfClass:[SSCompetitionsTableViewController class]]) {
						self.ideasViewController = (SSCompetitionsTableViewController *)viewController;
					} else if ([viewController isKindOfClass:[SSDealsTableViewController class]]) {
						self.dealsViewController = (SSDealsTableViewController *)viewController;
					} else if ([viewController isKindOfClass:[SSSettingsTableViewController class]]) {
						self.settingsViewController = (SSSettingsTableViewController *)viewController;
					}
				}
			}
		}
		
#ifdef ENABLE_API_MONITOR
		
		SSApiMonitorViewController *monitorController = [[SSApiMonitorViewController alloc] init];
		UINavigationController *monitorNavigation = [[UINavigationController alloc] initWithRootViewController:monitorController];
		monitorController.title = @"Monitor";
		monitorController.tabBarItem.tag = 100;
		[tabs addObject:monitorNavigation];
		tabbarController.viewControllers = tabs;
		
#endif
		
		tabbarController.selectedIndex = [SSUserSettings settings].lastTabbarIndex;
		tabbarController.delegate = self;
		
	}
	
	[DWAlertView setScreenBackgroundColor:[UIColor colorWithWhite:0.0f alpha:0.5f]];
	

	
	if ([self isOverTestphase] == NO) {
		dispatch_async(dispatch_get_main_queue(), ^{
			[DWURLDownload cleanup];
			[self startInfoRequest];
		});
	}
	
#ifdef SCREEN_MIRROR_ENABLED
	[[UIApplication sharedApplication] setupScreenMirroring];
#endif
	
    return YES;
}

- (BOOL)isOverTestphase {
#ifdef BOMB_ENABLED
	BOOL stop = NO;
	NSDate *bombDate = [[DWConfiguration UICustomizatingConfiguration] customValueForKey:@"bombDate"];
	if (bombDate) {
		if ([bombDate timeIntervalSinceNow] < 0) {
			static DWActivityItem *bombActivity = nil;
			if (bombActivity == nil) {
				bombActivity = [[DWActivityItem alloc] initWithType:DWActivityItemTypeTextOnly title:@"Diese Testversion is abgelaufen."];
				[[DWActivityManager sharedManager] addActivityItem:bombActivity];
			}

			stop = YES;
		}
	}
	return stop;
#else
	return NO;
#endif
}

- (void)startInfoRequest {
	
	[self.infoModule performInitalLoading:^(SSApiModule *module, NSError *error) {
		
		if (error != nil) {
			self->_infoRequestFailed = YES;
		}
		
		dispatch_async(dispatch_get_main_queue(), ^{
			[self refreshInfo];
		});
		
	}];
}

- (void)refreshInfo {
	
	DWActivityItem *infoActivity = [[DWActivityManager sharedManager] addActivityWithTitle:NSLocalizedString(@"Lade Daten", @"Startup")];
	
	[self.infoModule refreshWithCompletion:^(id data, NSError *error) {
		
		[infoActivity hide];
		
		if (self.infoModule.modules.count > 0) {
			self->_lastInfoRefreshDate = [NSDate date];
			self->_infoRequestFailed = NO;
			if ([self.window.rootViewController isKindOfClass:[UITabBarController class]]) {
				
				for (SSApiModule *module in self.infoModule.modules) {
					
					if ([module.identifier isEqualToString:@"news"]) {
						self.newsViewController.apiModule = module;
						self.newsViewController.tabBarItem.enabled = YES;
					} else if ([module.identifier isEqualToString:@"ideas"]) {
						self.ideasViewController.apiModule = module;
					} else if ([module.identifier isEqualToString:@"deals"]) {
						self.dealsViewController.apiModule = module;
					} else if ([module.identifier isEqualToString:@"settings"]) {
						self.settingsViewController.apiModule = module;
					}
					
					self->_runningInitialLoadings++;
					SSLog("Starting module initialization ('%@')",module.identifier);
					[module performInitalLoading:^(SSApiModule *module, NSError *error) {
						[self apiModuleDidFinishInitalImport:module];
						
					}];
				}
			}
			
		} else {
			
			DWAlertView *errorView = [[DWAlertView alloc] initWithTitle:NSLocalizedString(@"Datenfehler", @"Info")
															 andMessage:NSLocalizedString(@"Es konnten keine Daten geladen werden.\nErneut versuchen? ", @"Info")];
			[errorView addActionWithTitle:NSLocalizedString(@"Ja", @"General") block:^{
				dispatch_async(dispatch_get_main_queue(), ^{
					[self refreshInfo];
				});
			}];
			[errorView addActionWithTitle:NSLocalizedString(@"Nein", @"General")];
			[errorView show];
		}
	}];
}

- (void)apiModuleDidFinishInitalImport:(SSApiModule *)module {
	
	if ([module.identifier isEqualToString:@"news"]) {
		[self.newsViewController.tableView reloadData];
	} else if ([module.identifier isEqualToString:@"ideas"]) {
		[self.ideasViewController.tableView reloadData];
	} else if ([module.identifier isEqualToString:@"deals"]) {
		[self.dealsViewController.tableView reloadData];
	} else if ([module.identifier isEqualToString:@"settings"]) {
		[self.settingsViewController.tableView reloadData];
	}
	
	SSLog("Finished module initialization ('%@')",module.identifier);
	
	BOOL doRefresh = NO;
	if (module.userlevel == SSApiModuleUserLevelGuest) {
		doRefresh = YES;
	} else if (module.userlevel == SSApiModuleUserLevelUser) {
		if ([SSBackendConnection sharedConnection].isLoggedIn) {
			doRefresh = YES;
		}
	}
	if (doRefresh) {
		[module refreshWithCompletion:^(id data, NSError *error) {}];
	}
	
	self->_runningInitialLoadings--;
	if (self->_runningInitialLoadings == 0) {
		
		[UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
		
		[UIView animateWithDuration:0.5
						 animations:^{
							 self.splashView.alpha = 0.0f;
						 }
						 completion:^(BOOL finished) {
							 [self.splashView removeFromSuperview];
							 self.splashView = nil;
							 
						 }];
	}
}

- (void)connectionDidReceiveTokenError:(SSBackendConnection *)connection {
	
	DWActivityItem *infoActivity = [[DWActivityManager sharedManager] addActivityWithTitle:NSLocalizedString(@"Login abgelaufen", @"Startup")];
	[SSUserSettings settings].lastTabbarIndex = 0;
	[self.newsViewController.tabBarController setSelectedIndex:0];
	int64_t delayInSeconds = 3.0;
	dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
	dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
		[infoActivity hide];
		[self startInfoRequest];
	});
	
}


- (void)applicationWillResignActive:(UIApplication *)application
{
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
	// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
	if ([self isOverTestphase] == NO &&
		(self->_infoRequestFailed || [self->_lastInfoRefreshDate timeIntervalSinceNow] < -10.0f)) {
		[self startInfoRequest];
	}
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Tabbar delegate

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
	
	if ([viewController isKindOfClass:[UINavigationController class]]) {
		
		UINavigationController *navigationController = (UINavigationController *)viewController;
		if (navigationController.viewControllers.count > 0) {
			
			UIViewController *rootController = [navigationController.viewControllers objectAtIndex:0];
			if ([rootController isKindOfClass:[SSModuleTableViewController class]]) {
				
				SSApiModule *module = ((SSModuleTableViewController *)rootController).apiModule;
				if (module.userlevel == SSApiModuleUserLevelGuest) {
					return YES;
				} else if (module.userlevel == SSApiModuleUserLevelNone) {
					return NO;
				} else {
					// user must be logged in
					if ([SSBackendConnection sharedConnection].isLoggedIn) {
						return YES;
					} else {
						SSLoginTableViewController *loginController = [[SSLoginTableViewController alloc] initWithCompletion:^(BOOL success) {
							if (success) {
								
								if ([viewController isKindOfClass:[SSModuleTableViewController class]]) {
									SSModuleTableViewController *moduleController = (SSModuleTableViewController *)viewController;
									[moduleController.apiModule refreshWithCompletion:^(id data, NSError *error) {}];
								}
								
								tabBarController.selectedViewController = viewController;
								[self.window.rootViewController dismissViewControllerAnimated:YES completion:^{}];
							}
						}];
						UINavigationController *loginNavigation = [[UINavigationController alloc] initWithRootViewController:loginController];
						[self.window.rootViewController presentViewController:loginNavigation animated:YES completion:^{}];
						return NO;
					}
				}
			}
		}
	}
	return YES;
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
	if (tabBarController == self.window.rootViewController) {
		[SSUserSettings settings].lastTabbarIndex = [tabBarController.viewControllers indexOfObject:viewController];
	}
}

#pragma mark - Login

- (void)startLogin {
	
	SSLoginTableViewController *loginController = [[SSLoginTableViewController alloc] initWithCompletion:^(BOOL success) {
		
		if (success) {
			[self.window.rootViewController dismissViewControllerAnimated:YES completion:^{}];
		}
		
	}];
	UINavigationController *loginNavigation = [[UINavigationController alloc] initWithRootViewController:loginController];
	[self.window.rootViewController presentViewController:loginNavigation animated:YES completion:^{}];
}

@end
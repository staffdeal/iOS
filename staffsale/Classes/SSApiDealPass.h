//
//  SSApiDealPass.h
//  staffsale
//
//  Created by Daniel Wetzel on 16.11.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiObject.h"

typedef enum {
	SSApiDealPassTypeStatic
} SSApiDealPassType;

typedef enum {
	SSApiDealPassMethodText,
	SSApiDealPassMethodQR
} SSApiDealPassMethod;

typedef void (^SSApiDealCodeRequestHandler)(id code, SSApiDealPassMethod method, NSError *error);

@interface SSApiDealPass : SSApiObject

@property (nonatomic, readonly) SSApiDealPassType type;
@property (nonatomic, readonly) SSApiDealPassMethod method;

@property (nonatomic, strong, readonly) NSURL *URL;

- (void)requestCodeWithCompletion:(SSApiDealCodeRequestHandler)completion;

@end

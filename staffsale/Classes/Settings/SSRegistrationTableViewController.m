//
//  SSRegistrationTableViewController.m
//  staffsale
//
//  Created by Daniel Wetzel on 03.12.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSRegistrationTableViewController.h"

#import "SSBackendRequest.h"

#import "NSString+DWToolbox.h"
#import "DWActivityManager.h"
#import "SSApiInfo.h"
#import "DWAlertView.h"

#import "SSApiError.h"

@interface SSRegistrationTableViewController () <UITextFieldDelegate>

@property (nonatomic, copy) void (^completion)(BOOL success);

@property (nonatomic, strong) UITextField *emailTextField;
@property (nonatomic, strong) UITextField *firstnameTextField;
@property (nonatomic, strong) UITextField *lastnameTextField;
@property (nonatomic, strong) UITextField *passwordTextField;
@property (nonatomic, strong) UITextField *passwordTextField2;

@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *firstname;
@property (nonatomic, strong) NSString *lastname;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *password2;

@end

@implementation SSRegistrationTableViewController

- (id)initWithCompletion:(void (^)(BOOL))completion {
	if ((self = [self initWithStyle:UITableViewStyleGrouped])) {
		self.completion = completion;
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	
	self.emailTextField = [[UITextField alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 100.0f, self.tableView.rowHeight)];
	self.emailTextField.placeholder = NSLocalizedString(@"E-Mail-Adresse", @"Login");
	self.emailTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
	self.emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
	self.emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
	self.emailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
	self.emailTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
	self.emailTextField.delegate = self;
	
	self.firstnameTextField = [[UITextField alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 100.0f, self.tableView.rowHeight)];
	self.firstnameTextField.placeholder = NSLocalizedString(@"Vorname", @"Login");
	self.firstnameTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
	self.firstnameTextField.keyboardType = UIKeyboardTypeDefault;
	self.firstnameTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
	self.firstnameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
	self.firstnameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
	self.firstnameTextField.delegate = self;
	
	self.lastnameTextField = [[UITextField alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 100.0f, self.tableView.rowHeight)];
	self.lastnameTextField.placeholder = NSLocalizedString(@"Nachname", @"Login");
	self.lastnameTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
	self.lastnameTextField.keyboardType = UIKeyboardTypeDefault;
	self.lastnameTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
	self.lastnameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
	self.lastnameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
	self.lastnameTextField.delegate = self;
	
	self.passwordTextField = [[UITextField alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 100.0f, self.tableView.rowHeight)];
	self.passwordTextField.placeholder = NSLocalizedString(@"Password", @"Login");
	self.passwordTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
	self.passwordTextField.keyboardType = UIKeyboardTypeDefault;
	self.passwordTextField.secureTextEntry = YES;
	self.passwordTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
	self.passwordTextField.autocorrectionType = UITextAutocorrectionTypeNo;
	self.passwordTextField.delegate = self;
	
	self.passwordTextField2 = [[UITextField alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 100.0f, self.tableView.rowHeight)];
	self.passwordTextField2.placeholder = NSLocalizedString(@"Password wiederholen", @"Login");
	self.passwordTextField2.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
	self.passwordTextField2.keyboardType = UIKeyboardTypeDefault;
	self.passwordTextField2.secureTextEntry = YES;
	self.passwordTextField2.autocapitalizationType = UITextAutocapitalizationTypeNone;
	self.passwordTextField2.autocorrectionType = UITextAutocorrectionTypeNo;
	self.passwordTextField2.delegate = self;
	
	UIBarButtonItem *startButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Senden", @"General")
																	style:UIBarButtonItemStyleDone
																   target:self
																   action:@selector(startRegistration)];
	startButton.enabled = NO;
	self.navigationItem.rightBarButtonItem = startButton;
	
	[self.emailTextField becomeFirstResponder];

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return (section == 0) ? 1 : 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	
	UIView *control = nil;

	if (indexPath.section == 0 && indexPath.row == 0) {
		control = self.emailTextField;
	} else if (indexPath.section == 1 && indexPath.row == 0) {
		control = self.firstnameTextField;
	} else if (indexPath.section == 1 && indexPath.row == 1) {
		control = self.lastnameTextField;
	} else if (indexPath.section == 2 && indexPath.row == 0) {
		control = self.passwordTextField;
	} else if (indexPath.section == 2 && indexPath.row == 1) {
		control = self.passwordTextField2;
	}
	
	control.frame = CGRectMake(5.0f, 0.0f, cell.contentView.bounds.size.width - 10.0f, cell.contentView.bounds.size.height);
	control.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
	control.backgroundColor = [UIColor clearColor];
	[cell.contentView addSubview:control];
	
	return cell;
	
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
	NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
	
	if (textField == self.emailTextField) {
		self.email = newString;
	} else if (textField == self.firstnameTextField) {
		self.firstname = newString;
	} else if (textField == self.lastnameTextField) {
		self.lastname = newString;
	} else if (textField == self.passwordTextField) {
		self.password = newString;
	} else {
		self.password2 = newString;
	}
	
	[self refreshStartButton];
	
	return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	
	if (textField == self.emailTextField) {
		[self.firstnameTextField becomeFirstResponder];
	} else if (textField == self.firstnameTextField) {
		[self.lastnameTextField becomeFirstResponder];
	} else if (textField == self.lastnameTextField) {
		[self.passwordTextField becomeFirstResponder];
	} else if (textField == self.passwordTextField) {
		[self.passwordTextField2 becomeFirstResponder];
	} else {
		[self startRegistration];
	}
	
	return YES;
}

- (void)refreshStartButton {
	self.navigationItem.rightBarButtonItem.enabled = [self inputIsSufficient];
}

- (BOOL)inputIsSufficient {
	if (self.password.length > 5 && [self.password isEqualToString:self.password2] && self.firstname.length > 2 && self.lastname.length > 2) {
		if ([self.email isEmailAddress]) {
			return YES;
		}
	}
	return NO;
}

- (void)startRegistration {
	
	if ([self inputIsSufficient]) {
		
		DWActivityItem *activity = [[DWActivityManager sharedManager] addActivityWithTitle:NSLocalizedString(@"Registrierung läuft", @"Registrierung")];
	
		SSBackendRequest *request = [[SSBackendRequest alloc] initWithPath:[SSApiInfo latestInfoInstance].registration.path];
		[request addUploadValue:self.email forKey:@"email"];
		[request addUploadValue:self.firstname forKey:@"firstname"];
		[request addUploadValue:self.lastname forKey:@"lastname"];
		[request addUploadValue:self.password forKey:@"password"];
		
		[[SSBackendConnection sharedConnection] sendRequest:request completion:^(SSBackendResponse *response, NSError *error) {
			
			[activity hide];
			
			
			NSString *errorString = nil;
			if (response.error != nil) {
				if (response.error.errorCode == SSApiErrorRegistrationExists) {
					errorString = NSLocalizedString(@"Diese E-Mail-Adresse ist bereits registreirt.", @"Login");
				} else if (response.error.errorCode == SSApiErrorRegistrationInsufficient) {
					errorString = NSLocalizedString(@"Ihre Daten sind ferhlerhaft oder nicht vollständig.", @"Login");
				} else if (response.error.errorCode == SSApiErrorRegistrationInternal) {
					errorString = NSLocalizedString(@"Fehler ber der Anmeldung. Bitte versuchen Sie es erneut.", @"Login");
				}
			} else if (error != nil) {
				errorString = NSLocalizedString(@"Fehler ber der Anmeldung. Bitte versuchen Sie es erneut.", @"Login");
			}
			
			if (errorString != nil) {
				DWAlertView *errorView = [[DWAlertView alloc] initWithTitle:NSLocalizedString(@"Registrierung", @"Login") andMessage:errorString];
				[errorView show];
				self.completion(NO);
			} else {
				[[SSBackendConnection sharedConnection] loginWithEmail:self.email andPassword:self.password completion:^(SSApiUser *user, NSError *error) {
					self.completion(user != nil);
				}];
			}
			
			
			
		}];
		
	}
	
}

@end

//
//  SSDebugSettingsControllerViewController.m
//  staffsale
//
//  Created by Daniel Wetzel on 04.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSDebugSettingsControllerViewController.h"
#import "DWConfiguration.h"
#import "SSBackendConnection.h"

@interface SSDebugSettingsControllerViewController ()

@end

@implementation SSDebugSettingsControllerViewController

- (id)init {
	if ((self = [super initWithNibName:@"SSDebugSettings" bundle:nil])) {
		
	}
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	DWConfiguration *config = [SSBackendConnection sharedConnection].configuration;
	self.URLTextfield.text = (NSString *)[config customValueForKey:@"url"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)done:(id)sender {
	
	DWConfiguration *config = [SSBackendConnection sharedConnection].configuration;
	
	BOOL needRestart = NO;
	
	NSString *newURL = self.URLTextfield.text;
	if (![newURL isEqualToString:(NSString *)[config customValueForKey:@"url"]]) {
		if (newURL.length == 0) {
			[[NSUserDefaults standardUserDefaults] removeObjectForKey:@"DEBUG_OVERRIDE_BASEURL"];
		} else {
			[[NSUserDefaults standardUserDefaults] setObject:newURL forKey:@"DEBUG_OVERRIDE_BASEURL"];
		}
		needRestart = YES;
		[[NSUserDefaults standardUserDefaults] synchronize];
	}
	
	[self.presentingViewController dismissViewControllerAnimated:YES completion:^{
		int64_t delayInSeconds = 1.0;
		dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
		dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
			if (needRestart) {
				exit(1);
			}
		});
	}];
}

- (IBAction)clearCache:(id)sender {
}
@end

//
//  SSLoginTableViewController.h
//  staffsale
//
//  Created by Daniel Wetzel on 10.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSLoginTableViewController : UITableViewController

- (id)initWithCompletion:(void (^)(BOOL success))completion;

@end

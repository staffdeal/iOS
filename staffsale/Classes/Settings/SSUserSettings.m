//
//  SSUserSettings.m
//  staffsale
//
//  Created by Daniel Wetzel on 04.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSUserSettings.h"

#define kSettingsLastTabbarIndexKey @"tabbarIndex"

@implementation SSUserSettings

+ (SSUserSettings *)settings {
	
	static SSUserSettings *sharedSettings = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedSettings = [[SSUserSettings alloc] init];
	});
	return sharedSettings;
	
}

#pragma mark - Settings

- (void)setLastTabbarIndex:(NSUInteger)lastTabbarIndex {
	[[NSUserDefaults standardUserDefaults] setInteger:lastTabbarIndex forKey:kSettingsLastTabbarIndexKey];
}

- (NSUInteger)lastTabbarIndex {
	return [[NSUserDefaults standardUserDefaults] integerForKey:kSettingsLastTabbarIndexKey];
}

@end

//
//  SSSettingsViewController.h
//  staffsale
//
//  Created by Daniel Wetzel on 02.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SSModuleTableViewController.h"

@interface SSSettingsTableViewController : SSModuleTableViewController

@property (weak, nonatomic) IBOutlet UILabel *authActionLabel;

@end

//
//  SSLoginTableViewController.m
//  staffsale
//
//  Created by Daniel Wetzel on 10.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSLoginTableViewController.h"

#import "NSString+DWToolbox.h"
#import "DWActivityManager.h"
#import "SSBackendConnection.h"

#import "SSApiInfo.h"

#import "SSRegistrationTableViewController.h"

#define kSSLoginLastUsedEmailKey @"lastLoginEmail"

@interface SSLoginTableViewController () <UITextFieldDelegate> {
	
	BOOL _hadError;
	
}

@property (nonatomic, copy) void (^completion)(BOOL success);

@property (nonatomic, strong) UITextField *emailTextField;
@property (nonatomic, strong) UITextField *passwordTextField;

@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *password;

@end

@implementation SSLoginTableViewController

- (id)initWithCompletion:(void (^)(BOOL success))completion {
	if ((self = [super initWithStyle:UITableViewStyleGrouped])) {
		self.completion = completion;
	}
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.title = NSLocalizedString(@"Login", @"Login");
	
	self.emailTextField = [[UITextField alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 100.0f, self.tableView.rowHeight)];
	self.emailTextField.placeholder = NSLocalizedString(@"E-Mail-Adresse", @"Login");
	self.emailTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
	self.emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
	self.emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
	self.emailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
	self.emailTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
	self.emailTextField.delegate = self;
	self.emailTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:kSSLoginLastUsedEmailKey];
	self.email = self.emailTextField.text;
	
	self.passwordTextField = [[UITextField alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 100.0f, self.tableView.rowHeight)];
	self.passwordTextField.placeholder = NSLocalizedString(@"Password", @"Login");
	self.passwordTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
	self.passwordTextField.keyboardType = UIKeyboardTypeDefault;
	self.passwordTextField.secureTextEntry = YES;
	self.passwordTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
	self.passwordTextField.autocorrectionType = UITextAutocorrectionTypeNo;
	self.passwordTextField.delegate = self;
	
	UIBarButtonItem *loginButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Anmelden", @"Login")
																	style:UIBarButtonItemStyleDone
																   target:self
																   action:@selector(signalLogin)];
	loginButton.enabled = NO;
	self.navigationItem.rightBarButtonItem = loginButton;
	
	UIBarButtonItem *abortButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
																				 target:self
																				 action:@selector(cancel)];
	self.navigationItem.leftBarButtonItem = abortButton;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return ([SSApiInfo latestInfoInstance].registration) ? 2 : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	switch (section) {
		case 0:
			return 2;
			break;
		case 1:
			return 1;
			break;
		default:
			return 0;
			break;
	}
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	switch (section) {
		case 0:
			return NSLocalizedString(@"Anmeldung", @"Login");
			break;
		case 1:
			return NSLocalizedString(@"Registrierung", @"Login");
			break;
		default:
			return nil;
	}
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
	switch (section) {
		case 0:
		{
			if (self->_hadError) {
				return NSLocalizedString(@"Ihre Zugangsdaten wurden nicht akzeptiert.", @"Login");
			} else {
				return NSLocalizedString(@"Um alle Bereiche dieser App zu nutzen, melden Sie sich bitte mit Ihren Benutzerdaten an.", @"Login");
			}
		}
			break;
		case 1:
			return [SSApiInfo latestInfoInstance].registration.hint;
			break;
		default:
			return nil;
	}
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
	
	if (indexPath.section == 0) {
		UIView *control = nil;
		if (indexPath.row == 0) {
			control = self.emailTextField;
		} else if (indexPath.row == 1) {
			control = self.passwordTextField;
		}
		control.frame = CGRectMake(5.0f, 0.0f, cell.contentView.bounds.size.width - 10.0f, cell.contentView.bounds.size.height);
		control.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
		control.backgroundColor = [UIColor clearColor];
		[cell.contentView addSubview:control];
		
	} else if (indexPath.section == 1) {
		if (indexPath.row == 0) {
			
			cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
			cell.textLabel.text = NSLocalizedString(@"Neu registrieren", @"Login");
		}
	}
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 1 && indexPath.row == 0) {
		SSRegistrationTableViewController *registrationViewController = [[SSRegistrationTableViewController alloc] initWithCompletion:^(BOOL success) {
			self.completion(success);
		}];
		registrationViewController.title = NSLocalizedString(@"Registrierung", @"Login");
		[self.navigationController pushViewController:registrationViewController animated:YES];
	}
}

#pragma mark - Cancellation

- (void)cancel {
	[self.presentingViewController dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark - Textfield delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	if (textField == self.emailTextField) {
		[self.passwordTextField becomeFirstResponder];
	} else if (textField == self.passwordTextField) {
		[self signalLogin];
	}
	return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
	NSString *input = [textField.text stringByReplacingCharactersInRange:range withString:string];
	if (textField == self.emailTextField) {
		self.email = input;
	} else if (textField == self.passwordTextField) {
		self.password = input;
	}
	
	self.navigationItem.rightBarButtonItem.enabled = [self isCredentialsSufficient];
	return YES;
}

#pragma mark - Login

- (BOOL)isCredentialsSufficient {
	
	if (self.password.length > 5) {
		if ([self.email isEmailAddress]) {
			return YES;
		}
	}
	
	return NO;
}

- (void)signalLogin {
	if ([self isCredentialsSufficient]) {
		[self.emailTextField resignFirstResponder];
		[self.passwordTextField resignFirstResponder];
		
		[[NSUserDefaults standardUserDefaults] setObject:self.email forKey:kSSLoginLastUsedEmailKey];
		
		DWActivityItem *activity = [[DWActivityManager sharedManager] addActivityWithTitle:NSLocalizedString(@"Anmeldung läuft", @"Login")];
		self.navigationItem.leftBarButtonItem.enabled = NO;
		[[SSBackendConnection sharedConnection] loginWithEmail:self.email
												   andPassword:self.password
													completion:^(SSApiUser *user, NSError *error) {
														[activity hide];
														if (user) {
															self.completion(YES);
														} else {
															self->_hadError = YES;
															self.navigationItem.leftBarButtonItem.enabled = YES;
															[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
															self.completion(NO);
														}
													}];
		
	}
}

@end
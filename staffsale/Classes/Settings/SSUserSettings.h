//
//  SSUserSettings.h
//  staffsale
//
//  Created by Daniel Wetzel on 04.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SSUserSettings : NSObject

@property (nonatomic, assign) NSUInteger lastTabbarIndex;

+ (SSUserSettings *)settings;

@end

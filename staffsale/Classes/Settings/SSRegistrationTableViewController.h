//
//  SSRegistrationTableViewController.h
//  staffsale
//
//  Created by Daniel Wetzel on 03.12.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSRegistrationTableViewController : UITableViewController

- (id)initWithCompletion:(void (^)(BOOL success))completion;

@end

//
//  SSDebugSettingsControllerViewController.h
//  staffsale
//
//  Created by Daniel Wetzel on 04.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSDebugSettingsControllerViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;
@property (weak, nonatomic) IBOutlet UITextField *URLTextfield;

- (IBAction)done:(id)sender;
- (IBAction)clearCache:(id)sender;

@end

//
//  SSSettingsViewController.m
//  staffsale
//
//  Created by Daniel Wetzel on 02.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSSettingsTableViewController.h"
#import "SSDebugSettingsControllerViewController.h"

#import "SSLoginTableViewController.h"
#import "SSBackendConnection.h"

#import "SSApiUser.h"

#import "SSAppDelegate.h"

#import "DWInputTableViewController.h"

static NSInteger userObservationContext;

@interface SSSettingsTableViewController ()

@property (nonatomic, readonly) SSApiUser *currentUser;

@end

@implementation SSSettingsTableViewController

- (SSApiUser *)currentUser {
	return [[SSBackendConnection sharedConnection] user];
}

- (void)awakeFromNib {
#ifdef DEBUG
	UIBarButtonItem *debugSettingsItem = [[UIBarButtonItem alloc] initWithTitle:@"DEBUG" style:UIBarButtonItemStylePlain target:self action:@selector(showDebugSettings)];
	self.navigationItem.leftBarButtonItem = debugSettingsItem;
#endif
	
#ifdef TESTFLIGHT
	UIBarButtonItem *feedbackButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Feedback", @"Settings")
																	   style:UIBarButtonItemStylePlain
																	  target:self
																	  action:@selector(startFeedback)];
	self.navigationItem.rightBarButtonItem = feedbackButton;
#endif
}

- (void)viewDidLoad {
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
	[self refreshUserSection];

	[[SSBackendConnection sharedConnection] addObserver:self
											 forKeyPath:@"user"
												options:NSKeyValueObservingOptionNew
												context:&userObservationContext];
}

- (void)viewDidUnload {
	[[SSBackendConnection sharedConnection] removeObserver:self
												forKeyPath:@"user"
												   context:&userObservationContext];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if (context == &userObservationContext) {
		[self refreshUserSection];
	} else {
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
	}
}

- (void)refreshUserSection {
	if (self.currentUser) {
		self.authActionLabel.text = NSLocalizedString(@"Abmelden", @"Settings");
	} else {
		self.authActionLabel.text = NSLocalizedString(@"Jetzt anmelden", @"Settings");
	}
	[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	if (section == 0) {
		if (self.currentUser) {
			return [NSString stringWithFormat:@"%@ %@ %@",NSLocalizedString(@"Angemeldet als", @"Settings"), self.currentUser.firstname, self.currentUser.lastname];
		} else {
			return NSLocalizedString(@"Nicht angemeldet", @"Settings");
		}
	}
	return nil;
}

- (void)showDebugSettings {
	
	SSDebugSettingsControllerViewController *settingsController = [[SSDebugSettingsControllerViewController alloc] init];
	if (settingsController) {
		[self.navigationController presentViewController:settingsController animated:YES completion:^{
			
		}];
	}
	
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.section == 0 && indexPath.row == 0) {
		if ([SSBackendConnection sharedConnection].isLoggedIn) {
			// Logout
			[[SSBackendConnection sharedConnection] logout];
		} else {
			
			SSAppDelegate *appDelegate = (SSAppDelegate *)[UIApplication sharedApplication].delegate;
			[appDelegate startLogin];
			
		}
	}
}

#ifdef TESTFLIGHT
- (void)startFeedback {
	
	DWInputTableViewController *feedbackController = [[DWInputTableViewController alloc] initWithType:DWInputTableViewControllerTypeTextField returnHandler:^(NSString *input) {
		
		if (input.length > 0) {
			
			[TestFlight submitFeedback:input];
			[self.navigationController popViewControllerAnimated:YES];
		}
		
	}];
	
	feedbackController.footerText = NSLocalizedString(@"Bitte beschreiben Sie das Problem so genau wie möglich. Vielen Dank für Ihre Mühe.", @"Feedback");
	feedbackController.title = NSLocalizedString(@"Feedback", @"Feedback");
	[self.navigationController pushViewController:feedbackController animated:YES];
}
#endif

@end

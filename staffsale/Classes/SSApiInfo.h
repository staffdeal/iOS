//
//  SSApiInfo.h
//  staffsale
//
//  Created by Daniel Wetzel on 09.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiObject.h"
#import "SSApiModule.h"
#import "SSApiUser.h"

#import "SSApiRegistration.h"

@interface SSApiInfo : SSApiObject

@property (nonatomic, strong, readonly) NSString *clientName;
@property (nonatomic, strong, readonly) NSString *clientMail;

@property (nonatomic, strong, readonly) NSDictionary *modules;

@property (nonatomic, strong, readonly) SSApiUser *user;
@property (nonatomic, strong, readonly) SSApiRegistration *registration;

+ (SSApiInfo *)latestInfoInstance;

@end

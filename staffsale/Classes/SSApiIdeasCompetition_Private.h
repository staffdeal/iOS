//
//  SSApiIdeasCompetition_Private.h
//  staffsale
//
//  Created by Daniel Wetzel on 05.11.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiIdeasCompetition.h"

@class SSApiCategory;

@interface SSApiIdeasCompetition ()

@property (nonatomic, strong) NSString *categoryIdentifier;
@property (nonatomic, weak, readwrite) SSApiCategory *category;

@property (nonatomic, strong, readwrite) NSString *title;
@property (nonatomic, strong, readwrite) NSString *companyName;

@property (nonatomic, strong, readwrite) NSString *teaserText;
@property (nonatomic, strong, readwrite) NSString *descriptionHTML;
@property (nonatomic, strong, readwrite) NSString *rulesHTML;

@property (nonatomic, strong, readwrite) NSDate *startDate;
@property (nonatomic, strong, readwrite) NSDate *endDate;

@property (nonatomic, strong, readwrite) NSURL *titleImageURL;
@property (nonatomic, strong, readwrite) NSArray *imageURLs;

@property (nonatomic, strong, readwrite) NSDictionary *ideas;

@end

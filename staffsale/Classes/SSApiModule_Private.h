//
//  SSApiModule_Private.h
//  staffsale
//
//  Created by Daniel Wetzel on 18.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiModule.h"

@interface SSApiModule ()

@property (nonatomic, weak, readwrite) SSBackendConnection *backendConnection;
@property (nonatomic, strong, readwrite) NSString *identifier;
@property (nonatomic, strong, readwrite) NSString *title;
@property (nonatomic, strong, readwrite) NSString *path;

@property (nonatomic, readwrite) BOOL isRefreshing;

- (void)clearCategories;
- (void)addCategory:(SSApiCategory *)category;
- (void)removeCategory:(SSApiCategory *)category;

@end

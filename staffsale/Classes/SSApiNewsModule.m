//
//  SSApiNewsModule.m
//  staffsale
//
//  Created by Daniel Wetzel on 18.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiNewsModule.h"
#import "SSApiModule_Private.h"

#import "SSApiObject_Private.h"

#import "SSBackendRequest.h"
#import "SSBackendResponse.h"
#import "RXMLElement.h"

#import "SSApiNews.h"
#import "SSApiNews_Private.h"

#import "SSApiNewsCategory.h"
#import "SSApiCategory_Private.h"

#import "SSBackendResponse.h"

#import "NSSortDescriptor+DWToolbox.h"

@interface SSApiNewsModule ()

@property (nonatomic, strong, readwrite) NSArray *news;

@end

@implementation SSApiNewsModule

- (id)initWithXMLElement:(RXMLElement *)element {
	if ((self = [super initWithXMLElement:element])) {

	}
	return self;
}

- (void)performInitalLoading:(void (^)(SSApiModule *module, NSError *error))completion {
	SSBackendResponse *response = [self cachedResponseForKey:@"index"];
	if (response && response.error == nil) {
		[self importNewsFromElement:response.element];
		completion(self, nil);
	} else {
		completion(self,[NSError errorWithDomain:@"SSNewsModuleInitialLoadingFailedErrorDomain" code:600 userInfo:nil]);
	}
}

- (void)refreshWithCompletion:(void (^)(id data, NSError *))completion {
	if (self.isRefreshing == NO) {
				
		[super refreshWithCompletion:completion];
		
		SSBackendRequest *request = [SSBackendRequest requestWithPath:self.path];
		[self.backendConnection sendRequest:request completion:^(SSBackendResponse *response, NSError *error) {
			
			if (error == nil) {
				dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
					[self cacheResponse:response forKey:@"index"];
					// we do this concurrently
					[self importNewsFromElement:response.element];
					// the completion is signaled in the main thread
					dispatch_async(dispatch_get_main_queue(), ^{
						completion(self.news, nil);
						[self signalRefreshFinished];
					});
				});
				
			} else {
				completion(nil, error);
				[self signalRefreshFinished];
			}
		}];
	}
}

- (void)importNewsFromElement:(RXMLElement *)element {
	RXMLElement *newsElement = [element child:@"news"];
	if (newsElement) {
		
		NSArray *categoryElements = [newsElement children:@"category"];
		[self clearCategories];
		for (RXMLElement *categoryElement in categoryElements) {
			
			SSApiNewsCategory *category = [[SSApiNewsCategory alloc] initWithXMLElement:categoryElement];
			if (category) {
				category.module = self;
				[self addCategory:category];
			}
		}
		
		NSArray *newsElements = [newsElement children:@"newsItem"];
		NSMutableArray *receivedNews = [NSMutableArray array];
		for (RXMLElement *newsItemElement in newsElements) {
			
			SSApiNews *receivedNewsItem = [[SSApiNews alloc] initWithXMLElement:newsItemElement];
			if (receivedNewsItem) {
				receivedNewsItem.module = self;
				receivedNewsItem.category = (SSApiNewsCategory *)[self categoryWithIdentifier:receivedNewsItem.categoryIdentifier];
				[receivedNews addObject:receivedNewsItem];
			}
		}
		
		[receivedNews sortUsingDescriptors:[NSSortDescriptor arrayWithSortDescriptorWithKey:@"publishedDate" ascending:NO]];
		self.news = [NSArray arrayWithArray:receivedNews];
	}
}

@end
//
//  SSIdeaDraft.m
//  staffsale
//
//  Created by Daniel Wetzel on 12.11.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSIdeaDraft.h"

#import "SSApiIdeasCompetition.h"
#import "UIImage+DWToolbox.h"

@implementation SSIdeaDraft

- (NSURL *)localImageURL {
	NSString *tmpFilepath = [NSString stringWithFormat:@"%@ideaImage.png",NSTemporaryDirectory()];
	
	NSURL *url = [NSURL fileURLWithPath:tmpFilepath];
	return url;
}

- (SSHTMLTemplate *)draftTemplate {
	
	NSURL *imageURL = nil;
	if (self.image) {
		imageURL = [self localImageURL];
		[self.image writeAsPNGToFile:imageURL.path];
	}
	
	SSHTMLTemplate *template = [[SSHTMLTemplate alloc] initWithName:@"ideaDraft"];
	
	[template assignString:self.competition.title forField:@"title"];
	[template assignString:NSLocalizedString(@"Ideenbeschreibung", @"New Idea") forField:@"headline"];
	[template assignString:self.text forField:@"content"];
	
	if (self.image != nil) {
		[template assignString:@"block" forField:@"titleImageDIVDisplay"];
		[template assignString:imageURL.absoluteString forField:@"titleImageURL"];
	} else {
		[template assignString:@"none" forField:@"titleImageDIVDisplay"];
	}
	
	if (self.URL) {
		[template assignString:NSLocalizedString(@"Link", @"New Idea") forField:@"URLHeadline"];
		[template assignString:self.URL.absoluteString forField:@"URL"];
		[template assignString:@"block" forField:@"URLDIVDisplay"];
	} else {
		[template assignString:@"none" forField:@"URLDIVDisplay"];
	}
	
	return template;
}

@end

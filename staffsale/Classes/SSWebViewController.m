//
//  SSWebViewController.m
//  staffsale
//
//  Created by Daniel Wetzel on 08.11.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSWebViewController.h"

@interface SSWebViewController ()

@property (nonatomic, strong, readwrite) DWWebView *webView;

@property (nonatomic, strong) NSURL *startURL;
@property (nonatomic, copy) NSString *startHTML;

@end

@implementation SSWebViewController

- (id)initWithURL:(NSURL *)startURL {
	if ((self = [super init])) {
		self.startURL = startURL;
	}
	return self;
}

- (id)initWithHTML:(NSString *)startHTML {
	if ((self = [self initWithURL:nil])) {
		self.startHTML = startHTML;
	}
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.webView = [[DWWebView alloc] initWithFrame:self.view.bounds];
	self.webView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
	self.webView.scalesPageToFit = YES;
	
	[self.view addSubview:self.webView];
	
	if (self.startURL) {
		[self.webView loadURL:self.startURL];
		self.startURL = nil;
	} else if (self.startHTML.length > 0) {
		[self.webView loadHTML:self.startHTML baseURL:nil];
		self.startHTML = nil;
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

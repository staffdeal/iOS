//
//  SSApiNewsCategory.h
//  staffsale
//
//  Created by Daniel Wetzel on 05.11.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiCategory.h"

@interface SSApiNewsCategory : SSApiCategory

@property (nonatomic, strong, readonly) NSString *slug;
@property (nonatomic, strong, readonly) UIColor *color;

@end

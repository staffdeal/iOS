//
//  SSApiNewsCategory.m
//  staffsale
//
//  Created by Daniel Wetzel on 05.11.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSApiNewsCategory.h"

#import "UIColor+DWToolbox.h"

@interface SSApiNewsCategory()

@property (nonatomic, strong, readwrite) NSString *slug;
@property (nonatomic, strong, readwrite) UIColor *color;

@end

@implementation SSApiNewsCategory

- (id)initWithXMLElement:(RXMLElement *)element {
	if ((self = [super initWithXMLElement:element])) {
		self.color = [[UIColor colorWithHexString:[[element child:@"color"] text]] copy];
		if (self.color == nil) {
			self.color = [UIColor whiteColor];
		}
	}
	return self;
}

@end

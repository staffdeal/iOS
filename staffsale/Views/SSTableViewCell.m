//
//  SSNewsTableViewCell.m
//  staffsale
//
//  Created by Daniel Wetzel on 17.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSTableViewCell.h"

#import "DWConfiguration.h"
#import "UIColor+DWToolbox.h"
#import <QuartzCore/QuartzCore.h>

#import "SSApiNews.h"
#import "SSApiNewsCategory.h"

#import "DWConfiguration.h"

#import "DWURLDownload.h"

NSString * const kSSTableViewCellRequseIdentifier = @"SSTableViewCell";

@interface SSTableViewCell () {
	
	BOOL _selected;
	
}

@property (nonatomic, assign) CGFloat sectionSeperationX;
@property (nonatomic, strong) DWURLDownload *imageDownload;

@end

@implementation SSTableViewCell

- (void)awakeFromNib {
	
	[super awakeFromNib];
	
	self.layer.masksToBounds = YES;
	
	self.style = SSTableViewCellStyleNews;
	self.sectionStyle = SSTableViewCellSectionStyleOneSection;
		
	[self applySectionStyle];
	
	self.gradientBackgroundView.layer.shouldRasterize = YES;
	
	
	self.rightSectionBackgroundView.backgroundColor = [UIColor whiteColor];
	self.rightSectionBackgroundView.layer.shadowColor = [UIColor blackColor].CGColor;
	self.rightSectionBackgroundView.layer.shadowOffset = CGSizeMake(-1.0f, 0.0f);
	self.rightSectionBackgroundView.layer.shadowOpacity = 0.5f;
	self.rightSectionBackgroundView.layer.shadowRadius = 2.0f;
	
	self.titleImageView.URLChangeBehavior = DWImageViewURLChangeBehaviorClearImmediately;
	self.titleImageView.layer.cornerRadius = 3.0f;
	
	self.selectedBackgroundColor = [UIColor colorFromCustomizationKey:@"lists.backgroundColor.selection"];
	self.readBackgroundColor = [UIColor colorFromCustomizationKey:@"lists.backgroundColor.read"];
	self.unreadBackgroundColor = [UIColor colorFromCustomizationKey:@"lists.backgroundColor.unread"];
	if (self.unreadBackgroundColor == nil) {
		self.unreadBackgroundColor = [self.readBackgroundColor colorByLighteningTo:0.7f];
	}
		
	self.titleLabel.verticalAlignment = DWLabelVerticalAlignmentMiddle;
	
	self.categoryColorView.layer.cornerRadius = 3.0f;
		
	self.titleImageView.contentMode = UIViewContentModeScaleAspectFill;
	self.titleImageView.clipsToBounds = YES;
	self.info1Label.verticalAlignment = DWLabelVerticalAlignmentMiddle;
	
	self.countBadge.insets = UIEdgeInsetsMake(0.0f, 5.0f, 0.0f, 5.0f);
	self.countBadge.hidden = YES;
		
	self.info1Label.negativPrefixString = NSLocalizedString(@"vor", @"Countdown");
	self.info1Label.negativSuffixString = nil;
	self.info1Label.positivPrefixString = NSLocalizedString(@"in", @"Countdown");
	self.info1Label.positivSuffixString = nil;
	
	self.checkbox.enabled = NO;
	
	self.info1Label.textStyleKey = @"lists.info1";
	self.info2Label.textStyleKey = @"lists.info2";
	self.info3Label.textStyleKey = @"lists.info3";
	self.titleLabel.textStyleKey = @"lists.title";
}

- (void)prepareForReuse {
	[self.imageDownload cancel];
	self.imageDownload = nil;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	self->_selected = selected;
	[self applyBackgroundColor];
}

- (void)setRead:(BOOL)read {
	if (read != self->_read) {
		self->_read = read;
		[self applyBackgroundColor];
		
		if (self.read) {
			if (self.titleReadTextStyleKey) {
				self.titleLabel.textStyleKey = self.titleReadTextStyleKey;
			} else {
				self.titleLabel.textStyleKey = @"lists.title";
			}
		}
		if (!self.read) {
			if (self.titleUnreadTextStyleKey) {
				self.titleLabel.textStyleKey = self.titleUnreadTextStyleKey;
			} else {
				self.titleLabel.textStyleKey = @"lists.title";
			}
		}
	}
}

- (void)applyBackgroundColor {
	if (self->_selected) {
		self.leftSectionBackgroundView.backgroundColor = self.selectedBackgroundColor;
		self.rightSectionBackgroundView.backgroundColor = [self.selectedBackgroundColor colorByLighteningTo:0.1f];
	} else {
		self.rightSectionBackgroundView.backgroundColor = [UIColor whiteColor];
		if (self.read) {
			self.leftSectionBackgroundView.backgroundColor = self.readBackgroundColor;
		} else {
			self.leftSectionBackgroundView.backgroundColor = self.unreadBackgroundColor;
		}
	}
}

- (void)setTitleImageURL:(NSURL *)titleImageURL {
	if (![titleImageURL.absoluteString isEqualToString:self.titleImageURL.absoluteString]) {
		
		if (titleImageURL == nil) {
			self.titleImageView.layer.borderColor = [UIColor clearColor].CGColor;
			self.titleImageView.layer.borderWidth = 0.0f;
		} else {
			self.titleImageView.layer.borderColor = [UIColor colorWithWhite:0.7f alpha:1.0f].CGColor;
			self.titleImageView.layer.borderWidth = 1.0f;
		}
		
		self.titleImageView.URL = titleImageURL;
	}
}

#pragma mark - Badge

- (void)setBadge:(NSString *)badge {
	if (![badge isEqualToString:self.badge]) {
		self.countBadge.badgeText = badge;
		
		CGRect titleFrame = self.titleLabel.frame;
		if (self.badge.length == 0) {
			self.countBadge.hidden = YES;
			titleFrame.size.width = 200.0f;
		} else {
			self.countBadge.hidden = NO;
		}
		self.titleLabel.frame = titleFrame;
	}
}

- (NSString *)badge {
	return self.countBadge.badgeText;
}

#pragma mark - Style

- (void)setStyle:(SSTableViewCellStyle)style {
	if (self.style != style) {
		self->_style = style;
		[self setNeedsLayout];
	}
}

- (void)layoutSubviews {
	[super layoutSubviews];

	CGRect titleImageFrame = self.titleImageView.frame;
	CGRect titleLabelFrame = self.titleLabel.frame;

	if (self.style == SSTableViewCellStyleNews) {
				
		self.sectionStyle = SSTableViewCellSectionStyleTwoSections;
		self.sectionSeperationX = 95.0f;
		[self applySectionStyle];
		
		titleLabelFrame.origin = (CGPoint){self.sectionSeperationX + 5.0f, 5.0f};
		titleLabelFrame.size = (CGSize){self.bounds.size.width - self.sectionSeperationX - 25.0f, 70.0f};
		self.titleLabel.verticalContentOffset = 0.0f;
		
		self.categoryColorView.hidden = YES;
		
		//self.info1Label.frame = CGRectMake(20.0f, 2.0f, 65.0f, 16.0f);
		self.info1Label.frame = CGRectMake(5.0f, 2.0f, 80.0f, 16.0f);
		self.info2Label.hidden = NO;
		self.info3Label.hidden = YES;
		
		self.checkbox.hidden = YES;
		
		titleImageFrame.size = (CGSize){80.0f,40.0f};
	} else if (self.style == SSTableViewCellStyleIdeas) {
		
		self.sectionStyle = SSTableViewCellSectionStyleTwoSections;
		self.sectionSeperationX = 95.0f;
		[self applySectionStyle];
		
		titleLabelFrame.origin = (CGPoint){self.sectionSeperationX + 5.0f, 10.0f};
		titleLabelFrame.size = (CGSize){self.bounds.size.width - self.sectionSeperationX - 25.0f, 70.0f};
		self.titleLabel.verticalContentOffset = 0.0f;
		
		self.categoryColorView.hidden = YES;
		
		self.info1Label.frame = CGRectMake(5.0f, 2.0f, 80.0f, 16.0f);
		self.info2Label.hidden = YES;
		self.info3Label.hidden = NO;
		
		self.checkbox.hidden = YES;

		titleImageFrame.size = (CGSize){80.0f,55.0f};
	} else if (self.style == SSTableViewCellStyleCompetitionDetails ||
			   self.style == SSTableViewCellStyleDealDetails) {
		
		self.sectionStyle = SSTableViewCellSectionStyleOneSection;
		[self applySectionStyle];
		self.read = YES;
		
		titleLabelFrame = CGRectMake(5.0f, 10.0f, 290.0f, 60.0f);
		self.titleLabel.verticalContentOffset = 0.0f;
		
		self.titleImageView.hidden = YES;
		self.categoryColorView.hidden = YES;
		self.info1Label.hidden = YES;
		self.info2Label.hidden = YES;
		self.info3Label.hidden = YES;
		
		self.checkbox.hidden = YES;
		
	} else if (self.style == SSTableViewCellStyleInputList) {
		
		self.sectionStyle = SSTableViewCellSectionStyleTwoSections;
		self.read = YES;
		self.sectionSeperationX = 50.0f;
		[self applySectionStyle];
		
		titleLabelFrame.origin = (CGPoint){self.sectionSeperationX + 5.0f, 18.0f};
		titleLabelFrame.size = (CGSize){self.bounds.size.width - self.sectionSeperationX - 25.0f, 57.0f};
		self.titleLabel.verticalContentOffset = -8.0f;
		
		self.titleImageView.hidden = YES;
		self.categoryColorView.hidden = YES;
		self.info1Label.hidden = YES;
		self.info2Label.hidden = YES;
		self.info3Label.frame = (CGRect){{titleLabelFrame.origin.x,self.info3Label.frame.origin.y},self.bounds.size.width - titleLabelFrame.origin.x, self.info3Label.frame.size.height};
		self.info3Label.hidden = NO;
		
		self.checkbox.hidden = NO;
		self.checkbox.frame = (CGRect){{14.0f, 26.0f},self.checkbox.frame.size};
	}
	
	if (self.countBadge.badgeText.length > 0) {
		[self.countBadge sizeToFit];
		CGRect badgeFrame = self.countBadge.frame;
		badgeFrame.origin = CGPointMake(self.bounds.size.width - badgeFrame.size.width - 20.0f, badgeFrame.origin.y);
		self.countBadge.frame = badgeFrame;
		titleLabelFrame.size.width = badgeFrame.origin.x - titleLabelFrame.origin.x - 5.0f;
	}
	
	self.titleImageView.frame = titleImageFrame;
	self.titleLabel.frame = titleLabelFrame;
	
}

- (void)setSectionStyle:(SSTableViewCellSectionStyle)sectionStyle {
	if (sectionStyle != self->_sectionStyle) {
		self->_sectionStyle = sectionStyle;
		[self applySectionStyle];
	}
}

- (void)applySectionStyle {
		self.leftSectionBackgroundView.frame = CGRectMake(0.0f, 0.0f, 320.0f, 80.0f);
	if (self.sectionStyle == SSTableViewCellSectionStyleOneSection) {
		self.leftSectionBackgroundView.hidden = YES;
		self.rightSectionBackgroundView.frame = CGRectMake(0.0f, 0.0f, 320.0f, 80.0f);
	} else if (self.sectionStyle == SSTableViewCellSectionStyleTwoSections) {
		self.leftSectionBackgroundView.hidden = NO;
		self.rightSectionBackgroundView.frame = CGRectMake(self.sectionSeperationX, 0.0f, 320.0f - self.sectionSeperationX, 80.0f);
	}
}

@end
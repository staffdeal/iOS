//
//  SSSplashView.m
//  staffsale
//
//  Created by Daniel Wetzel on 10.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import "SSSplashView.h"

#import "UIDevice+DWToolbox.h"
#import <QuartzCore/QuartzCore.h>

@interface SSSplashView ()

#ifdef START_APP_INFO_ENABLED
@property (nonatomic, strong) UILabel *infoLabel;
#endif

@end

@implementation SSSplashView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
		
		UIImage *defaultImage = ([UIDevice currentDevice].hasRetina4Display) ? [UIImage imageNamed:@"Default-568h@2x"] : [UIImage imageNamed:@"Default"];
		if (defaultImage) {
			
			UIImageView *imageView = [[UIImageView alloc] initWithImage:defaultImage];
			imageView.frame = self.bounds;
			[self addSubview:imageView];
			
		} else {
			self.backgroundColor = [UIColor clearColor];
		}
		
#ifdef START_APP_INFO_ENABLED
		self.infoLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 524.0f, 305.0f, 25.0f)];
		self.infoLabel.backgroundColor = [UIColor clearColor];
		self.infoLabel.textColor = [UIColor colorWithWhite:0.6f alpha:1.0f];
		self.infoLabel.font = [UIFont boldSystemFontOfSize:9.0f];
		self.infoLabel.textAlignment = NSTextAlignmentRight;
		self.infoLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
		self.infoLabel.alpha = 0.0f;
		
		self.infoLabel.layer.shadowColor = [UIColor blackColor].CGColor;
		self.infoLabel.layer.shadowOffset = CGSizeMake(0.5f, 0.5f);
		self.infoLabel.layer.shadowRadius = 2.0f;
		self.infoLabel.layer.shadowOpacity = 0.5f;
		self.infoLabel.layer.masksToBounds = NO;
		
		[self addSubview:self.infoLabel];
		
		self.infoLabel.numberOfLines = 2;
		
		NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
		NSString *appBuildNumber = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
		self.infoLabel.text = [NSString stringWithFormat:@"Version %@\nBuild %@",appVersion, appBuildNumber];
		
		[UIView animateWithDuration:0.2f animations:^{
			self.infoLabel.alpha = 1.0f;
		}];
#endif
		
    }
    return self;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

//
//  SSNewsTableViewCell.h
//  staffsale
//
//  Created by Daniel Wetzel on 17.10.12.
//  Copyright (c) 2012 Daniel Wetzel. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DWLabel.h"
#import "DWCountdownLabel.h"
#import "DWImageView.h"
#import "DWBadge.h"
#import "DWCheckbox.h"

typedef enum {
	SSTableViewCellStyleNews,
	SSTableViewCellStyleIdeas,
	SSTableViewCellStyleCompetitionDetails,
	SSTableViewCellStyleInputList,
	SSTableViewCellStyleDealDetails,
} SSTableViewCellStyle;

typedef enum {
	SSTableViewCellSectionStyleOneSection,
	SSTableViewCellSectionStyleTwoSections,
} SSTableViewCellSectionStyle;

extern NSString * const kSSTableViewCellRequseIdentifier;

@class SSApiNews;


@interface SSTableViewCell : UITableViewCell

@property (nonatomic, assign) SSTableViewCellStyle style;
@property (nonatomic, assign) SSTableViewCellSectionStyle sectionStyle;

@property (weak, nonatomic) IBOutlet UIView *gradientBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *leftSectionBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *rightSectionBackgroundView;

@property (weak, nonatomic) IBOutlet DWLabel *titleLabel;

@property (weak, nonatomic) IBOutlet DWCountdownLabel *info1Label;
@property (weak, nonatomic) IBOutlet DWLabel *info2Label;
@property (weak, nonatomic) IBOutlet DWLabel *info3Label;

@property (nonatomic, strong) NSString *badge;
@property (weak, nonatomic) IBOutlet DWBadge *countBadge;

@property (weak, nonatomic) IBOutlet UIView *categoryColorView;

@property (weak, nonatomic) IBOutlet DWImageView *titleImageView;
@property (nonatomic, copy) NSURL *titleImageURL;

@property (weak, nonatomic) IBOutlet DWCheckbox *checkbox;

@property (nonatomic, copy) UIColor *unreadBackgroundColor;
@property (nonatomic, copy) UIColor *readBackgroundColor;
@property (nonatomic, copy) UIColor *selectedBackgroundColor;

@property (nonatomic, assign) BOOL read;
@property (nonatomic, copy) NSString *titleReadTextStyleKey;
@property (nonatomic, copy) NSString *titleUnreadTextStyleKey;

@end

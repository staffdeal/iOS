#! /usr/bin/env python

import os
import sys
import re
import fileinput
import plistlib

numberOfLinesOfContext = 3
fixAfterPrefix = 'FIXAFTER-'

directory = sys.argv[1]
excludes = sys.argv[2:]
annotationRE = re.compile(r'[!?]{3}:|TODO:|FIXME:|%s[^ :]+:' % fixAfterPrefix)

sourceRoot = os.getenv('SRCROOT')
infoPlistPath = os.path.join(sourceRoot, os.getenv('INFOPLIST_FILE'))
bundleVersion = plistlib.readPlist(infoPlistPath)["CFBundleShortVersionString"]

def isGreaterVersion(version1, version2):
	components1 = version1.strip().split('.')
	components2 = version2.strip().split('.')
	
	for i in range(min(len(components1), len(components2))):
		if components1[i] < components2[i]:
			return False

		if components1[i] > components2[i]:
			return True

	return False

suffixes = [".m", ".c", ".h"]
def findFiles(fileList, dirname, fileNames):
	for fileName in fileNames[:]:
		for exclude in excludes:
			if fileName.find(exclude) != -1:
				fileNames.remove(fileName)
				break


	for fileName in fileNames:
		skipFile = True
		for suffix in suffixes:
			if fileName.endswith(suffix):
				skipFile = False
				break

		if skipFile:
			continue

		fullpath = os.path.join(dirname, fileName)
		if os.path.isdir(fullpath):
			continue
		input = fileinput.input(fullpath)
		context = 0
		for line in input:
			match = annotationRE.search(line)
			if match:
				tag = match.group(0)
				if tag.startswith(fixAfterPrefix):
					fixAfterVersion = tag.replace(fixAfterPrefix, "").replace(':', '')
					if isGreaterVersion(fixAfterVersion, bundleVersion):
						continue
					
				context = numberOfLinesOfContext

			if context > 0:
				print "%s:%d:%s" % (fullpath, input.lineno(), line.replace('\n',''))
			if context > 0:
				context -= 1
				if context == 0:
					print
				
files = []
os.path.walk(directory, findFiles, files)
